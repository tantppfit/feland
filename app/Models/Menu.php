<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
	protected $fillable = [
		'name', 'url', 'location', 'order', 'show'
	];
	public function getMenus($location='', $show='')
  {
  	$query = $this->query();
  	$query->orderBy('order','desc');
  	if(!empty($location)){
      $query->where('location', $location);
    }
    if(!empty($show)){
      $query->where('show', true);
    }
    return $query->get();
  }
}