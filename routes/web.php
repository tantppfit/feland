<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Frontend')->group(function(){
  //HomeController
  Route::get('/', 'HomeController@index')->name('frontend.home.index');
  //
  Route::prefix('user')->group(function(){
    Route::get('/login', 'UserController@index')->name('frontend.user.login');
    Route::get('/profile', 'UserController@profile')->name('frontend.user.profile');
    Route::get('/profile/sale', 'UserController@sale')->name('frontend.user.sale');
  });
  //
  Route::prefix('purchase')->group(function(){
    Route::get('/all', 'PurchaseController@index')->name('frontend.purchase.index');
    Route::get('/all/item', 'PurchaseController@item')->name('frontend.purchase.item');
  });
  //
  Route::prefix('lease')->group(function(){
    Route::get('/all', 'LeaseController@index')->name('frontend.lease.index');
    Route::get('/all/item', 'LeaseController@item')->name('frontend.lease.item');
  });
  //
  Route::prefix('project')->group(function(){
    Route::get('/', 'ProjectController@index')->name('frontend.project.index');
  });
  //
  Route::prefix('shop')->group(function(){
    Route::get('/', 'ShopController@index')->name('frontend.shop.index');
    Route::get('/page', 'ShopController@page')->name('frontend.shop.page');
  });
  //
  Route::prefix('news')->group(function(){
    Route::get('/', 'NewsController@index')->name('frontend.news.index');
    Route::get('/page', 'NewsController@page')->name('frontend.news.page');
    Route::get('/page/post', 'NewsController@post')->name('frontend.news.post');
  });

  //
  //SearchController
  /*Route::get('search', 'SearchController@index')->name('frontend.search.index');
  Route::get('search/result', 'SearchController@result')->name('frontend.search.result');*/
  //PostController
  // Route::get('{slug}', 'PostController@show')->name('frontend.post.show');
  // Route::get('{slug}/album', 'PostController@album')->name('frontend.post.album');
  // Route::prefix('blog')->group(function(){
  //   Route::post('submit-vote/{id}', 'PostController@submitVote')->name('frontend.post.submitVote');
  //   Route::middleware(['auth'])->group(function(){
  //   	Route::get('preview/{id}', 'PostController@preview')->name('frontend.post.preview');
  //   });
  //   Route::post('load-more', 'PostController@loadMore')->name('frontend.post.loadmore');
  // });
  //UserController
  

  //CategoryController
  // Route::prefix('cat')->group(function(){
  //   Route::get('{id}', 'CategoryController@show')->name('frontend.category.show');
  // });
  //TagController
  // Route::prefix('tag')->group(function(){
  //   Route::get('feature', 'TagController@featureIndex')->name('frontend.tag.feature');
  //   Route::get('series', 'TagController@seriesIndex')->name('frontend.tag.series');
  //   Route::get('{id}', 'TagController@show')->name('frontend.tag.show');
  // });
  //PageController
  // Route::prefix('_p')->group(function(){
    /*Route::get('term-of-service', 'PageController@termOfService')->name('frontend.page.termofservice');
    Route::get('privacy-policy', 'PageController@privacyPolicy')->name('frontend.page.privacypolicy');*/
    //InquiryController
    // Route::get('inquiry', 'InquiryController@index')->name('frontend.inquiry.index');
    // Route::post('inquiry', 'InquiryController@submit')->name('frontend.inquiry.submit');
    //Search page
    // Route::get('search', 'SearchController@index')->name('frontend.search.index');
    // Route::get('search/result', 'SearchController@result')->name('frontend.search.result');
    //Normal page
  //   Route::get('{slug}', 'PageController@show')->name('frontend.page.show');
  // });
  //Widget Embed
  // Route::get('widget/embed', 'WidgetController@embed')->name('frontend.widget.embed');
});