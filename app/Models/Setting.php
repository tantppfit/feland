<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
	protected $fillable = [
		'key', 'display_name', 'value'
	];

	public function getSettingsData(){
		$query = $this->query();
		$settings = $query->get()->mapWithKeys(function ($item) {
	    return [$item['key'] => $item['value']];
		});
		return $settings->all();
	}
	public function getSettingData($key=''){
		$query = $this->query();
		$query->where('key', $key);
		$settingData = $query->first();
		if($settingData){
			return $settingData->value;
		}else{
			return false;
		}		
	}
}