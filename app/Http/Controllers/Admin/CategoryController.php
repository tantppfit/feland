<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    protected $categoryModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Category $categoryModel )
    {
        $this->categoryModel = $categoryModel;
    }

    public function index(Request $request)
    {
        $this->authorize('index', $this->categoryModel);
        $parentCategories = $this->categoryModel->getParentCategories();
        return view('admin.templates.category.index')->with([
            'parentCategories' => $parentCategories
        ]);
    }

    public function create(Request $request)
    {
        $this->authorize('create', $this->categoryModel);
        $parentCategories = $this->categoryModel->getParentCategories();
        return view('admin.templates.category.create')->with([
            'parentCategories' => $parentCategories
        ]);
    }
    public function store(Request $request)
    {
        $this->authorize('store', $this->categoryModel);
        $this->validate($request, [
          'name' => 'required|unique:categories',
        ],[],[
            'name' => trans('table_fields.categories.name')
        ]);
        $postData = $request->except(['_token']);
        $category = $this->categoryModel->create($postData);
        if($request->has('redirect')){
            return redirect($request->redirect)->with('success', trans('messages.success.create', ['Module' => trans('module.entry.category')]));
        }
        return redirect()->route('admin.category.index')->with('success', trans('messages.success.create', ['Module' => trans('module.entry.category')]));
    }

    public function edit(Request $request, $id)
    {
        $category = $this->categoryModel->findOrFail($id);
        $this->authorize('edit', $category);
        $parentCategories = $this->categoryModel->getParentCategories();
        return view('admin.templates.category.edit')->with([
            'category' => $category,
            'parentCategories' => $parentCategories
        ]);
    }
    public function update(Request $request, $id)
    {
        $category = $this->categoryModel->findOrFail($id);
        $this->authorize('update', $category);
        $this->validate($request, [
          'name' => 'required|unique:categories,name,' . $category->id,
        ],[],[
            'name' => trans('table_fields.categories.name')
        ]);
        $postData = $request->except(['_token']);
        $category->update($postData);
        return redirect()->route('admin.category.edit', $category)->with('success', trans('messages.success.update', ['Module' => trans('module.entry.category')]));
    }
    public function destroy($id)
    {
        $category = $this->categoryModel->findOrFail($id);
        $this->authorize('destroy', $category);
        $this->categoryModel->destroy($id);
        return redirect()->route('admin.category.index')->with('success', trans('messages.success.delete', ['Module' => trans('module.entry.category')]));
    }
}
