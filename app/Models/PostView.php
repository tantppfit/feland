<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PostView extends Model
{
	protected $fillable = [
		'post_id', 'session_id', 'ip', 'agent', 'user_id'
	];
	public function post()
  {
    return $this->belongsTo(Post::class);
  }
  public function createViewLog($post) {
    $postsViewAttributes = [
    	'post_id' => $post->id,
    	'user_id' => $post->user_id,
    	'session_id' => \Request::getSession()->getId(),
    	'ip' => \Request::getClientIp(),
    	'agent' => \Request::header('User-Agent')
    ];
    $postsView = $this->updateOrCreate($postsViewAttributes);
    return  $postsView;
  }
  public function getPostTotalView($user_id=null) {
  	$query = $this->query();
  	if(!is_null($user_id)){
  		$query->where('user_id', $user_id);
  	}
    $query->whereDate('updated_at', '>', Carbon::now()->subDays(30));
    return $query->count();
  }
  public function getUserTotalView() {
    $query = $this->query();
    $query->whereDate('updated_at', '>', Carbon::now()->subDays(30));
    return $query->count();
  }
  public function getViewLogByPost($post) {
  	$data = [];
  	for ($i=30; $i >= 0 ; $i--) {
  		$query = $this->query();
  		$date = Carbon::now()->subDays($i);
  		$data[] = [
  			'date' => $date->toDateString(),
  			'views' => $query->where('post_id', $post->id)->where('updated_at', '>', $date->startOfDay()->toDateTimeString())->where('updated_at', '<', $date->endOfDay()->toDateTimeString())->count()
  		];
  	}
  	$post->permalink = route('frontend.post.show', $post->slug);
  	$post->view_log = $data;
  	return $post;
  }
  public function getViewLogByUser($user) {
    $data = [];
    for ($i=30; $i >= 0 ; $i--) {
      $query = $this->query();
      $date = Carbon::now()->subDays($i);
      $data[] = [
        'date' => $date->toDateString(),
        'views' => $query->where('user_id', $user->id)->where('updated_at', '>', $date->startOfDay()->toDateTimeString())->where('updated_at', '<', $date->endOfDay()->toDateTimeString())->count()
      ];
    }
    $user->view_log = $data;
    return $user;
  }

}