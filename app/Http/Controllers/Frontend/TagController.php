<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Tag;
use App\Models\Post;

class TagController extends Controller
{
    protected $tagModel;
    protected $postModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Tag $tagModel, Post $postModel )
    {
        $this->tagModel = $tagModel;
        $this->postModel = $postModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function featureIndex(Request $request)
    {
        return view('frontend.templates.tag.feature');
    }
    
    public function seriesIndex(Request $request)
    {
        return view('frontend.templates.tag.series');
    }
    
    public function show(Request $request, $id)
    {
        $tag = $this->tagModel->findOrFail($id);
        $posts = $this->postModel->getPostByTag($tag->id, 1);
        $metaTitle = $tag->name;
        return view('frontend.templates.tag.show')->with([
            'tag' => $tag,
            'posts' => $posts,
            'metaTitle' => $metaTitle
        ]);
    }
    
}
