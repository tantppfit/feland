<!-- MODAL FILTER -->
<div class="modal fade" id="dynamic-filter-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="dynamic-filter-modal-dialog modal-dialog" role="document">
		<div class="dynamic-filter-modal-content modal-content">
			<div class="dynamic-filter-modal-header">
				<h4 class="text-center dymanic-filter-modal-title">Lọc Kết Quả</h4>
				<button type="button" class="close float-left" data-dismiss="modal" aria-label="Close">
					<span class="close-btn" aria-hidden="true">&times;</span>
				</button>
				<button type="button" class="cancel-filter-btn" data-dismiss="modal" aria-label="Close">
					<span class="cancel-filter" aria-hidden="true">Bỏ lọc</span>
				</button>
			</div>
			<div class="dynamic-filter-modal-body">
				<div class="section-wrapper">
					<a class="item-label">
						<span class="title">Danh mục</span>
						<span class="sub-title">Tất cả bất động sản</span>
						<img src="{{ asset('image/icon/c301c743e129ea23b89868cba27ca6c8.svg')}}" alt="" class="right-icon">
					</a>
				</div>
				<div class="section-wrapper">
					<a class="item-label">
						<span class="title">Tin có video</span>
						<span class="sub-title">Chưa lọc</span>
						<img src="{{ asset('image/icon/c301c743e129ea23b89868cba27ca6c8.svg')}}" alt="" class="right-icon">
					</a>
				</div>
				<div class="price-range-wrapper">
					<div class="price-from-to">
						<div>Giá từ <b class="price-min">0₫</b> đến <b class="price-max">30.000.000.000+₫</b></div>
					</div>
					<div class="range-price"></div>
				</div>
				<div class="section-wrapper">
					<a class="item-label">
						<span class="title">Dự án</span>
						<span class="sub-title">Chưa lọc</span>
						<img src="{{ asset('image/icon/c301c743e129ea23b89868cba27ca6c8.svg')}}" alt="" class="right-icon">
					</a>
				</div>
				<div class="section-filter p-0">
					<div class="static-check-field">
						<h2 class="h2-label">Sắp xếp theo</h2>
						<div class="without-modal-group">
							<div class="layout-group">
								<div class="layout-group-wrapper">
									<div class="layout-group-item">
										<div class="label-item">
											<div class="icon">
												<img class="left-icon" src="{{ asset('image/icon/order-by-time.svg') }}" alt="">
											</div>
											Mới nhất
										</div>
										<div class="radio-button">
											<!-- <label for="" class="label-button label-button-checked"></label> -->
											<input type="radio" name="1" value="" checked="">
										</div>
									</div>
								</div>
							</div>
							<div class="layout-group">
								<div class="layout-group-wrapper">
									<div class="layout-group-item">
										<div class="label-item">
											<div class="icon">
												<img class="left-icon" src="{{ asset('image/icon/order-by-price.svg') }}" alt="">
											</div>
											Giá thấp trước
										</div>
										<div class="radio-button">
											<!-- <label for="" class="label-button"></label> -->
											<input type="radio" name="1" value="">
										</div>
									</div>
								</div>
							</div>
						</div>

						<h2 class="h2-label">Đăng bởi</h2>
						<div class="without-modal-group">
							<div class="layout-group">
								<div class="layout-group-wrapper">
									<div class="layout-group-item">
										<div class="label-item">
											<div class="icon">
												<img class="left-icon" src="{{ asset('image/icon/individual-seller.svg') }}" alt="">
											</div>
											Cá nhân
										</div>
										<span class="checkbox-button">
											<input name="" type="checkbox" class="checkbox-button-input" value="">
											<!-- <span class="checkbox-button-inner"></span> -->
										</span>
									</div>
								</div>
							</div>
							<div class="layout-group">
								<div class="layout-group-wrapper">
									<div class="layout-group-item">
										<div class="label-item">
											<div class="icon">
												<img class="left-icon" src="{{ asset('image/icon/pro-seller.svg') }}" alt="">
											</div>
											Môi giới
										</div>
										<span class="checkbox-button">
											<input name="" type="checkbox" class="checkbox-button-input" value="">
											<!-- <span class="checkbox-button-inner"></span> -->
										</span>
									</div>
								</div>
							</div>
						</div>

						<h2 class="h2-label">Đăng bởi</h2>
						<div class="without-modal-group">
							<div class="layout-group">
								<div class="layout-group-wrapper">
									<div class="layout-group-item">
										<div class="label-item">
											<div class="icon">
												<img class="left-icon" src="{{ asset('image/icon/want-to-buy.svg') }}" alt="">
											</div>
											Mua
										</div>
										<span class="checkbox-button">
											<input name="" type="checkbox" class="checkbox-button-input" value="">
											<!-- <span class="checkbox-button-inner"></span> -->
										</span>
									</div>
								</div>
							</div>
							<div class="layout-group">
								<div class="layout-group-wrapper">
									<div class="layout-group-item">
										<div class="label-item">
											<div class="icon">
												<img class="left-icon" src="{{ asset('image/icon/want-to-sell.svg') }}" alt="">
											</div>
											Bán
										</div>
										<span class="checkbox-button">
											<input name="" type="checkbox" class="checkbox-button-input" value="">
											<!-- <span class="checkbox-button-inner"></span> -->
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="dynamic-filter-modal-footer">
				<button class="filter-submit" type="submit">
					<span aria-hidden="true">Áp dụng </span>
				</button>
			</div>
		</div>
	</div>
</div>