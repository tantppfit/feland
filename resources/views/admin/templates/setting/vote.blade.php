@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- main ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3>メディア設定</h3>
			@include('admin.parts.alert')
		</div>
		@include('admin.templates.setting.parts.setting-tabs')

		<section id="nav-tab-1">
			<div class="nav-tab-1-title">
				<p>アンケート</p>
			</div>
			<a href="{{ route('admin.vote.create') }}"><button class="btn btn-no-radius" style="background: #111111; color: #ffffff;"><i class="fas fa-plus" style="width: 25px; color: #ffffff;" aria-hidden="true"></i>アンケートフォームを追加</button></a>

			<div class="setting-table-list table-responsive-md" style="overflow: auto;">
				<table class="table">
						<tr>
						   	<th style="width: 25px;">ID</th>
						   	<th>タイトル</th>
						    <th>管理</th>
						</tr>
						@if(count($votes))
							@foreach( $votes as $vote )
								<tr>
								    <td>{{ $vote->id }}</td>
								    <td>{{ $vote->name }}</td>
								    <td>
								    	<div class="d-flex">
									    	<a href="{{ route('admin.vote.edit', $vote) }}" class="btn" style="font-size: 12px; background: #111111; color: #ffffff;width: 70px;">編集</a>
									    	<a class="ml-3 mt-1" href="{{ route('admin.vote.destroy', $vote) }}"><i class="fas fa-trash-alt" style="color: gray; font-size: 20px; vertical-align: middle;" aria-hidden="true"></i></a>
										</div>
								    </td>
								</tr>
							@endforeach
						@endif
				</table>
			</div>
			<div style="margin-top: 20px;">
				<p style="color: #999999;">#{{ (($votes->currentPage() - 1) * $votes->perPage()) + 1 }}...#{{ ($votes->currentPage() * $votes->perPage()) <= $votes->total() ? $votes->currentPage() * $votes->perPage() : $votes->total() }} / {{ $votes->total() }}</p>
				<div class="pagination-menu d-flex">{{ $votes->onEachSide(1)->links() }}</div>
			</div>
		</section>
	</section>
	<!-----x----- main -----x----->
@endsection