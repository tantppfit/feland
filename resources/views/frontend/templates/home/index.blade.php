@extends('frontend.layouts.app')
@section('content')
	<main class="main-wrapper">
		<div class="main-wrapper-banner">
			<div class="main-wrapper-banner-container">
				<div id="main-wrapper-banner-slide" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#main-wrapper-banner-slide" data-slide-to="0" class="active"></li>
						<li data-target="#main-wrapper-banner-slide" data-slide-to="1"></li>
						<li data-target="#main-wrapper-banner-slide" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner">
						<div class="carousel-item active">
							<a href="#">
								<img src="{{ asset('image/10260244425131594207.png') }}" class="d-block w-100" alt="...">
							</a>
						</div>
						<div class="carousel-item">
							<a href="#">
								<img src="{{ asset('image/11874923039034090262.png') }}" class="d-block w-100" alt="...">
							</a>
						</div>
						<div class="carousel-item">
							<a href="#">
								<img src="{{ asset('image/4031500640883312282.png') }}" class="d-block w-100" alt="...">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="main-wrapper-home">
			<!-- CATEGORIES -->
			<div class="categories-wrapper">
				<h1 class="categories-title">Khám phá danh mục Bất động sản</h1>
				<div class="categories-wrapper-cat d-flex">
					<li class="categories-cat-item col-4 p-0">
						<a class="categories-cat-item-wrapper d-flex" href="{{route('frontend.purchase.index')}}">
							<img src="image/icon/cat-1.svg">
							<div class="d-flex flex-col flex-auto">
								<span class="categories-cat-item-text">Mua bán</span>
								<span class="categories-cat-item-subtext"><b>470.390</b> tin đăng mua bán</span>
							</div>
						</a>
						<div class="categories-wrapper-dropdown">
							<a href="#" class="categories-dropdown-item">Căn hộ/Chung cư</a>
							<a href="#" class="categories-dropdown-item">Nhà ở</a>
							<a href="#" class="categories-dropdown-item">Đất</a>
							<a href="#" class="categories-dropdown-item">Văn phòng, Mặt bằng kinh doanh</a>
						</div>
					</li>
					<li class="categories-cat-item col-4 p-0">
						<a class="categories-cat-item-wrapper d-flex" href="{{route('frontend.lease.item')}}">
							<img src="image/icon/cat-2.svg">
							<div class="d-flex flex-col flex-auto">
								<span class="categories-cat-item-text">Cho thuê</span>
								<span class="categories-cat-item-subtext"><b>160.254</b> tin đăng mua bán</span>
							</div>
						</a>
						<div class="categories-wrapper-dropdown">
							<a href="#" class="categories-dropdown-item">Căn hộ/Chung cư</a>
							<a href="#" class="categories-dropdown-item">Nhà ở</a>
							<a href="#" class="categories-dropdown-item">Đất</a>
							<a href="#" class="categories-dropdown-item">Văn phòng, Mặt bằng kinh doanh</a>
							<a href="#" class="categories-dropdown-item">Phòng trọ</a>
						</div>
					</li>
					<li class="categories-cat-item col-4 p-0">
						<a class="categories-cat-item-wrapper d-flex" href="{{ route('frontend.project.index') }}">
							<img src="image/icon/cat-3.svg">
							<div class="d-flex flex-col flex-auto">
								<span class="categories-cat-item-text">Dự án</span>
								<span class="categories-cat-item-subtext"><b>4000</b> tin đăng mua bán</span>
							</div>
						</a>
					</li>
				</div>
			</div>
			<!-- ADS 1 -->
			<div class="ads-wrapper">
				<h2 class="ads-title">Mua bán Bất động sản</h2>
				<div class="wrapper-overflow position-relative" id="ads-slider-purchase">
					<div class="one-row-content swiper-container-ads-slider-purchase">
						<div class="ads-grid swiper-wrapper">
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.purchase.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/1.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">5</span>
												</div>
											</div>

										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">3,95 tỷ</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.purchase.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/2.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">3</span>
												</div>
											</div>
										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">3,95 tỷ</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.purchase.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/1.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">5</span>
												</div>
											</div>

										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">3,95 tỷ</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.purchase.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/2.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">3</span>
												</div>
											</div>
										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">3,95 tỷ</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.purchase.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/1.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">5</span>
												</div>
											</div>

										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">3,95 tỷ</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.purchase.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/2.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">3</span>
												</div>
											</div>
										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">3,95 tỷ</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.purchase.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/1.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">5</span>
												</div>
											</div>

										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">3,95 tỷ</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.purchase.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/2.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">3</span>
												</div>
											</div>
										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">3,95 tỷ</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<i class="i-next icon-next" style="background: url(image/icon/slide-next.svg) 50% no-repeat;"></i>
					<i class="i-prev icon-prev" style="background: url(image/icon/slide-prev.svg) 50% no-repeat;"></i>
				</div>
				<div class="load-more">
					<a href="{{ route('frontend.purchase.index') }}">Xem thêm 472.355 tin khác<i class="fas fa-chevron-right"></i></a>
				</div>
			</div>
			<!-- ADS 2 -->
			<div class="ads-wrapper">
				<h2 class="ads-title">Cho thuê Bất động sản</h2>
				<div class="wrapper-overflow position-relative" id="ads-slider-lease">
					<div class="one-row-content swiper-container-ads-slider-lease">
						<div class="ads-grid swiper-wrapper">
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.lease.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/1.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">5</span>
												</div>
											</div>

										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">25 triệu/tháng</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.lease.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/2.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">3</span>
												</div>
											</div>
										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">25 triệu/tháng</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.lease.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/1.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">5</span>
												</div>
											</div>

										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">25 triệu/tháng</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.lease.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/2.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">3</span>
												</div>
											</div>
										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">25 triệu/tháng</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.lease.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/1.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">5</span>
												</div>
											</div>

										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">25 triệu/tháng</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.lease.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/2.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">3</span>
												</div>
											</div>
										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">25 triệu/tháng</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.lease.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/1.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">5</span>
												</div>
											</div>

										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">25 triệu/tháng</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-item swiper-slide">
								<div class="ads-wrapper-link">
									<a class="ads-link" href="{{route('frontend.lease.item')}}">
										<div class="ads-thumnail-wrapper">
											<div class="thumnail-image">
												<img src="{{ asset('image/2.jpg') }}">
												<div class="image-number" style="background: url(image/icon/number-image.svg) no-repeat;">
													<span class="image-number-text">3</span>
												</div>
											</div>
										</div>
										<div class="ads-caption">
											<div class="ads-caption-title">Siêu đẹp nhà Trần Phú 38m2 5tầng 3,95T</div>
											<div class="ads-caption-condition-wrapper">
												<span class="ads-caption-condition">38 m² - 5 PN</span>
											</div>
											<span class="price">25 triệu/tháng</span>
										</div>
										<div class="ads-footer">
											<div>
												<img src="image/icon/pro.svg">
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-time">
												<span class="ads-footer-text">4 phút trước</span>
											</div>
											<div class="devider-wrapper">
											</div>
											<div class="ads-item-posted-location">
												<span class="ads-footer-text">Hà Nội</span>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<i class="i-next icon-next" style="background: url(image/icon/slide-next.svg) 50% no-repeat;"></i>
					<i class="i-prev icon-prev" style="background: url(image/icon/slide-prev.svg) 50% no-repeat;"></i>
				</div>
				<div class="load-more">
					<a href="{{ route('frontend.lease.index') }}">Xem thêm 472.355 tin khác<i class="fas fa-chevron-right"></i></a>
				</div>
			</div>

			<div class="mid-space"></div>

			<!-- ADS PROJECT -->
			<div class="ads-project-wrapper">
				<h2 class="ads-project-title">Dự án được quan tâm</h2>
				<div class="wrapper-overflow position-relative" id="ads-slider-interested">
					<div class="one-row-content swiper-container-ads-slider-interested">
						<div class="ads-grid-project swiper-wrapper">
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/3.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title">
												Celadon City
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Quận Tân Phú, Tp Hồ Chí Minh
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price">36.2 - 56.4 triệu/m²</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/4.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title">
												The Sun Avenue
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Quận Tân Phú, Tp Hồ Chí Minh
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price">36.2 - 56.4 triệu/m²</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/3.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title">
												Celadon City
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Quận Tân Phú, Tp Hồ Chí Minh
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price">36.2 - 56.4 triệu/m²</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/4.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title">
												The Sun Avenue
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Quận Tân Phú, Tp Hồ Chí Minh
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price">36.2 - 56.4 triệu/m²</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/3.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title">
												Celadon City
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Quận Tân Phú, Tp Hồ Chí Minh
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price">36.2 - 56.4 triệu/m²</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/4.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title">
												The Sun Avenue
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Quận Tân Phú, Tp Hồ Chí Minh
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price">36.2 - 56.4 triệu/m²</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/3.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title">
												Celadon City
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Quận Tân Phú, Tp Hồ Chí Minh
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price">36.2 - 56.4 triệu/m²</span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/4.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title">
												The Sun Avenue
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Quận Tân Phú, Tp Hồ Chí Minh
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price">36.2 - 56.4 triệu/m²</span>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<i class="i-next icon-next" style="background: url(image/icon/slide-next.svg) 50% no-repeat;"></i>
					<i class="i-prev icon-prev" style="background: url(image/icon/slide-prev.svg) 50% no-repeat;"></i>
				</div>
				<div class="load-more">
					<a href="{{ route('frontend.project.index') }}">Xem thêm 4.000 dự án khác<i class="fas fa-chevron-right"></i></a>
				</div>
			</div>
			<!-- SHOP -->
			<div class="ads-project-wrapper">
				<h2 class="ads-project-title">Chuyên trang Bất động sản</h2>
				<div class="wrapper-overflow position-relative" id="ads-slider-page">
					<div class="one-row-content swiper-container-ads-slider-page">
						<div class="ads-grid-project swiper-wrapper">
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<div class="ads-shop-avatar">
										<a href="{{ route('frontend.shop.page') }}">
											<img src="image/page-avatar-1.jpg">
										</a>
									</div>
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail ads-project-shop-thumbnail">
												<img src="image/page-1.jpg">
											</div>
										</div>
										<div class="ads-project-caption text-center ads-shop-caption">
											<div class="ads-project-caption-title ads-shop-title">
												Cho Thuê Căn Hộ Giá Rẻ Quận 12 -Tân Phú-Tân Bình
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Quận 12, Hồ Chí Minh
											</div>
											<div class="ads-shop-intro">
												<div class="ads-shop-intro-text">
													Chuyên cho thuê chung cư quận 12: chung cư topaz home, chung cư prosper plaza,chung cư 8x trường chinh.Căn hộ mới bàn giao giá chỉ 5tr/tháng,căn hộ full nội thất giá chỉ 7tr/tháng
												</div>
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<div class="ads-shop-avatar">
										<a href="{{ route('frontend.shop.page') }}">
											<img src="image/page-avatar-2.jpg">
										</a>
									</div>
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail ads-project-shop-thumbnail">
												<img src="image/page-2.jpg">
											</div>
										</div>
										<div class="ads-project-caption text-center ads-shop-caption">
											<div class="ads-project-caption-title ads-shop-title">
												Cho Thuê Căn Hộ Cao Cấp TP HCM
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Aqua 1, Bến Nghé, Quận 1, Hồ Chí Minh, Việt Nam
											</div>
											<div class="ads-shop-intro">
												<div class="ads-shop-intro-text">
													Chuyên cho thuê căn hộ cao cấp khu vực TP Hồ Chí Minh
												</div>
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<div class="ads-shop-avatar">
										<a href="{{ route('frontend.shop.page') }}">
											<img src="image/page-avatar-1.jpg">
										</a>
									</div>
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail ads-project-shop-thumbnail">
												<img src="image/page-1.jpg">
											</div>
										</div>
										<div class="ads-project-caption text-center ads-shop-caption">
											<div class="ads-project-caption-title ads-shop-title">
												Cho Thuê Căn Hộ Giá Rẻ Quận 12 -Tân Phú-Tân Bình
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Quận 12, Hồ Chí Minh
											</div>
											<div class="ads-shop-intro">
												<div class="ads-shop-intro-text">
													Chuyên cho thuê chung cư quận 12: chung cư topaz home, chung cư prosper plaza,chung cư 8x trường chinh.Căn hộ mới bàn giao giá chỉ 5tr/tháng,căn hộ full nội thất giá chỉ 7tr/tháng
												</div>
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<div class="ads-shop-avatar">
										<a href="{{ route('frontend.shop.page') }}">
											<img src="image/page-avatar-2.jpg">
										</a>
									</div>
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail ads-project-shop-thumbnail">
												<img src="image/page-2.jpg">
											</div>
										</div>
										<div class="ads-project-caption text-center ads-shop-caption">
											<div class="ads-project-caption-title ads-shop-title">
												Cho Thuê Căn Hộ Cao Cấp TP HCM
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Aqua 1, Bến Nghé, Quận 1, Hồ Chí Minh, Việt Nam
											</div>
											<div class="ads-shop-intro">
												<div class="ads-shop-intro-text">
													Chuyên cho thuê căn hộ cao cấp khu vực TP Hồ Chí Minh
												</div>
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<div class="ads-shop-avatar">
										<a href="{{ route('frontend.shop.page') }}">
											<img src="image/page-avatar-1.jpg">
										</a>
									</div>
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail ads-project-shop-thumbnail">
												<img src="image/page-1.jpg">
											</div>
										</div>
										<div class="ads-project-caption text-center ads-shop-caption">
											<div class="ads-project-caption-title ads-shop-title">
												Cho Thuê Căn Hộ Giá Rẻ Quận 12 -Tân Phú-Tân Bình
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Quận 12, Hồ Chí Minh
											</div>
											<div class="ads-shop-intro">
												<div class="ads-shop-intro-text">
													Chuyên cho thuê chung cư quận 12: chung cư topaz home, chung cư prosper plaza,chung cư 8x trường chinh.Căn hộ mới bàn giao giá chỉ 5tr/tháng,căn hộ full nội thất giá chỉ 7tr/tháng
												</div>
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<div class="ads-shop-avatar">
										<a href="{{ route('frontend.shop.page') }}">
											<img src="image/page-avatar-2.jpg">
										</a>
									</div>
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail ads-project-shop-thumbnail">
												<img src="image/page-2.jpg">
											</div>
										</div>
										<div class="ads-project-caption text-center ads-shop-caption">
											<div class="ads-project-caption-title ads-shop-title">
												Cho Thuê Căn Hộ Cao Cấp TP HCM
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Aqua 1, Bến Nghé, Quận 1, Hồ Chí Minh, Việt Nam
											</div>
											<div class="ads-shop-intro">
												<div class="ads-shop-intro-text">
													Chuyên cho thuê căn hộ cao cấp khu vực TP Hồ Chí Minh
												</div>
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<div class="ads-shop-avatar">
										<a href="{{ route('frontend.shop.page') }}">
											<img src="image/page-avatar-1.jpg">
										</a>
									</div>
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail ads-project-shop-thumbnail">
												<img src="image/page-1.jpg">
											</div>
										</div>
										<div class="ads-project-caption text-center ads-shop-caption">
											<div class="ads-project-caption-title ads-shop-title">
												Cho Thuê Căn Hộ Giá Rẻ Quận 12 -Tân Phú-Tân Bình
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Quận 12, Hồ Chí Minh
											</div>
											<div class="ads-shop-intro">
												<div class="ads-shop-intro-text">
													Chuyên cho thuê chung cư quận 12: chung cư topaz home, chung cư prosper plaza,chung cư 8x trường chinh.Căn hộ mới bàn giao giá chỉ 5tr/tháng,căn hộ full nội thất giá chỉ 7tr/tháng
												</div>
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<div class="ads-shop-avatar">
										<a href="{{ route('frontend.shop.page') }}">
											<img src="image/page-avatar-2.jpg">
										</a>
									</div>
									<a href="{{ route('frontend.shop.page') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail ads-project-shop-thumbnail">
												<img src="image/page-2.jpg">
											</div>
										</div>
										<div class="ads-project-caption text-center ads-shop-caption">
											<div class="ads-project-caption-title ads-shop-title">
												Cho Thuê Căn Hộ Cao Cấp TP HCM
											</div>
											<div class="ads-project-caption-address">
												<i class="fas fa-map-marker-alt"></i>
												Aqua 1, Bến Nghé, Quận 1, Hồ Chí Minh, Việt Nam
											</div>
											<div class="ads-shop-intro">
												<div class="ads-shop-intro-text">
													Chuyên cho thuê căn hộ cao cấp khu vực TP Hồ Chí Minh
												</div>
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<i class="i-next icon-next" style="background: url(image/icon/slide-next.svg) 50% no-repeat;"></i>
					<i class="i-prev icon-prev" style="background: url(image/icon/slide-prev.svg) 50% no-repeat;"></i>
				</div>
			</div>
			<div class="load-more">
				<a href="{{ route('frontend.shop.index') }}">Xem thêm<i class="fas fa-chevron-right"></i></a>
			</div>
			<!-- NEWS -->
			<div class="ads-project-wrapper">
				<h2 class="ads-project-title">Tin tức Bất động sản</h2>
				<div class="wrapper-overflow position-relative" id="ads-slider-news">
					<div class="one-row-content swiper-container-ads-slider-news">
						<div class="ads-grid-project swiper-wrapper">
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.news.post') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/blog-1.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title ads-blog-title">
												CHUYỆN TÌM TRỌ Ở TP.HCM: GIÁ THUÊ BAO NHIÊU LÀ ĐỦ?
											</div>
											<div class="ads-blog-description three-line">
												Chuyên trang Chợ Tốt Nhà khảo sát trên 40,000 tin đăng phòng trọ ở TP.HCM trong tháng 10/2020, thị trường cho thuê phòng trọ liên tục sôi động từ tháng 8 đến nay với nguồn cung phòng trọ đầy đủ tiện nghi dồi dào.
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.news.post') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/blog-2.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title ads-blog-title">
												CẬP NHẬT GIÁ THỊ TRƯỜNG CHO THUÊ TPHCM THÁNG 9/2020
											</div>
											<div class="ads-blog-description three-line">
												Theo thống kê của Chợ Tốt Nhà trong tháng 09/2020, thông tin giá thuê của hai phân khúc nhà nguyên căn và căn hộ chung cư theo từng quận/huyện ở TP.HCM được cập nhật mới nhất ở bài viết bên dưới:
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.news.post') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/blog-1.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title ads-blog-title">
												CHUYỆN TÌM TRỌ Ở TP.HCM: GIÁ THUÊ BAO NHIÊU LÀ ĐỦ?
											</div>
											<div class="ads-blog-description three-line">
												Chuyên trang Chợ Tốt Nhà khảo sát trên 40,000 tin đăng phòng trọ ở TP.HCM trong tháng 10/2020, thị trường cho thuê phòng trọ liên tục sôi động từ tháng 8 đến nay với nguồn cung phòng trọ đầy đủ tiện nghi dồi dào.
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.news.post') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/blog-2.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title ads-blog-title">
												CẬP NHẬT GIÁ THỊ TRƯỜNG CHO THUÊ TPHCM THÁNG 9/2020
											</div>
											<div class="ads-blog-description three-line">
												Theo thống kê của Chợ Tốt Nhà trong tháng 09/2020, thông tin giá thuê của hai phân khúc nhà nguyên căn và căn hộ chung cư theo từng quận/huyện ở TP.HCM được cập nhật mới nhất ở bài viết bên dưới:
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.news.post') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/blog-1.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title ads-blog-title">
												CHUYỆN TÌM TRỌ Ở TP.HCM: GIÁ THUÊ BAO NHIÊU LÀ ĐỦ?
											</div>
											<div class="ads-blog-description three-line">
												Chuyên trang Chợ Tốt Nhà khảo sát trên 40,000 tin đăng phòng trọ ở TP.HCM trong tháng 10/2020, thị trường cho thuê phòng trọ liên tục sôi động từ tháng 8 đến nay với nguồn cung phòng trọ đầy đủ tiện nghi dồi dào.
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.news.post') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/blog-2.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title ads-blog-title">
												CẬP NHẬT GIÁ THỊ TRƯỜNG CHO THUÊ TPHCM THÁNG 9/2020
											</div>
											<div class="ads-blog-description three-line">
												Theo thống kê của Chợ Tốt Nhà trong tháng 09/2020, thông tin giá thuê của hai phân khúc nhà nguyên căn và căn hộ chung cư theo từng quận/huyện ở TP.HCM được cập nhật mới nhất ở bài viết bên dưới:
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.news.post') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/blog-1.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title ads-blog-title">
												CHUYỆN TÌM TRỌ Ở TP.HCM: GIÁ THUÊ BAO NHIÊU LÀ ĐỦ?
											</div>
											<div class="ads-blog-description three-line">
												Chuyên trang Chợ Tốt Nhà khảo sát trên 40,000 tin đăng phòng trọ ở TP.HCM trong tháng 10/2020, thị trường cho thuê phòng trọ liên tục sôi động từ tháng 8 đến nay với nguồn cung phòng trọ đầy đủ tiện nghi dồi dào.
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="ads-grid-project-item swiper-slide">
								<div class="ads-project-wrapper-link">
									<a href="{{ route('frontend.news.post') }}" class="ads-project-link">
										<div class="ads-project-thumbnail-wrapper">
											<div class="ads-project-thumbnail">
												<img src="image/blog-2.jpg">
											</div>
										</div>
										<div class="ads-project-caption">
											<div class="ads-project-caption-title ads-blog-title">
												CẬP NHẬT GIÁ THỊ TRƯỜNG CHO THUÊ TPHCM THÁNG 9/2020
											</div>
											<div class="ads-blog-description three-line">
												Theo thống kê của Chợ Tốt Nhà trong tháng 09/2020, thông tin giá thuê của hai phân khúc nhà nguyên căn và căn hộ chung cư theo từng quận/huyện ở TP.HCM được cập nhật mới nhất ở bài viết bên dưới:
											</div>
											<div class="ads-project-caption-condition-wrapper">
												<span class="ads-project-caption-condition"></span>
											</div>
											<div class="ads-project-caption-price">
												<span class="price"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<i class="i-next icon-next" style="background: url(image/icon/slide-next.svg) 50% no-repeat;"></i>
					<i class="i-prev icon-prev" style="background: url(image/icon/slide-prev.svg) 50% no-repeat;"></i>
				</div>
			</div>
			<div class="load-more">
				<a href="{{ route('frontend.news.page') }}">Xem thêm<i class="fas fa-chevron-right"></i></a>
			</div>

			<!-- READ MORE -->
			<div class="home-container">
				<div class="description-wrapper">
					<div id="home-content" class="home-content-less-info">
						<div class="d-flex">
							<div class="col-12 p-0">
								<h2 class="text-center"><strong>BẤT ĐỘNG SẢN THÁNG 12/2020</strong></h2>
								<p style="text-align: justify;">Chợ Tốt Nhà (nha.chotot.com) Chuyên trang <strong>mua bán và cho thuê bất động sản</strong> được ra mắt bởi trang mua bán trực tuyến Chợ Tốt vào đầu năm 2017. Với hơn 5 triệu lượt truy cập và hơn 200.000 tin đăng phân bố khắp các tỉnh thành trong cả nước mỗi tháng, Chợ Tốt Nhà hướng tới là một trang mua bán bất động sản hiệu quả, dễ sử dụng, đa dạng lựa chọn cho người dùng.</p>
								<p style="text-align: justify;">Với Chợ Tốt Nhà, bạn dễ dàng tìm kiếm mua bán/cho thuê với đa dạng loại hình <strong>bất động sản</strong>, bao gồm:</p>
								<ul style="text-align: justify;">
									<li><a href="#">Nhà đất</a>: bạn có thể dễ dàng tìm kiếm theo diện tích, vị trí, hướng cửa chính, đáp ứng đầy đủ các nhu cầu từ tìm nhà hẻm, nhà mặt tiền, nhà phố, nhà biệt thự.</li>
									<li><a href="#">Căn hộ chung cư</a>: đa dạng các loại hình căn hộ từ chung cư, duplex, penthouse, căn hộ dịch vụ, căn hộ mini, tập thể, cư xá với đầy đủ tiện ích xung quanh.</li>
									<li><a href="#">Đất</a>: tùy vào mục đích sử dụng, vị trí và diện tích mong muốn, bạn có thể dễ dàng chọn lựa giữa hàng nghìn tin đăng đáp ứng theo nhu cầu.</li>
									<li><a href="#">Văn phòng, mặt bằng kinh doanh</a>: nếu bạn muốn bắt đầu kinh doanh từ việc mở quán cafe, quán ăn, kinh doanh quần áo, tiệm in ấn, sửa chữa,... thì hoàn toàn có thể tìm kiếm mặt bằng phù hợp. Nếu bạn đang tìm kiếm văn phòng làm việc thì hãy lên Chợ Tốt Nhà để tìm kiếm vị trí hợp phong thủy, diện tích sử dụng rộng rãi,... giúp việc kinh doanh trở nên thuận lợi hơn.</li>
									<li><a href="#">Phòng trọ</a>: dễ dàng tìm kiếm phòng trọ gần khu vực văn phòng, trường học với không gian thoáng mát, tiện di chuyển đến khu vực trung tâm.</li>
								</ul>
								<p style="text-align: justify;"><strong>Mua bán và cho thuê bất động sản</strong> trên Chợ Tốt Nhà trở nên dễ dàng hơn khi bạn có thể tìm kiếm dựa trên nhu cầu cá nhân với những tin đăng mới nhất, được cập nhật thường xuyên, liên tục.</p>
								<p style="text-align: justify;">Hy vọng bạn sẽ có những trải nghiệm tuyệt với với Chợ Tốt Nhà.</p>
							</div>
						</div>
					</div>	
					<p class="description-read-less-gradient"></p>
					<p class="text-center">
						<a id="btn-read-more">Mở rộng<i class="fa fa-angle-down ml-2" aria-hidden="true"></i></a>
					</p>
					<p class="text-center">
						<a id="btn-read-less">Thu gọn<i class="fa fa-angle-up ml-2" aria-hidden="true"></i></a>
					</p>
				</div>
			</div>									
		</div>
	</main>
@endsection