@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main" >
		<div class="post-edit">
			<h3>カテゴリー管理</h3>
      		<p style="margin-bottom: 0px;">記事作成時に入力候補として使用するカテゴリーを管理します。</p>
      		<p>あらかじめカテゴリーを登録しておくことにより、記事を作成する際にカテゴリーの入力候補として利用できます。</p>

      @include('admin.parts.alert')

    </div>
    <a href="{{ route('admin.category.create') }}" ><button class="btn btn-no-radius" style="background: #111111; color: #ffffff; margin-top: 2rem;"><i class="fas fa-plus" style="width: 25px; color: #ffffff;"></i>新規カテゴリー</button></a>
    <div class="tag-management-table table-responsive-md">
			<table class="table">
			 <tr>
			    <th>カテゴリー名</th>
			    <th style="width: 100px;"></th>
			  </tr>
			  @if(count($parentCategories))
			  	@foreach($parentCategories as $parentCategory)
					  <tr>
							<td>{{ $parentCategory->name }}</td>
							<td>
								<a href="{{ route('admin.category.edit', $parentCategory) }}"><button style="margin-top: 5px;"><i class="fa fa-pencil" style="color:black;"></i></button></a>
								<a href="{{ route('admin.category.destroy', $parentCategory) }}"><button style="margin-top: 5px;"><i class="fas fa-trash-alt" style="color:black;"></i></button></a>
							</td> 
					  </tr>
					  @if($parentCategory->children && count($parentCategory->children))
					  	@foreach($parentCategory->children as $categoryChild)
							  <tr>
									<td>-- {{ $categoryChild->name }}</td>
									<td>
										<a href="{{ route('admin.category.edit', $categoryChild) }}"><button style="margin-top: 5px;"><i class="fa fa-pencil" style="color:black;"></i></button></a>
										<a href="{{ route('admin.category.destroy', $categoryChild) }}"><button style="margin-top: 5px;"><i class="fas fa-trash-alt" style="color:black;"></i></button></a>
									</td> 
							  </tr>
							@endforeach
						@endif
				  @endforeach
			  @endif
			</table>
		</div>
	</section>
	<!---------- admin-edit main section ---------->
@endsection