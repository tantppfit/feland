@extends('frontend.layouts.app')
@section('content')
	<main class="list-view-container">
		<div class="list-view-wrapper">
			<div class="dynamic-filter-wrapper">
				<div class="d-flex">
					<div class="dynamic-filter">
						<div class="dynamic-filter-advance" data-toggle="modal" data-target="#dynamic-filter-modal">
							<span class="dynamic-filter-badge">1</span>
							<span class="dynamic-filter-advance-icon"></span>
							Lọc
						</div>
						<div class="dynamic-filter-item dynamic-filter-item-selected">
							<span class="dynamic-filter-item-icon"></span>
							<span class="dynamic-filter-item-value">Toàn quốc</span>
							<span class="dynamic-item-dropdow"></span>
						</div>
						<div class="dynamic-filter-item dynamic-filter-item-selected" data-toggle="modal" data-target="#dynamic-filter-you-need">
							<span class="dynamic-filter-item-value">Mua bán</span>
							<span class="dynamic-item-dropdow"></span>
						</div>
						<div class="dynamic-filter-item dynamic-filter-item-selected" data-toggle="modal" data-target="#dynamic-filter-type">
							<span class="dynamic-filter-item-value">Loại BĐS</span>
							<span class="dynamic-item-dropdow"></span>
						</div>
						<div class="dynamic-filter-item" data-toggle="modal" data-target="#dynamic-filter-price">
							<span>Giá</span>
							<span>+</span>
						</div>
						<div class="dynamic-filter-item" data-toggle="modal" data-target="#dynamic-filter-project">
							<span>Dự án</span>
							<span>+</span>
						</div>
						<div class="dynamic-filter-item" data-toggle="modal" data-target="#dynamic-filter-video-type">
							<span>Tin có video</span>
							<span>+</span>
						</div>
					</div>
				</div>
			</div>
			<div class="base">
				<div class="base-header">
					<div class="googdle-ads"></div>
					<div>
						<ol class="base-header-breadcrumb hidden-xs">
							<li class="breadcrumb-item"><a href="#">Chợ Tốt Nhà</a></li>
							<li class="breadcrumb-item"><a href="#">Toàn quốc</a></li>
							<li class="breadcrumb-item active"><a href="#">Tất cả loại bất động sản</a></li>
						</ol>
					</div>
					<div><h1 itemprop="name" class="base-header-title">Bất Động Sản Giá Rẻ</h1></div>
				</div>
				<div class="base-main">
					<div class="col-md-8 p-0">
						<div class="wrapper-overflow">
							<div class="quick-filter-content">
								<div>
									<a href="#">
										<div class="item-circle">
											<div class="item-circle-icon-round">
												<img class="item-circle-icon" src="{{ asset('image/icon/1010.svg') }}">
											</div>
											<p class="item-circle-label-inline">Căn hộ/Chung cư</p>
										</div>
									</a>
								</div>
								<div>
									<a href="#">
										<div class="item-circle">
											<div class="item-circle-icon-round">
												<img class="item-circle-icon" src="{{ asset('image/icon/1020.svg') }}">
											</div>
											<p class="item-circle-label-inline">Nhà ở</p>
										</div>
									</a>
								</div>
								<div>
									<a href="#">
										<div class="item-circle">
											<div class="item-circle-icon-round">
												<img class="item-circle-icon" src="{{ asset('image/icon/1040.svg') }}">
											</div>
											<p class="item-circle-label-inline">Đất</p>
										</div>
									</a>
								</div>
								<div>
									<a href="#">
										<div class="item-circle">
											<div class="item-circle-icon-round">
												<img class="item-circle-icon" src="{{ asset('image/icon/1030.svg') }}">
											</div>
											<p class="item-circle-label-inline">Văn phòng, Mặt bằng kinh doanh</p>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="order-filter">
							<div class="info-ads float-left">
								<a href="#">
									<div class="element-info active">
										<span>Tất cả</span>
									</div>
								</a>
								<a href="#">
									<div class="element-info">
										<span>Cá nhân</span>
									</div>
								</a>
								<a href="#">
									<div class="element-info">
										<span>Môi giới</span>
									</div>
								</a>
							</div>
							<div class="sort-ads float-right">
								<div class="wrapper-dropdown">
									<button class="btn filter-btn">
										<span>Tin mới trước</span>
										<span class="caret"></span>
									</button>
								</div>
								<div class="view-mode">
									<button type="button" class="grid-mode"></button>
								</div>
							</div>
						</div>
						<div class="list-view">
							<!-- list mode -->
							<div class="list-view-wrapper-list">
								<ul>
									<!-- 25 item -->
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-1.jpg') }}" alt="">
														<div class="ad-image-group"></div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">Cần mua 360</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">360 m²</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/user.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Chung</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															<span class="ad-item-footer-sticky"></span>
															Tin ưu tiên
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Long An</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-2.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">5</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">Bán 14 căn phòng trọ hiện đang cho thuê full phòng</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">32 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,14 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															<span class="ad-item-footer-sticky"></span>
															Tin ưu tiên
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-3.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">7</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">52.62 m² - 2 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,68 tỷ</span>
																<div class="comon-style-label-price">GIÁ TỐT</div>
															</span>
															
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
											<div class="ad-item-ribbon">HOT</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-4.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">4</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">BÂN CĂN HỘ XA ĐÀN - MỚI - FULL NỘI THẤT- Ở NGAY.</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">9 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">2,6 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/shop.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Căn hộ Topaz Elite - Topaz City - Topaz Home2</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Lâm Đồng</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-2.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">5</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">Bán 14 căn phòng trọ hiện đang cho thuê full phòng</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">32 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,14 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															<span class="ad-item-footer-sticky"></span>
															Tin ưu tiên
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-3.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">7</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">52.62 m² - 2 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,68 tỷ</span>
																<div class="comon-style-label-price">GIÁ TỐT</div>
															</span>
															
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
											<div class="ad-item-ribbon">HOT</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-4.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">4</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">BÂN CĂN HỘ XA ĐÀN - MỚI - FULL NỘI THẤT- Ở NGAY.</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">9 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">2,6 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/shop.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Căn hộ Topaz Elite - Topaz City - Topaz Home2</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Lâm Đồng</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-2.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">5</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">Bán 14 căn phòng trọ hiện đang cho thuê full phòng</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">32 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,14 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															<span class="ad-item-footer-sticky"></span>
															Tin ưu tiên
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-3.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">7</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">52.62 m² - 2 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,68 tỷ</span>
																<div class="comon-style-label-price">GIÁ TỐT</div>
															</span>
															
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
											<div class="ad-item-ribbon">HOT</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-4.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">4</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">BÂN CĂN HỘ XA ĐÀN - MỚI - FULL NỘI THẤT- Ở NGAY.</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">9 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">2,6 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/shop.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Căn hộ Topaz Elite - Topaz City - Topaz Home2</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Lâm Đồng</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-2.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">5</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">Bán 14 căn phòng trọ hiện đang cho thuê full phòng</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">32 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,14 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															<span class="ad-item-footer-sticky"></span>
															Tin ưu tiên
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-3.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">7</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">52.62 m² - 2 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,68 tỷ</span>
																<div class="comon-style-label-price">GIÁ TỐT</div>
															</span>
															
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
											<div class="ad-item-ribbon">HOT</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-4.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">4</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">BÂN CĂN HỘ XA ĐÀN - MỚI - FULL NỘI THẤT- Ở NGAY.</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">9 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">2,6 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/shop.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Căn hộ Topaz Elite - Topaz City - Topaz Home2</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Lâm Đồng</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-2.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">5</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">Bán 14 căn phòng trọ hiện đang cho thuê full phòng</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">32 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,14 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															<span class="ad-item-footer-sticky"></span>
															Tin ưu tiên
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-3.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">7</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">52.62 m² - 2 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,68 tỷ</span>
																<div class="comon-style-label-price">GIÁ TỐT</div>
															</span>
															
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
											<div class="ad-item-ribbon">HOT</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-4.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">4</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">BÂN CĂN HỘ XA ĐÀN - MỚI - FULL NỘI THẤT- Ở NGAY.</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">9 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">2,6 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/shop.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Căn hộ Topaz Elite - Topaz City - Topaz Home2</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Lâm Đồng</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-2.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">5</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">Bán 14 căn phòng trọ hiện đang cho thuê full phòng</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">32 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,14 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															<span class="ad-item-footer-sticky"></span>
															Tin ưu tiên
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-3.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">7</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">52.62 m² - 2 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,68 tỷ</span>
																<div class="comon-style-label-price">GIÁ TỐT</div>
															</span>
															
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
											<div class="ad-item-ribbon">HOT</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-4.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">4</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">BÂN CĂN HỘ XA ĐÀN - MỚI - FULL NỘI THẤT- Ở NGAY.</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">9 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">2,6 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/shop.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Căn hộ Topaz Elite - Topaz City - Topaz Home2</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Lâm Đồng</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-2.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">5</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">Bán 14 căn phòng trọ hiện đang cho thuê full phòng</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">32 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,14 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															<span class="ad-item-footer-sticky"></span>
															Tin ưu tiên
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-3.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">7</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">52.62 m² - 2 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,68 tỷ</span>
																<div class="comon-style-label-price">GIÁ TỐT</div>
															</span>
															
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
											<div class="ad-item-ribbon">HOT</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-4.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">4</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">BÂN CĂN HỘ XA ĐÀN - MỚI - FULL NỘI THẤT- Ở NGAY.</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">9 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">2,6 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/shop.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Căn hộ Topaz Elite - Topaz City - Topaz Home2</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Lâm Đồng</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-2.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">5</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">Bán 14 căn phòng trọ hiện đang cho thuê full phòng</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">32 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,14 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															<span class="ad-item-footer-sticky"></span>
															Tin ưu tiên
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-3.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">7</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">52.62 m² - 2 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">1,68 tỷ</span>
																<div class="comon-style-label-price">GIÁ TỐT</div>
															</span>
															
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/pro.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Môi giới</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Tp Hồ Chí Minh</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
											<div class="ad-item-ribbon">HOT</div>
										</li>
									</div>
									<div>
										<li class="wrapper-ad-item">
											<a href="{{ route('frontend.purchase.item') }}" class="ad-item">
												<div class="ad-item-left">
													<div class="ad-thumbnail-wrapper">
														<img class="ad-thumbnail" src="{{ asset('image/list-view-ads-4.jpg') }}" alt="">
														<div class="ad-image-group">
															<div class="ad-image-group-number">4</div>
														</div>
													</div>
												</div>
												<div class="ad-item-right">
													<div class="ad-item-right-body">
														<h3 class="ad-item-title">BÂN CĂN HỘ XA ĐÀN - MỚI - FULL NỘI THẤT- Ở NGAY.</h3>
														<div class="ad-upper-body">
															<span class="ad-item-condition">9 m² - 3 PN</span>
														</div>
														<div class="ad-upper-body">
															<span class="ad-item-price">
																<span class="ad-item-price-normal">2,6 tỷ
																</span>
															</span>
														</div>
													</div>
												</div>
											</a>
											<div class="ad-item-bottom">
												<div class="ad-item-wrapper-footer">
													<div class="ad-item-footer-text">
														<a class="ad-item-footer-wrapper-link">
															<div class="ad-item-footer-item float-left">
																<img class="ad-item-footer-item-image" height="16" width="16" src="{{ asset('image/icon/shop.svg') }}" alt="Chung">
															</div>
															<span class="ad-item-footer-item">
																<span style="margin-left: 2px;"></span>Căn hộ Topaz Elite - Topaz City - Topaz Home2</span>
														</a>
														&nbsp;
														<div class="ad-item-devider-wrapper"></div>
														&nbsp;
														<span class="ad-item-footer-item">
															4 phút trước
														</span>
														&nbsp;
														<span>
															<div class="ad-item-devider-wrapper"></div>
															&nbsp;
															<span class="ad-item-footer-item">Lâm Đồng</span>
														</span>
													</div>
												</div>
												<div class="save-ad-wrapper">
													<button class="btn-save-ad">
														<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
													</button>
												</div>
											</div>
										</li>
									</div>
								</ul>
							</div>
							<!-- grid mode -->
							<div class="grid-view-wrapper-list">
								<!-- 25 item -->
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-1.jpg') }}">
												<div class="grid-ad-item-image-group">
													<!-- <div class="grid-ad-item-image-number">
														<span></span>
													</div> -->
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Cần mua 360</span>
											</a>
											<span class="grid-ad-item-condition">360 m²</span>
											<div>
												<span class="price">1 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/user.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-icon"></span>
													<span class="grid-ad-item-sticky-text"> Tin ưu tiên</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Long An</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-2.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>5</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">1,14 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/user.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-icon"></span>
													<span class="grid-ad-item-sticky-text"> Tin ưu tiên</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-3.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>7</span>
													</div>
												</div>
												<div class="grid-ad-item-ribbon">HOT</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span class="font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</span>
											</a>
											<span class="grid-ad-item-condition">52.62 m² - 2 PN</span>
											<div>
												<span class="price">1,68 tỷ</span>
												<div class="common-style-label-price-grid">GIÁ TỐT</div>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/pro.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<!-- <span class="grid-ad-item-sticky-icon"></span> -->
													<span class="grid-ad-item-sticky-text"> 4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Lâm Đồng</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-4.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>4</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">2,6 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/shop.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-text">  4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-2.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>5</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">1,14 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/user.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-icon"></span>
													<span class="grid-ad-item-sticky-text"> Tin ưu tiên</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-3.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>7</span>
													</div>
												</div>
												<div class="grid-ad-item-ribbon">HOT</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span class="font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</span>
											</a>
											<span class="grid-ad-item-condition">52.62 m² - 2 PN</span>
											<div>
												<span class="price">1,68 tỷ</span>
												<div class="common-style-label-price-grid">GIÁ TỐT</div>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/pro.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<!-- <span class="grid-ad-item-sticky-icon"></span> -->
													<span class="grid-ad-item-sticky-text"> 4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Lâm Đồng</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-4.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>4</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">2,6 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/shop.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-text">  4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-2.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>5</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">1,14 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/user.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-icon"></span>
													<span class="grid-ad-item-sticky-text"> Tin ưu tiên</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-3.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>7</span>
													</div>
												</div>
												<div class="grid-ad-item-ribbon">HOT</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span class="font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</span>
											</a>
											<span class="grid-ad-item-condition">52.62 m² - 2 PN</span>
											<div>
												<span class="price">1,68 tỷ</span>
												<div class="common-style-label-price-grid">GIÁ TỐT</div>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/pro.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<!-- <span class="grid-ad-item-sticky-icon"></span> -->
													<span class="grid-ad-item-sticky-text"> 4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Lâm Đồng</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-4.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>4</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">2,6 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/shop.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-text">  4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-2.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>5</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">1,14 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/user.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-icon"></span>
													<span class="grid-ad-item-sticky-text"> Tin ưu tiên</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-3.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>7</span>
													</div>
												</div>
												<div class="grid-ad-item-ribbon">HOT</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span class="font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</span>
											</a>
											<span class="grid-ad-item-condition">52.62 m² - 2 PN</span>
											<div>
												<span class="price">1,68 tỷ</span>
												<div class="common-style-label-price-grid">GIÁ TỐT</div>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/pro.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<!-- <span class="grid-ad-item-sticky-icon"></span> -->
													<span class="grid-ad-item-sticky-text"> 4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Lâm Đồng</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-4.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>4</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">2,6 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/shop.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-text">  4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-2.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>5</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">1,14 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/user.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-icon"></span>
													<span class="grid-ad-item-sticky-text"> Tin ưu tiên</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-3.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>7</span>
													</div>
												</div>
												<div class="grid-ad-item-ribbon">HOT</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span class="font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</span>
											</a>
											<span class="grid-ad-item-condition">52.62 m² - 2 PN</span>
											<div>
												<span class="price">1,68 tỷ</span>
												<div class="common-style-label-price-grid">GIÁ TỐT</div>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/pro.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<!-- <span class="grid-ad-item-sticky-icon"></span> -->
													<span class="grid-ad-item-sticky-text"> 4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Lâm Đồng</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-4.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>4</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">2,6 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/shop.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-text">  4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-2.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>5</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">1,14 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/user.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-icon"></span>
													<span class="grid-ad-item-sticky-text"> Tin ưu tiên</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-3.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>7</span>
													</div>
												</div>
												<div class="grid-ad-item-ribbon">HOT</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span class="font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</span>
											</a>
											<span class="grid-ad-item-condition">52.62 m² - 2 PN</span>
											<div>
												<span class="price">1,68 tỷ</span>
												<div class="common-style-label-price-grid">GIÁ TỐT</div>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/pro.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<!-- <span class="grid-ad-item-sticky-icon"></span> -->
													<span class="grid-ad-item-sticky-text"> 4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Lâm Đồng</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-4.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>4</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">2,6 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/shop.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-text">  4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-2.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>5</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">1,14 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/user.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-icon"></span>
													<span class="grid-ad-item-sticky-text"> Tin ưu tiên</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-3.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>7</span>
													</div>
												</div>
												<div class="grid-ad-item-ribbon">HOT</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span class="font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</span>
											</a>
											<span class="grid-ad-item-condition">52.62 m² - 2 PN</span>
											<div>
												<span class="price">1,68 tỷ</span>
												<div class="common-style-label-price-grid">GIÁ TỐT</div>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/pro.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<!-- <span class="grid-ad-item-sticky-icon"></span> -->
													<span class="grid-ad-item-sticky-text"> 4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Lâm Đồng</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-4.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>4</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">2,6 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/shop.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-text">  4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-2.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>5</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">1,14 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/user.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-icon"></span>
													<span class="grid-ad-item-sticky-text"> Tin ưu tiên</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-3.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>7</span>
													</div>
												</div>
												<div class="grid-ad-item-ribbon">HOT</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span class="font-weight-bold">Bán Nhà Phan Văn Trị-Bình Thạnh 1Trệt1Lững 56,19m2</span>
											</a>
											<span class="grid-ad-item-condition">52.62 m² - 2 PN</span>
											<div>
												<span class="price">1,68 tỷ</span>
												<div class="common-style-label-price-grid">GIÁ TỐT</div>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/pro.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<!-- <span class="grid-ad-item-sticky-icon"></span> -->
													<span class="grid-ad-item-sticky-text"> 4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Lâm Đồng</span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-6 col-md-4">
									<div class="grid-wrapper-ad-item">
										<div class="grid-thumbnail-wrapper-ad-item">
											<a class="grid-thumbnail-image-ad-item" href="{{ route('frontend.purchase.item') }}">
												<img src="{{ asset('image/list-view-ads-4.jpg') }}">
												<div class="grid-ad-item-image-group">
													<div class="grid-ad-item-image-number">
														<span>4</span>
													</div>
												</div>
											</a>
											<div class="save-ad-wrapper">
												<button class="btn-save-ad">
													<img width="16" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
												</button>
											</div>
										</div>
										<div class="grid-ad-item-caption">
											<a href="#" class="grid-ad-item-title">
												<span>Bán 14 căn phòng trọ hiện đang cho thuê full phòng</span>
											</a>
											<span class="grid-ad-item-condition">32 m² - 3 PN²</span>
											<div>
												<span class="price">2,6 tỷ</span>
											</div>
										</div>
										<div class="grid-ad-item-footer">
											<a><img src="{{ asset('image/icon/shop.svg') }}" height="16" width="16"></a>
											<div class="devider-wrapper"></div>
											<div class="grid-ad-item-posted-time">
												<div class="grid-ad-item-sticky">
													<span class="grid-ad-item-sticky-text">  4 phút trước</span>
												</div>
											</div>
											<div class="devider-wrapper"></div>
											<span class="grid-ad-item-posted-time grid-ad-item-location">
												<span>Tp Hồ Chí Minh</span>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="ads-sense"></div>
						<div>
							<div class="pagination">
								<div class="pagination-item">
									<a href="#">
										<i class="pagination-left-icon-disable"></i>
									</a>
								</div>
								<div class="pagination-item">
									<a href="#">
										<span class="active">1</span>
									</a>
								</div>
								<div class="pagination-item">
									<a href="#">
										<span>2</span>
									</a>
								</div>
								<div class="pagination-item">
									<a href="#">
										<span>3</span>
									</a>
								</div>
								<div class="pagination-item">
									<a href="#">
										<span>4</span>
									</a>
								</div>
								<div class="pagination-item">
									<a href="#">
										<span>5</span>
									</a>
								</div>
								<div class="pagination-item">
									<a href="#">
										<span>6</span>
									</a>
								</div>
								<div class="pagination-item">
									<a href="#">
										<span>7</span>
									</a>
								</div>
								<div class="pagination-item">
									<a href="#">
										<span>8</span>
									</a>
								</div>
								<div class="pagination-item">
									<a href="#">
										<span>9</span>
									</a>
								</div>
								<div class="pagination-item">
									<a href="#">
										<i class="pagination-right-icon"></i>
									</a>
								</div>
							</div>
						</div>
					</div>

					<!-- GOOGLE ADS -->
					<div class="col-md-4 d-lg-block d-none"></div>
				</div>
				<div class="base-footer">
					<div class="seo-wrapper">
						<div class="cat-description col-12">
							<div class="content-cat-wrapper">
								<div class="block4 row">
									<div class="col-12">
										<h2 class="text-center"><strong>MUA BÁN BẤT ĐỘNG SẢN THÁNG 12/2020</strong></h2>
									</div>
								</div>
								<div class="block2 row">
									<div class="col-md-8">
										<p style="text-align:justify"><strong>Bất động sản </strong> hứa hẹn là thị trường đầy tiềm năng khi ngày càng nhận được nhiều sự đầu tư của trong và ngoài nước. Với hàng loạt các loại hình từ nhà đất, đất nền, căn hộ chung cư, phòng trọ đến văn phòng, mặt bằng kinh doanh,... đều trở thành tiêu điểm chú ý của <strong>bất động sản </strong>. Đặc biệt là trong diễn biến dịch bệnh phức tạp, việc <strong>mua bán bất động sản </strong> trở nên dè dặt và cẩn thận hơn bao giờ hết.</p>
										<p style="text-align:justify">Để cập nhật những <strong>thông tin bất động sản mới nhất</strong>, truy cập ngay Chợ Tốt Nhà để theo dõi <strong>giá bất động sản </strong> tháng 12/2020. Việc tìm kiếm bất động sản trở nên dễ dàng hơn khi người dùng có thể chọn lọc theo từng địa điểm, giá cả và loại hình mong muốn. Chợ Tốt Nhà sẽ đề xuất các tin đăng đáp ứng theo các tiêu chí để dễ dàng so sánh.</p>
										<p style="text-align:justify">Các keywords được tìm kiếm nhiều hiện nay: <strong>bất động sản mới nhất</strong>, <strong>bđs </strong>, <strong>batdongsan </strong>,...</p>
									</div>
									<div class="col-md-4">
										<p><img class="w-100 h-auto" src="{{ asset('image/bat-dong-san.jpg') }}"></p>
									</div>
								</div>
								<div class="block4 row">
									<div class="col-12">
										<p style="text-align:justify">Tùy vào mục đích sử dụng, diện tích và vị trí của bất động sản sẽ có sự chênh lệch về giá mua bán. Để việc <strong>mua bán bất động sản </strong> trở nên dễ dàng, thuận tiện và an toàn hơn, người mua cần chú ý các điểm sau đây:</p>

										<p style="text-align:justify">✅ Xem xét năng lực của chủ đầu tư, qua đó tìm hiểu dự án đã hoàn thiện cơ sở hạ tầng đạt chất lượng hay chưa?</p>

										<p style="text-align:justify">✅ Kiểm tra tính pháp lý của bất động sản. Chú ý đến các vấn đề về chứng nhận quyền sở hữu bất động sản, giấy phép xây dựng,...</p>

										<p style="text-align:justify">✅ Quy trình thực hiện giấy tờ giao dịch tuân thủ theo pháp luật với đầy đủ trình tự. Nếu có người đại diện đứng ra thực hiện giao dịch thì cần nhận được sự ủy quyền hợp pháp.</p>

										<p style="text-align:justify">✅ Thương lượng về các vấn đề thay đổi trong bản quy hoạch và đưa vào văn bản chính thức.</p>

										<p style="text-align:justify">✅ Các điều khoản trong hợp đồng cần phải được quy định rõ ràng và chi tiết: về giá bán bất động sản , cách thanh toán, thời hạn thanh toán, thời hạn bàn giao, các mức bồi thường thiệt hại,...</p>

										<p style="text-align:justify">✅ Không nên mua bất động sản có sổ đỏ chung để tránh vấn đề tách sổ sau này.</p>

										<p style="text-align:justify">Để việc tìm <strong>mua bất động sản </strong> trở nên dễ dàng và thuận tiện hơn, bạn truy cập vào Chợ Tốt Nhà và lọc theo các tiêu chí giá cả, khu vực, vị trí, loại hình, diện tích,... Chợ Tốt Nhà sẽ nhanh chóng đề xuất các tin đăng phù hợp.</p>

										<p style="text-align:right"><em><strong>--- Chợ Tốt Nhà ---</strong></em></p>
									</div>
								</div>
							</div>
							<p class="text-center">
								<a class="btn-see-more">Mở rộng</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<button class="scroll-top">
			<span></span>
		</button>
	</main>

	@include('frontend.common.modals.filter.modal-filter')

	@include('frontend.common.modals.filter.modal-filter_need')

	@include('frontend.common.modals.filter.modal-filter_type')

	@include('frontend.common.modals.filter.modal-filter_price')

	@include('frontend.common.modals.filter.modal-filter_project')

	@include('frontend.common.modals.filter.modal-filter_video-type')

@endsection