@extends('frontend.layouts.news.app')
@section('content')
	<div class="title-page">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<h1 class="text-center" style="font-size: 32px;">Tin Tức Bất Động Sản</h1>
					<div class="description-info text-center"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="content-page">
		<div class="post-featured">
			<div class="container">
				<div class="item-post">
					<a href="{{ route('frontend.news.post') }}">
						<div class="row">
							<div class="col-md-6">
								<img src="{{ asset('image/f4e0adcb-gia-bat-dong-san-ha-noi-thang-10-2020.png') }}">
							</div>
							<div class="col-md-6">
								<div class="post-meta">
									<h3 class="title-post fs-24 ff-bold">CẬP NHẬT GIÁ BẤT ĐỘNG SẢN TẠI HÀ NỘI THÁNG 10/2020</h3>
								</div>
							</div>
						</div>
					</a>
					<div class="post-ab">
						<div class="row">
							<div class="col-md-6"></div>
							<div class="col-md-6">
								<div class="post-meta">
									<div class="excerpt-info">
										<div class="excerpt-info-text">
											Theo số liệu ghi nhận từ&nbsp;Chợ Tốt Nhà, giá mua bán nhà đất và căn hộ tại Hà Nội vào tháng 10/2020 có sự khác biệt giữa các quận. Thông tin cụ thể sẽ được cập nhật trong bài viết dưới đây: GIÁ BÁN NHÀ NGUYÊN CĂN CÁC QUẬN Ở HÀ NỘI Tháng 10/2020, nhà đất ở Quận Hai Bà Trưng có lượng tìm kiếm tăng 10% chỉ trong một tháng. Điểm thu hút ở khu vực này là tuyến đường bộ trên cao dọc theo Vành Đai 2, từ cầu Vĩnh Tuy đến Mai Động, từ đường Trường Chinh đến Ngã Tư Sở được nâng cấp mở rộng. Điều này giúp cho đường Minh Khai nói riêng và quận Hai Bà Trưng nói chung có diện mạo mới, hiện đại và đồng bộ. Hơn thế nữa, những dự án quy hoạch giao thông gần đây đã giúp việc di chuyển đến các tỉnh lân cận như Hải Phòng, Hưng Yên trở nên thuận tiện hơn trước. Giá bán nhà nguyên căn ở quận Hai Bà Trưng dao động ở mức 91.4 triệu/m2. Một cái tên khác được người mua nhà đất quan tâm nhiều không kém là Quận Long Biên. Sở hữu quỹ đất rộng nhất trong 12 quận ở Hà Nội, nơi đây đang nắm bắt lợi thế trở thành “tâm điểm” ở thời điểm hiện tại trong lĩnh vực bất động sản. Theo quy hoạch N10 của Thủ đô, Long Biên sẽ chuyển đổi hoàn toàn mục đích sử dụng từ nhà máy, kho bãi, công nghiệp thành khu công viên, khu vui chơi và thương mại, nhằm hướng đến mục tiêu thay đổi toàn bộ diện mạo và biến quận Long Biên trở thành khu đô thị xa hoa của Hà Nội trong 10 – 20 […]
										</div>
									</div>
									<ul>
										<li>
											<a href="{{ route('frontend.news.page') }}">Thị trường</a>
											· 31/10/2020
										</li>
									</ul>
									<hr class="d-none d-md-block">
									<ul class="relate-of-feature-post d-none d-md-block">
										<li>
											<a href="{{ route('frontend.news.post') }}">CẬP NHẬT GIÁ THỊ TRƯỜNG BẤT ĐỘNG SẢN TP.HCM THÁNG 10/2020</a>
										</li>
										<li>
											<a href="{{ route('frontend.news.post') }}">CẬP NHẬT GIÁ THỊ TRƯỜNG CHO THUÊ TP.HCM THÁNG 10/2020</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="list-post">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="cate-group">
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/e6571d18-tan-co-giao-dau-768x292.png') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Hết cảnh: Kẻ thừa phòng trọ, người lần không ra</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Theo số liệu ghi nhận từ&nbsp;Chợ Tốt Nhà, giá mua bán nhà đất và căn hộ tại Hà Nội vào tháng 10/2020 có sự khác biệt giữa các quận. Thông tin cụ thể sẽ được cập nhật trong bài viết dưới đây: GIÁ BÁN NHÀ NGUYÊN CĂN CÁC QUẬN Ở HÀ NỘI Tháng 10/2020, nhà đất ở Quận Hai Bà Trưng có lượng tìm kiếm tăng 10% chỉ trong một tháng. Điểm thu hút ở khu vực này là tuyến đường bộ trên cao dọc theo Vành Đai 2, từ cầu Vĩnh Tuy đến Mai Động, từ đường Trường Chinh đến Ngã Tư Sở được nâng cấp mở rộng. Điều này giúp cho đường Minh Khai nói riêng và quận Hai Bà Trưng nói chung có diện mạo mới, hiện đại và đồng bộ. Hơn thế nữa, những dự án quy hoạch giao thông gần đây đã giúp việc di chuyển đến các tỉnh lân cận như Hải Phòng, Hưng Yên trở nên thuận tiện hơn trước. Giá bán nhà nguyên căn ở quận Hai Bà Trưng dao động ở mức 91.4 triệu/m2. Một cái tên khác được người mua nhà đất quan tâm nhiều không kém là Quận Long Biên. Sở hữu quỹ đất rộng nhất trong 12 quận ở Hà Nội, nơi đây đang nắm bắt lợi thế trở thành “tâm điểm” ở thời điểm hiện tại trong lĩnh vực bất động sản. Theo quy hoạch N10 của Thủ đô, Long Biên sẽ chuyển đổi hoàn toàn mục đích sử dụng từ nhà máy, kho bãi, công nghiệp thành khu công viên, khu vui chơi và thương mại, nhằm hướng đến mục tiêu thay đổi toàn bộ diện mạo và biến quận Long Biên trở thành khu đô thị xa hoa của Hà Nội trong 10 – 20 […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/f667eedd-index-nha-q3-768x576.png') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Hết cảnh: Kẻ thừa phòng trọ, người lần không ra</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Theo số liệu mới nhất từ chuyên trang Chợ Tốt Nhà khảo sát trên 40,000 tin đăng phòng trọ ở TP HCM trong tháng 10/2020, thị trường cho thuê phòng trọ liên tục sôi động từ tháng 8 đến nay với nguồn cung phòng trọ đầy đủ tiện nghi dồi dào. Giá thuê trung bình từ 1-4 triệu đồng/tháng Sau hai đợt dịch Covid-19, nền kinh tế bị ảnh hưởng và nhiều trường học phải tạm đóng cửa khiến cho việc đến lớp của nhiều học sinh – sinh viên trên địa bàn TP.HCM bị gián đoạn. Tuy nhiên, nhu cầu nhà ở vẫn là nhu cầu tồn tại thường trực, đặc biệt khi mùa thi Đại học đến gần. Từ tháng 8 trở đi, tình hình dịch bệnh được kiểm soát, các trường học bắt đầu mở cửa trở lại và kỳ thi Đại học được tổ chức, chuyên trang Chợ Tốt Nhà đón nhận lượng lớn lượt tìm kiếm đổ về nhóm sản phẩm phòng trọ ở TP HCM cho thuê. Đại diện chuyên trang này nhận định, trung bình một căn phòng trọ cơ bản và đầy đủ nội thất có diện tích khoảng 20 mét vuông, phù hợp cho nhu cầu nhà ở từ 1-2 người. Giá thuê trung bình hàng tháng của những căn này dao động từ 1 triệu đến 4 triệu đồng tùy từng khu vực. Theo phân khúc trên, hiện có 16/22 quận huyện trên địa bàn TP.HCM có giá thuê trung bình dưới 3 triệu đồng/tháng. Nhóm các quận ở trung tâm, có lợi thế về địa lý, giao thông cực thuận tiện và đón nhận nhiều tiện ích thương mại, giải trí, mua sắm như quận 2, quận 3, quận 10, … có giá thuê ở mức 3-4 triệu đồng/tháng. Những khu vực nằm […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Homepage-Index-T9-800x600-300x225.png') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Hết cảnh: Kẻ thừa phòng trọ, người lần không ra</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														
														Theo thống kê của Chợ Tốt Nhà, thông tin giá thị trường cho thuê TPHCM tháng 9/2020 của hai phân khúc nhà nguyên căn và căn hộ chung cư theo từng quận/huyện ở TP.HCM được cập nhật mới nhất ở bài viết bên dưới: GIÁ CHO THUÊ NHÀ NGUYÊN CĂN CÁC QUẬN TP. HCM Số liệu ghi nhận từ Chợ Tốt Nhà cho thấy, Quận 12 là một trong những khu vực được người thuê nhà nguyên căn quan tâm nhiều nhất trong tháng 9/2020. Đây là địa điểm tập trung khu công nghệ phần mềm Quang Trung, các doanh nghiệp, khu chế xuất, và nhiều trung tâm nghiên cứu. Hệ thống tiện ích dân sinh tại khu vực này được đầu tư phát triển để phù hợp cho công nhân, nhân dân lao động. Giá thuê tại đây dao động ở mức 5 triệu đồng/căn, tương đối thấp so với mặt bằng chung các quận. Danh sách nhà đất cho thuê tại TPHCM: Với giá thuê khoảng 7 triệu đồng/ căn, quận 8 cũng là một khu vực thu hút đông đảo lượng người tìm thuê. Là quận trung tâm sở hữu hệ thống kênh rạch nhiều nhất thành phố, khu vực này sở hữu hạ tầng giao thông đường thủy và đường bộ phát triển. Nhiều tuyến đường hiện đang được chú trọng nâng cấp, đầu tư xây dựng như đường vành đai 2, đường Tạ Quang Bửu, tuyến Metro số 3A, giúp người dân thuận tiện di chuyển sang các quận lân cận. Các con đường nổi bật, được nhiều người tìm thuê nhất gồm đường Phạm Hùng, đường Âu Dương Lân, đường Phạm Thế Hiển. Một khu vực khác cũng nhận được sự quan tâm từ nhiều người tìm thuê nhà là Quận Gò Vấp – […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="cate-group">
							<h2 class="title-group">Thị trường</h2>
							<!-- 5 items -->
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Cau-Giay-1-300x188.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Quận Cầu Giấy đang được giới đầu tư địa ốc rất để tâm với những dự án mới mở. Bài viết liệt kê danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020 đáng chú ý nhất hiện nay. Nếu TP.HCM có Landmark 81 thì ở Hà Nội có Landmark 72 toạ lạc tại quận Cầu Giấy cũng không hề kém cạnh cả về độ sầm uất lẫn các tiện nghi mang đến. Ngoài ra, khu vực này vẫn còn có rất nhiều dự án bất động sản khác, điển hình là những khu chung cư cao tầng cũng đang nhận được rất nhiều sự quan tâm của giới đầu tư và cá nhân.  Bài viết sau đây của Chợ Tốt sẽ liệt kê danh sách chung cư quận Cầu Giấy đang mở bán để bạn có thêm thông tin cần thiết cho việc đầu tư. Bắt đầu ngay! Tham khảo danh sách chung cư tại Hà Nội: Địa ốc khu vực quận Cầu Giấy đang rất được quan tâm 1. Tổng quan thị trường chung cư quận Cầu Giấy Sở hữu cơ sở hạ tầng mới đồng bộ và hiện đại, quận Cầu Giấy được đánh giá là thị trường bất động sản hấp dẫn bậc nhất Thủ đô với nhiều dự án tiềm năng. Đa phần các khu chung cư này đều thuộc phân khúc cao cấp, phục vụ tầng lớp có thu nhập trung bình khá trở lên. Bên cạnh đó, khu vực này cũng có nhiều công ty, trụ sở, văn phòng làm việc, gần chợ, bệnh viện, trường học và các trung tâm thương mại… Do đó, danh sách chung cư quận Cầu Giấy cũng đang tăng dần lên theo thời gian nhằm cung cấp nhiều không gian hơn cho người dân.  Ngoài […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Binh-Thanh-2-300x200.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Muốn đầu tư, hãy tham khảo danh sách chung cư quận Bình Thạnh này</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Bình Thạnh là một nơi lý tưởng để đầu tư bất động sản cao cấp. Bài viết tổng hợp danh sách 3 dự án chung cư nổi bật ...
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Cau-Giay-1-300x188.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Quận Cầu Giấy đang được giới đầu tư địa ốc rất để tâm với những dự án mới mở. Bài viết liệt kê danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020 đáng chú ý nhất hiện nay. Nếu TP.HCM có Landmark 81 thì ở Hà Nội có Landmark 72 toạ lạc tại quận Cầu Giấy cũng không hề kém cạnh cả về độ sầm uất lẫn các tiện nghi mang đến. Ngoài ra, khu vực này vẫn còn có rất nhiều dự án bất động sản khác, điển hình là những khu chung cư cao tầng cũng đang nhận được rất nhiều sự quan tâm của giới đầu tư và cá nhân.  Bài viết sau đây của Chợ Tốt sẽ liệt kê danh sách chung cư quận Cầu Giấy đang mở bán để bạn có thêm thông tin cần thiết cho việc đầu tư. Bắt đầu ngay! Tham khảo danh sách chung cư tại Hà Nội: Địa ốc khu vực quận Cầu Giấy đang rất được quan tâm 1. Tổng quan thị trường chung cư quận Cầu Giấy Sở hữu cơ sở hạ tầng mới đồng bộ và hiện đại, quận Cầu Giấy được đánh giá là thị trường bất động sản hấp dẫn bậc nhất Thủ đô với nhiều dự án tiềm năng. Đa phần các khu chung cư này đều thuộc phân khúc cao cấp, phục vụ tầng lớp có thu nhập trung bình khá trở lên. Bên cạnh đó, khu vực này cũng có nhiều công ty, trụ sở, văn phòng làm việc, gần chợ, bệnh viện, trường học và các trung tâm thương mại… Do đó, danh sách chung cư quận Cầu Giấy cũng đang tăng dần lên theo thời gian nhằm cung cấp nhiều không gian hơn cho người dân.  Ngoài […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Binh-Thanh-2-300x200.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Muốn đầu tư, hãy tham khảo danh sách chung cư quận Bình Thạnh này</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Bình Thạnh là một nơi lý tưởng để đầu tư bất động sản cao cấp. Bài viết tổng hợp danh sách 3 dự án chung cư nổi bật ...
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Cau-Giay-1-300x188.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Quận Cầu Giấy đang được giới đầu tư địa ốc rất để tâm với những dự án mới mở. Bài viết liệt kê danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020 đáng chú ý nhất hiện nay. Nếu TP.HCM có Landmark 81 thì ở Hà Nội có Landmark 72 toạ lạc tại quận Cầu Giấy cũng không hề kém cạnh cả về độ sầm uất lẫn các tiện nghi mang đến. Ngoài ra, khu vực này vẫn còn có rất nhiều dự án bất động sản khác, điển hình là những khu chung cư cao tầng cũng đang nhận được rất nhiều sự quan tâm của giới đầu tư và cá nhân.  Bài viết sau đây của Chợ Tốt sẽ liệt kê danh sách chung cư quận Cầu Giấy đang mở bán để bạn có thêm thông tin cần thiết cho việc đầu tư. Bắt đầu ngay! Tham khảo danh sách chung cư tại Hà Nội: Địa ốc khu vực quận Cầu Giấy đang rất được quan tâm 1. Tổng quan thị trường chung cư quận Cầu Giấy Sở hữu cơ sở hạ tầng mới đồng bộ và hiện đại, quận Cầu Giấy được đánh giá là thị trường bất động sản hấp dẫn bậc nhất Thủ đô với nhiều dự án tiềm năng. Đa phần các khu chung cư này đều thuộc phân khúc cao cấp, phục vụ tầng lớp có thu nhập trung bình khá trở lên. Bên cạnh đó, khu vực này cũng có nhiều công ty, trụ sở, văn phòng làm việc, gần chợ, bệnh viện, trường học và các trung tâm thương mại… Do đó, danh sách chung cư quận Cầu Giấy cũng đang tăng dần lên theo thời gian nhằm cung cấp nhiều không gian hơn cho người dân.  Ngoài […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div>
							<div class="row justify-content-end">
								<div class="col-md-7 text-md-left text-center">
									<a class="btn btn-read-mode" href="#">Xem thêm</a>
								</div>
							</div>
						</div>
						<div class="cate-group">
							<h2 class="title-group">Cẩm nang</h2>
							<!-- 5 items -->
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Cau-Giay-1-300x188.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Quận Cầu Giấy đang được giới đầu tư địa ốc rất để tâm với những dự án mới mở. Bài viết liệt kê danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020 đáng chú ý nhất hiện nay. Nếu TP.HCM có Landmark 81 thì ở Hà Nội có Landmark 72 toạ lạc tại quận Cầu Giấy cũng không hề kém cạnh cả về độ sầm uất lẫn các tiện nghi mang đến. Ngoài ra, khu vực này vẫn còn có rất nhiều dự án bất động sản khác, điển hình là những khu chung cư cao tầng cũng đang nhận được rất nhiều sự quan tâm của giới đầu tư và cá nhân.  Bài viết sau đây của Chợ Tốt sẽ liệt kê danh sách chung cư quận Cầu Giấy đang mở bán để bạn có thêm thông tin cần thiết cho việc đầu tư. Bắt đầu ngay! Tham khảo danh sách chung cư tại Hà Nội: Địa ốc khu vực quận Cầu Giấy đang rất được quan tâm 1. Tổng quan thị trường chung cư quận Cầu Giấy Sở hữu cơ sở hạ tầng mới đồng bộ và hiện đại, quận Cầu Giấy được đánh giá là thị trường bất động sản hấp dẫn bậc nhất Thủ đô với nhiều dự án tiềm năng. Đa phần các khu chung cư này đều thuộc phân khúc cao cấp, phục vụ tầng lớp có thu nhập trung bình khá trở lên. Bên cạnh đó, khu vực này cũng có nhiều công ty, trụ sở, văn phòng làm việc, gần chợ, bệnh viện, trường học và các trung tâm thương mại… Do đó, danh sách chung cư quận Cầu Giấy cũng đang tăng dần lên theo thời gian nhằm cung cấp nhiều không gian hơn cho người dân.  Ngoài […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Binh-Thanh-2-300x200.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Muốn đầu tư, hãy tham khảo danh sách chung cư quận Bình Thạnh này</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Bình Thạnh là một nơi lý tưởng để đầu tư bất động sản cao cấp. Bài viết tổng hợp danh sách 3 dự án chung cư nổi bật ...
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Cau-Giay-1-300x188.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Quận Cầu Giấy đang được giới đầu tư địa ốc rất để tâm với những dự án mới mở. Bài viết liệt kê danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020 đáng chú ý nhất hiện nay. Nếu TP.HCM có Landmark 81 thì ở Hà Nội có Landmark 72 toạ lạc tại quận Cầu Giấy cũng không hề kém cạnh cả về độ sầm uất lẫn các tiện nghi mang đến. Ngoài ra, khu vực này vẫn còn có rất nhiều dự án bất động sản khác, điển hình là những khu chung cư cao tầng cũng đang nhận được rất nhiều sự quan tâm của giới đầu tư và cá nhân.  Bài viết sau đây của Chợ Tốt sẽ liệt kê danh sách chung cư quận Cầu Giấy đang mở bán để bạn có thêm thông tin cần thiết cho việc đầu tư. Bắt đầu ngay! Tham khảo danh sách chung cư tại Hà Nội: Địa ốc khu vực quận Cầu Giấy đang rất được quan tâm 1. Tổng quan thị trường chung cư quận Cầu Giấy Sở hữu cơ sở hạ tầng mới đồng bộ và hiện đại, quận Cầu Giấy được đánh giá là thị trường bất động sản hấp dẫn bậc nhất Thủ đô với nhiều dự án tiềm năng. Đa phần các khu chung cư này đều thuộc phân khúc cao cấp, phục vụ tầng lớp có thu nhập trung bình khá trở lên. Bên cạnh đó, khu vực này cũng có nhiều công ty, trụ sở, văn phòng làm việc, gần chợ, bệnh viện, trường học và các trung tâm thương mại… Do đó, danh sách chung cư quận Cầu Giấy cũng đang tăng dần lên theo thời gian nhằm cung cấp nhiều không gian hơn cho người dân.  Ngoài […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Binh-Thanh-2-300x200.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Muốn đầu tư, hãy tham khảo danh sách chung cư quận Bình Thạnh này</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Bình Thạnh là một nơi lý tưởng để đầu tư bất động sản cao cấp. Bài viết tổng hợp danh sách 3 dự án chung cư nổi bật ...
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Cau-Giay-1-300x188.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Quận Cầu Giấy đang được giới đầu tư địa ốc rất để tâm với những dự án mới mở. Bài viết liệt kê danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020 đáng chú ý nhất hiện nay. Nếu TP.HCM có Landmark 81 thì ở Hà Nội có Landmark 72 toạ lạc tại quận Cầu Giấy cũng không hề kém cạnh cả về độ sầm uất lẫn các tiện nghi mang đến. Ngoài ra, khu vực này vẫn còn có rất nhiều dự án bất động sản khác, điển hình là những khu chung cư cao tầng cũng đang nhận được rất nhiều sự quan tâm của giới đầu tư và cá nhân.  Bài viết sau đây của Chợ Tốt sẽ liệt kê danh sách chung cư quận Cầu Giấy đang mở bán để bạn có thêm thông tin cần thiết cho việc đầu tư. Bắt đầu ngay! Tham khảo danh sách chung cư tại Hà Nội: Địa ốc khu vực quận Cầu Giấy đang rất được quan tâm 1. Tổng quan thị trường chung cư quận Cầu Giấy Sở hữu cơ sở hạ tầng mới đồng bộ và hiện đại, quận Cầu Giấy được đánh giá là thị trường bất động sản hấp dẫn bậc nhất Thủ đô với nhiều dự án tiềm năng. Đa phần các khu chung cư này đều thuộc phân khúc cao cấp, phục vụ tầng lớp có thu nhập trung bình khá trở lên. Bên cạnh đó, khu vực này cũng có nhiều công ty, trụ sở, văn phòng làm việc, gần chợ, bệnh viện, trường học và các trung tâm thương mại… Do đó, danh sách chung cư quận Cầu Giấy cũng đang tăng dần lên theo thời gian nhằm cung cấp nhiều không gian hơn cho người dân.  Ngoài […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div>
							<div class="row justify-content-end">
								<div class="col-md-7 text-md-left text-center">
									<a class="btn btn-read-mode" href="#">Xem thêm</a>
								</div>
							</div>
						</div>
						<div class="cate-group">
							<h2 class="title-group">Phong thủy</h2>
							<!-- 5 items -->
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Cau-Giay-1-300x188.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Quận Cầu Giấy đang được giới đầu tư địa ốc rất để tâm với những dự án mới mở. Bài viết liệt kê danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020 đáng chú ý nhất hiện nay. Nếu TP.HCM có Landmark 81 thì ở Hà Nội có Landmark 72 toạ lạc tại quận Cầu Giấy cũng không hề kém cạnh cả về độ sầm uất lẫn các tiện nghi mang đến. Ngoài ra, khu vực này vẫn còn có rất nhiều dự án bất động sản khác, điển hình là những khu chung cư cao tầng cũng đang nhận được rất nhiều sự quan tâm của giới đầu tư và cá nhân.  Bài viết sau đây của Chợ Tốt sẽ liệt kê danh sách chung cư quận Cầu Giấy đang mở bán để bạn có thêm thông tin cần thiết cho việc đầu tư. Bắt đầu ngay! Tham khảo danh sách chung cư tại Hà Nội: Địa ốc khu vực quận Cầu Giấy đang rất được quan tâm 1. Tổng quan thị trường chung cư quận Cầu Giấy Sở hữu cơ sở hạ tầng mới đồng bộ và hiện đại, quận Cầu Giấy được đánh giá là thị trường bất động sản hấp dẫn bậc nhất Thủ đô với nhiều dự án tiềm năng. Đa phần các khu chung cư này đều thuộc phân khúc cao cấp, phục vụ tầng lớp có thu nhập trung bình khá trở lên. Bên cạnh đó, khu vực này cũng có nhiều công ty, trụ sở, văn phòng làm việc, gần chợ, bệnh viện, trường học và các trung tâm thương mại… Do đó, danh sách chung cư quận Cầu Giấy cũng đang tăng dần lên theo thời gian nhằm cung cấp nhiều không gian hơn cho người dân.  Ngoài […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Binh-Thanh-2-300x200.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Muốn đầu tư, hãy tham khảo danh sách chung cư quận Bình Thạnh này</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Bình Thạnh là một nơi lý tưởng để đầu tư bất động sản cao cấp. Bài viết tổng hợp danh sách 3 dự án chung cư nổi bật ...
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Cau-Giay-1-300x188.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Quận Cầu Giấy đang được giới đầu tư địa ốc rất để tâm với những dự án mới mở. Bài viết liệt kê danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020 đáng chú ý nhất hiện nay. Nếu TP.HCM có Landmark 81 thì ở Hà Nội có Landmark 72 toạ lạc tại quận Cầu Giấy cũng không hề kém cạnh cả về độ sầm uất lẫn các tiện nghi mang đến. Ngoài ra, khu vực này vẫn còn có rất nhiều dự án bất động sản khác, điển hình là những khu chung cư cao tầng cũng đang nhận được rất nhiều sự quan tâm của giới đầu tư và cá nhân.  Bài viết sau đây của Chợ Tốt sẽ liệt kê danh sách chung cư quận Cầu Giấy đang mở bán để bạn có thêm thông tin cần thiết cho việc đầu tư. Bắt đầu ngay! Tham khảo danh sách chung cư tại Hà Nội: Địa ốc khu vực quận Cầu Giấy đang rất được quan tâm 1. Tổng quan thị trường chung cư quận Cầu Giấy Sở hữu cơ sở hạ tầng mới đồng bộ và hiện đại, quận Cầu Giấy được đánh giá là thị trường bất động sản hấp dẫn bậc nhất Thủ đô với nhiều dự án tiềm năng. Đa phần các khu chung cư này đều thuộc phân khúc cao cấp, phục vụ tầng lớp có thu nhập trung bình khá trở lên. Bên cạnh đó, khu vực này cũng có nhiều công ty, trụ sở, văn phòng làm việc, gần chợ, bệnh viện, trường học và các trung tâm thương mại… Do đó, danh sách chung cư quận Cầu Giấy cũng đang tăng dần lên theo thời gian nhằm cung cấp nhiều không gian hơn cho người dân.  Ngoài […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Binh-Thanh-2-300x200.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Muốn đầu tư, hãy tham khảo danh sách chung cư quận Bình Thạnh này</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Bình Thạnh là một nơi lý tưởng để đầu tư bất động sản cao cấp. Bài viết tổng hợp danh sách 3 dự án chung cư nổi bật ...
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Cau-Giay-1-300x188.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Quận Cầu Giấy đang được giới đầu tư địa ốc rất để tâm với những dự án mới mở. Bài viết liệt kê danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020 đáng chú ý nhất hiện nay. Nếu TP.HCM có Landmark 81 thì ở Hà Nội có Landmark 72 toạ lạc tại quận Cầu Giấy cũng không hề kém cạnh cả về độ sầm uất lẫn các tiện nghi mang đến. Ngoài ra, khu vực này vẫn còn có rất nhiều dự án bất động sản khác, điển hình là những khu chung cư cao tầng cũng đang nhận được rất nhiều sự quan tâm của giới đầu tư và cá nhân.  Bài viết sau đây của Chợ Tốt sẽ liệt kê danh sách chung cư quận Cầu Giấy đang mở bán để bạn có thêm thông tin cần thiết cho việc đầu tư. Bắt đầu ngay! Tham khảo danh sách chung cư tại Hà Nội: Địa ốc khu vực quận Cầu Giấy đang rất được quan tâm 1. Tổng quan thị trường chung cư quận Cầu Giấy Sở hữu cơ sở hạ tầng mới đồng bộ và hiện đại, quận Cầu Giấy được đánh giá là thị trường bất động sản hấp dẫn bậc nhất Thủ đô với nhiều dự án tiềm năng. Đa phần các khu chung cư này đều thuộc phân khúc cao cấp, phục vụ tầng lớp có thu nhập trung bình khá trở lên. Bên cạnh đó, khu vực này cũng có nhiều công ty, trụ sở, văn phòng làm việc, gần chợ, bệnh viện, trường học và các trung tâm thương mại… Do đó, danh sách chung cư quận Cầu Giấy cũng đang tăng dần lên theo thời gian nhằm cung cấp nhiều không gian hơn cho người dân.  Ngoài […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div>
							<div class="row justify-content-end">
								<div class="col-md-7 text-md-left text-center">
									<a class="btn btn-read-mode" href="#">Xem thêm</a>
								</div>
							</div>
						</div>
						<div class="cate-group">
							<h2 class="title-group">Thủ tục</h2>
							<!-- 5 items -->
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Cau-Giay-1-300x188.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Quận Cầu Giấy đang được giới đầu tư địa ốc rất để tâm với những dự án mới mở. Bài viết liệt kê danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020 đáng chú ý nhất hiện nay. Nếu TP.HCM có Landmark 81 thì ở Hà Nội có Landmark 72 toạ lạc tại quận Cầu Giấy cũng không hề kém cạnh cả về độ sầm uất lẫn các tiện nghi mang đến. Ngoài ra, khu vực này vẫn còn có rất nhiều dự án bất động sản khác, điển hình là những khu chung cư cao tầng cũng đang nhận được rất nhiều sự quan tâm của giới đầu tư và cá nhân.  Bài viết sau đây của Chợ Tốt sẽ liệt kê danh sách chung cư quận Cầu Giấy đang mở bán để bạn có thêm thông tin cần thiết cho việc đầu tư. Bắt đầu ngay! Tham khảo danh sách chung cư tại Hà Nội: Địa ốc khu vực quận Cầu Giấy đang rất được quan tâm 1. Tổng quan thị trường chung cư quận Cầu Giấy Sở hữu cơ sở hạ tầng mới đồng bộ và hiện đại, quận Cầu Giấy được đánh giá là thị trường bất động sản hấp dẫn bậc nhất Thủ đô với nhiều dự án tiềm năng. Đa phần các khu chung cư này đều thuộc phân khúc cao cấp, phục vụ tầng lớp có thu nhập trung bình khá trở lên. Bên cạnh đó, khu vực này cũng có nhiều công ty, trụ sở, văn phòng làm việc, gần chợ, bệnh viện, trường học và các trung tâm thương mại… Do đó, danh sách chung cư quận Cầu Giấy cũng đang tăng dần lên theo thời gian nhằm cung cấp nhiều không gian hơn cho người dân.  Ngoài […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Binh-Thanh-2-300x200.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Muốn đầu tư, hãy tham khảo danh sách chung cư quận Bình Thạnh này</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Bình Thạnh là một nơi lý tưởng để đầu tư bất động sản cao cấp. Bài viết tổng hợp danh sách 3 dự án chung cư nổi bật ...
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Cau-Giay-1-300x188.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Quận Cầu Giấy đang được giới đầu tư địa ốc rất để tâm với những dự án mới mở. Bài viết liệt kê danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020 đáng chú ý nhất hiện nay. Nếu TP.HCM có Landmark 81 thì ở Hà Nội có Landmark 72 toạ lạc tại quận Cầu Giấy cũng không hề kém cạnh cả về độ sầm uất lẫn các tiện nghi mang đến. Ngoài ra, khu vực này vẫn còn có rất nhiều dự án bất động sản khác, điển hình là những khu chung cư cao tầng cũng đang nhận được rất nhiều sự quan tâm của giới đầu tư và cá nhân.  Bài viết sau đây của Chợ Tốt sẽ liệt kê danh sách chung cư quận Cầu Giấy đang mở bán để bạn có thêm thông tin cần thiết cho việc đầu tư. Bắt đầu ngay! Tham khảo danh sách chung cư tại Hà Nội: Địa ốc khu vực quận Cầu Giấy đang rất được quan tâm 1. Tổng quan thị trường chung cư quận Cầu Giấy Sở hữu cơ sở hạ tầng mới đồng bộ và hiện đại, quận Cầu Giấy được đánh giá là thị trường bất động sản hấp dẫn bậc nhất Thủ đô với nhiều dự án tiềm năng. Đa phần các khu chung cư này đều thuộc phân khúc cao cấp, phục vụ tầng lớp có thu nhập trung bình khá trở lên. Bên cạnh đó, khu vực này cũng có nhiều công ty, trụ sở, văn phòng làm việc, gần chợ, bệnh viện, trường học và các trung tâm thương mại… Do đó, danh sách chung cư quận Cầu Giấy cũng đang tăng dần lên theo thời gian nhằm cung cấp nhiều không gian hơn cho người dân.  Ngoài […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Binh-Thanh-2-300x200.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Muốn đầu tư, hãy tham khảo danh sách chung cư quận Bình Thạnh này</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Bình Thạnh là một nơi lý tưởng để đầu tư bất động sản cao cấp. Bài viết tổng hợp danh sách 3 dự án chung cư nổi bật ...
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Cau-Giay-1-300x188.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Quận Cầu Giấy đang được giới đầu tư địa ốc rất để tâm với những dự án mới mở. Bài viết liệt kê danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020 đáng chú ý nhất hiện nay. Nếu TP.HCM có Landmark 81 thì ở Hà Nội có Landmark 72 toạ lạc tại quận Cầu Giấy cũng không hề kém cạnh cả về độ sầm uất lẫn các tiện nghi mang đến. Ngoài ra, khu vực này vẫn còn có rất nhiều dự án bất động sản khác, điển hình là những khu chung cư cao tầng cũng đang nhận được rất nhiều sự quan tâm của giới đầu tư và cá nhân.  Bài viết sau đây của Chợ Tốt sẽ liệt kê danh sách chung cư quận Cầu Giấy đang mở bán để bạn có thêm thông tin cần thiết cho việc đầu tư. Bắt đầu ngay! Tham khảo danh sách chung cư tại Hà Nội: Địa ốc khu vực quận Cầu Giấy đang rất được quan tâm 1. Tổng quan thị trường chung cư quận Cầu Giấy Sở hữu cơ sở hạ tầng mới đồng bộ và hiện đại, quận Cầu Giấy được đánh giá là thị trường bất động sản hấp dẫn bậc nhất Thủ đô với nhiều dự án tiềm năng. Đa phần các khu chung cư này đều thuộc phân khúc cao cấp, phục vụ tầng lớp có thu nhập trung bình khá trở lên. Bên cạnh đó, khu vực này cũng có nhiều công ty, trụ sở, văn phòng làm việc, gần chợ, bệnh viện, trường học và các trung tâm thương mại… Do đó, danh sách chung cư quận Cầu Giấy cũng đang tăng dần lên theo thời gian nhằm cung cấp nhiều không gian hơn cho người dân.  Ngoài […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div>
							<div class="row justify-content-end">
								<div class="col-md-7 text-md-left text-center">
									<a class="btn btn-read-mode" href="#">Xem thêm</a>
								</div>
							</div>
						</div>
						<div class="cate-group">
							<h2 class="title-group">Tư vấn</h2>
							<!-- 2 items -->
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Cau-Giay-1-300x188.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Quận Cầu Giấy đang được giới đầu tư địa ốc rất để tâm với những dự án mới mở. Bài viết liệt kê danh sách chung cư quận Cầu Giấy mở bán cuối năm 2020 đáng chú ý nhất hiện nay. Nếu TP.HCM có Landmark 81 thì ở Hà Nội có Landmark 72 toạ lạc tại quận Cầu Giấy cũng không hề kém cạnh cả về độ sầm uất lẫn các tiện nghi mang đến. Ngoài ra, khu vực này vẫn còn có rất nhiều dự án bất động sản khác, điển hình là những khu chung cư cao tầng cũng đang nhận được rất nhiều sự quan tâm của giới đầu tư và cá nhân.  Bài viết sau đây của Chợ Tốt sẽ liệt kê danh sách chung cư quận Cầu Giấy đang mở bán để bạn có thêm thông tin cần thiết cho việc đầu tư. Bắt đầu ngay! Tham khảo danh sách chung cư tại Hà Nội: Địa ốc khu vực quận Cầu Giấy đang rất được quan tâm 1. Tổng quan thị trường chung cư quận Cầu Giấy Sở hữu cơ sở hạ tầng mới đồng bộ và hiện đại, quận Cầu Giấy được đánh giá là thị trường bất động sản hấp dẫn bậc nhất Thủ đô với nhiều dự án tiềm năng. Đa phần các khu chung cư này đều thuộc phân khúc cao cấp, phục vụ tầng lớp có thu nhập trung bình khá trở lên. Bên cạnh đó, khu vực này cũng có nhiều công ty, trụ sở, văn phòng làm việc, gần chợ, bệnh viện, trường học và các trung tâm thương mại… Do đó, danh sách chung cư quận Cầu Giấy cũng đang tăng dần lên theo thời gian nhằm cung cấp nhiều không gian hơn cho người dân.  Ngoài […]
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item-post">
								<a href="{{ route('frontend.news.post') }}" class="d-block">
									<div class="row">
										<div class="col-4 col-md-5 item-imgbox">
											<img src="{{ asset('image/Danh-sach-chung-cu-quan-Binh-Thanh-2-300x200.jpg') }}">
										</div>
										<div class="col-12 col-md-7">
											<div class="post-meta">
												<h3 class="title-post">Muốn đầu tư, hãy tham khảo danh sách chung cư quận Bình Thạnh này</h3>
											</div>
										</div>
									</div>
								</a>
								<div class="post-ab">
									<div class="row">
										<div class="col-4 col-md-5"></div>
										<div class="col-8 col-md-7 excerpt-info-box">
											<div class="post-meta">
												<div class="excerpt-info">
													<div class="excerpt-info-text">
														Bình Thạnh là một nơi lý tưởng để đầu tư bất động sản cao cấp. Bài viết tổng hợp danh sách 3 dự án chung cư nổi bật ...
													</div>
												</div>
												<ul>
													<li>
														<a href="#">Thị trường</a>
														· 31/10/2020
													</li>
												</ul>
												<hr class="d-none d-md-block">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div>
							<div class="row justify-content-end">
								<div class="col-md-7 text-md-left text-center">
									<a class="btn btn-read-mode" href="#">Xem thêm</a>
								</div>
							</div>
						</div>
						<div class="tags-list">
							<ul>
								<li><a href="#">Sổ đỏ</a></li>
								<li><a href="#">chung cư</a></li>
								<li><a href="#">văn phòng</a></li>
								<li><a href="#">thiết kế nội thất phòng trọ</a></li>
								<li><a href="#">chung cư quận 2</a></li>
								<li><a href="#">green park</a></li>
								<li><a href="#">>e-home</a></li>
								<li><a href="#">tân hương Tower</a></li>
								<li><a href="#">giá chung cư</a></li>
								<li><a href="#">nhà ở xã hội</a></li>
								<li><a href="#">ban công đẹp</a></li>
								<li><a href="#">đất thổ cư</a></li>
								<li><a href="#">phòng trọ</a></li>
								<li><a href="#">thiết kế nội thất</a></li>
								<li><a href="#">đầu tư bất động sản</a></li>
								<li><a href="#">biệt thự</a></li>
								<li><a href="#">xây nhà giá rẻ</a></li>
								<li><a href="#">phong thủy chung cư</a></li>
								<li><a href="#">phong thủy nhà ở</a></li>
								<li><a href="#">Vinhomes Central Park</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
		</div>
	</div>
@endsection