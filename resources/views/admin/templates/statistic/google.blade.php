@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- main ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3>アクセス解析</h3>
		</div>
		<div class="gg-statistics-page">
			<div class="container">
				<form action="{{ route('admin.google_statistic.index') }}" method="POST" class="form-inline google-statistic-form">
					<div class="form-group mr-2">
				    <select class="form-control" name="period">
						  <option value="7" {{ (request()->period == '7') ? 'selected' : '' }}>7日間</option>
						  <option value="30" {{ (request()->period == '30') ? 'selected' : '' }}>30日間</option>
						  <option value="90" {{ (request()->period == '90') ? 'selected' : '' }}>90日間</option>
						</select>
				  </div>
				  <div class="form-group mr-2">
				    <select class="form-control" name="metric">
						  <option value="sessions" {{ (request()->metric == 'sessions') ? 'selected' : '' }}>セッション</option>
						  <option value="pageviews" {{ (request()->metric == 'pageviews') ? 'selected' : '' }}>ページビュー</option>
						  <option value="users" {{ (request()->metric == 'users') ? 'selected' : '' }}>ユーザー</option>
						  <option value="organicSearches" {{ (request()->metric == 'organicSearches') ? 'selected' : '' }}>オーガニック検索</option>
						  <option value="bounceRate" {{ (request()->metric == 'bounceRate') ? 'selected' : '' }}>直帰率</option>
						</select>
				  </div>
				  <div class="form-group">
				  	{{ csrf_field() }}
				    <button type="submit" class="btn btn-no-radius" style="background: #111111; color: #ffffff;">適用する</button>
				  </div>
				</form>
			    	<div id="chartdiv" style="width: 100%;height: 500px;"></div>
			    	<div class="row">
			    		<div class="col-md-4 col-sm-6">
			    			<div class="gg-box">
			    				<h6>セッション</h6>
			    				<h3>{{ isset($dataTotals['visits']) ? $dataTotals['visits'] : '0' }}</h3>
			    			</div>
			    		</div>
			    		<div class="col-md-4 col-sm-6">
			    			<div class="gg-box">
			    				<h6>ユーザー</h6>
			    				<h3>{{ isset($dataTotals['visitors']) ? $dataTotals['visitors'] : '0' }}</h3>
			    			</div>
			    		</div>
			    		<div class="col-md-4 col-sm-6">
			    			<div class="gg-box">
			    				<h6>ページビュー</h6>
			    				<h3>{{ isset($dataTotals['pageViews']) ? $dataTotals['pageViews'] : '0' }}</h3>
			    			</div>
			    		</div>
			    		<div class="col-md-4 col-sm-6">
			    			<div class="gg-box">
			    				<h6>直帰率</h6>
			    				<h3>{{ isset($dataTotals['bounceRate']) ? $dataTotals['bounceRate'] : '0' }} %</h3>
			    			</div>
			    		</div>
			    		<div class="col-md-4 col-sm-6">
			    			<div class="gg-box">
			    				<h6>オーガニック検索</h6>
			    				<h3>{{ isset($dataTotals['organicSearches']) ? $dataTotals['organicSearches'] : '0' }}</h3>
			    			</div>
			    		</div>
			    		<div class="col-md-4 col-sm-6">
			    			<div class="gg-box">
			    				<h6>ページ/セッション</h6>
			    				<h3>{{ isset($dataTotals['pageViews']) && $dataTotals['pageViews'] != 0 && isset($dataTotals['visits']) && $dataTotals['visits'] != 0 ? round($dataTotals['pageViews']/$dataTotals['visits'], 2) : '0' }}</h3>
			    			</div>
			    		</div>
			    		<div class="col-md-4 col-sm-6">
			    			<div class="gg-box">
			    				<h6>ページ滞在時間</h6>
			    				@if(isset($dataTotals['avgTimeOnPage']))
			    					@php
									$hours = floor($dataTotals['avgTimeOnPage'] / 3600);
									$mins = floor($dataTotals['avgTimeOnPage'] / 60 % 60);
									$secs = floor($dataTotals['avgTimeOnPage'] % 60);
									$timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
			    					@endphp
			    					<h3>{{ $timeFormat }}</h3>
			    				@else
			    					<h3>00:00:00</h3>
			    				@endif
			    			</div>
			    		</div>
			    		<div class="col-md-4 col-sm-6">
			    			<div class="gg-box">
			    				<h6>ページ読込時間</h6>
			    				<h3>{{ isset($dataTotals['pageLoadTime']) ? $dataTotals['pageLoadTime'] : '0' }}</h3>
			    			</div>
			    		</div>
			    		<div class="col-md-4 col-sm-6">
			    			<div class="gg-box">
			    				<h6>セッション時間</h6>
			    				@if(isset($dataTotals['avgSessionDuration']))
			    					@php
									$hours = floor($dataTotals['avgSessionDuration'] / 3600);
									$mins = floor($dataTotals['avgSessionDuration'] / 60 % 60);
									$secs = floor($dataTotals['avgSessionDuration'] % 60);
									$timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
			    					@endphp
			    					<h3>{{ $timeFormat }}</h3>
			    				@else
			    					<h3>00:00:00</h3>
			    				@endif
			    			</div>
			    		</div>
			    	</div>
				 	</div>
		</div>
	</section>
	<!-----x----- main -----x----->
@endsection
@section('footer_js')
	<script src="https://www.amcharts.com/lib/4/core.js"></script>
	<script src="https://www.amcharts.com/lib/4/charts.js"></script>
	<script src="https://www.amcharts.com/lib/4/lang/ja_JP.js"></script>
	<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

	<!-- Chart code -->
	<script>
	am4core.ready(function() {

	// Themes begin
	am4core.useTheme(am4themes_animated);
	// Themes end

	var chart = am4core.create("chartdiv", am4charts.XYChart);
	chart.language.locale = am4lang_ja_JP;

	var data = [];
	var value = 0;
	var chartData = {!! (isset($chartData) && !is_null($chartData)) ? json_encode($chartData) : json_encode([]) !!};
	
	if(typeof(chartData) != "undefined" && chartData !== null){
		data = chartData;
	}
	chart.data = data;

	// Create axes
	var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
	dateAxis.renderer.minGridDistance = 150;

	var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
	valueAxis.min = 0;
	valueAxis.renderer.labels.template.adapter.add("text", function(text, target) {
	  return text.match(/\./) ? "" : text;
	});

	// Create series
	var series = chart.series.push(new am4charts.LineSeries());
	series.dataFields.valueY = "count";
	series.dataFields.dateX = "date";
	series.tooltipText = "{count}"

	series.tooltip.pointerOrientation = "vertical";

	chart.cursor = new am4charts.XYCursor();
	chart.cursor.snapToSeries = series;
	chart.cursor.xAxis = dateAxis;

	chart.scrollbarX = new am4charts.XYChartScrollbar();
	chart.scrollbarX.series.push(series);

	}); // end am4core.ready()
	</script>
@endsection