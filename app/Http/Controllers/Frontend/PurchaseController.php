<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

/**
 * 
 */
class PurchaseController extends Controller
{
	
	function __construct()
	{
		
	}
	
	public function index() 
	{
		return view('frontend.templates.purchase.index');
	}

	public function item()
	{
		return view('frontend.templates.purchase.item');
	}
}