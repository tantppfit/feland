@extends('frontend.layouts.app')
@section('content')
	<main class="all-container">
		<div class="item-detail">
			<div class="detail-base row">
				<div class="google-ads-view-top"></div>
				<div class="row justify-content-between">
					<div class="col-md-8 col-auto">
						<div>
							<ol class="base-detail-breadcrumb hidden-xs">
								<li class="breadcrumb-item"><a href="#">Chợ Tốt Nhà</a></li>
								<li class="breadcrumb-item"><a href="#">Tp Hồ Chí Minh</a></li>
								<li class="breadcrumb-item"><a href="#">Huyện Bình Chánh</a></li>
								<li class="breadcrumb-item"><a href="#">Nhà ở</a></li>
								<li class="breadcrumb-item active"><a href="#">CHỈ CẦN 200TR NHẬN NGAY CĂN HỘ TP THUẬN AN</a></li>
							</ol>
						</div>
					</div>
					<div class="col-md-4 col-12">
						<div class="button-wrapper">
							<button class="prev-ad">Về danh sách</button>
							<button class="next-ad">Tin tiếp</button>
						</div>
					</div>
				</div>
				<div style="margin:10px 0px"><div></div></div>
				<div class="col-lg-8">
					<div class="ad-image-wrapper">
						<div class="ad-ribbon" style="background-color: #F59A00; border-color: #F59A00; color: #FFFFFF;">HOT</div>
						<div class="ad-item-slider-wrapper">
							<div class="ad-slider-container">
								<div class="swiper-wrapper">
									<div class="swiper-slide">
										<div>
											<div class="slider-image">
												<div class="slider-wrapper">
													<div class="slider-image-wrapper">
														<span></span>
														<img src="{{ asset('image/slider-1.jpg') }}">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="swiper-slide">
										<div>
											<div class="slider-image">
												<div class="slider-wrapper">
													<div class="slider-image-wrapper">
														<span></span>
														<img src="{{ asset('image/slider-2.jpg') }}">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="swiper-slide">
										<div>
											<div class="slider-image">
												<div class="slider-wrapper">
													<div class="slider-image-wrapper">
														<span></span>
														<img src="{{ asset('image/slider-3.jpg') }}">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="swiper-slide">
										<div>
											<div class="slider-image">
												<div class="slider-wrapper">
													<div class="slider-image-wrapper">
														<span></span>
														<img src="{{ asset('image/slider-4.jpg') }}">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="swiper-slide">
										<div>
											<div class="slider-image">
												<div class="slider-wrapper">
													<div class="slider-image-wrapper">
														<span></span>
														<img src="{{ asset('image/slider-5.jpg') }}">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="swiper-pagination"></div>
							<button class="ad-next-prev-btn ad-prev"><i></i></button>
							<button class="ad-next-prev-btn ad-next"><i></i></button>
						</div>
						<div class="image-caption">
							<div class="image-caption-text">Tin môi giới đăng 1 tháng trước</div>
						</div>
					</div>
					<div class="ad-description-wrapper">
						<h1 class="ad-title">CHỈ CẦN 200TR NHẬN NGAY CĂN HỘ TP THUẬN AN</h1>
						<div class="price-wrapper">
							<div class="ad-price">
								<span>
									<div>
										<span class="price-wrapper">
											<span class="price">
												1 tỷ
												<span class="square-metre">65 m <sub>2</sub></span>
											</span>
										</span>
									</div>
								</span>
							</div>
							<div class="save-ad">
								<button type="button" class="save-ad-view-detail">
									<p style="margin:0; margin-right:5px;">Lưu tin</p>
									<img width="20" src="{{ asset('image/icon/new-save-ad.svg') }}" alt="like">
								</button>
							</div>
						</div>
						<div>
							<div class="col-12 p-0">
								<div class="media margin-top-05" style="display:flex;align-items:center;margin:10px 0px">
									<div class="media-left media-middle">
										<img class="ad-param-icon" alt="location" src=" {{ asset('https://static.chotot.com/storage/icons/logos/ad-param/location.svg') }}">
									</div>
									<div class="media-body media-middle address" role="button" tabindex="0">
										<span class="fz13">Số 11 Nguyễn Văn Tiết, phường Lái Thiêu, thành phố Thuận An, tỉnh Bình Dương., Phường Lái Thiêu, Thị xã Thuận An, Bình Dương</span>
									</div>
								</div>
							</div>
							<div class="ad-param-item">
								<div class="ad-media-param">
									<div class="media-left media-top">
										<img class="ad-param-icon" alt="Dự Án" src="{{ asset('image/new_project.png') }}">
									</div>
									<div class="media-body media-middle">
										<span>
											<span style="font-size:14px">Dự Án: </span>
											<span class="ad-param-value"><a href="" style="color:#5d89b6;cursor:pointer;outline:none;font-size:14px">Opal Skyline</a></span>
										</span>
									</div>
								</div>
							</div>
						</div>
						<p class="ad-body">
							Có căn tầng trung suất nội bộ giá chỉ 25tr/m2. 
							Tọa lạc ngay trên con đường Nguyễn Văn Tiết nhộn nhịp, 2 tòa tháp 36 tầng Opal Skyline sở hữu tầm nhìn không giới hạn hướng ra sân golf Sông Bé và ôm trọn cảnh quan thành phố mới Thuận An.
							THÔNG TIN DỰ ÁN OPAL SKYLINE
							🌍 Vị Trí : Mặt Tiền Nguyễn Văn Tiết - Trung Tâm Thành Phố Thuận An, Bình Dương (Chợ Lái Thiêu )
							♻ Chủ đầu tư: Công ty Bất Động Sản Hà An
							♻ Phát triển dự án: Tập Đoàn Đất Xanh
							♻ Tổng thầu xây dựng: An Phong, FBV (Dự kiến)
							♻ Thiết kế: Ong&amp;Ong – Singapore, Green Design, CPG
							♻ Quản lý: CBRE (Dự kiến)
							♻ Quy Mô : 1.530 Căn Hộ &amp; 24 Shophouse
						</p>
					</div>
					<div class="d-lg-block d-none">
						<div class="w-100">
							<span class="mini-contract">
								<span>Nhấn để hiện số: <strong>0964922101</strong></span>
							</span>
						</div>
					</div>
					<div style="margin: 5px 0 15px 0;"></div>
					<div>
						<div class="col-12 row p-0">
							<div></div>
							<div class="col-lg-6 ad-param-item">
								<div class="ad-media-param">
									<div class="media-left media-top">
										<img class="ad-param-icon" alt="Dự Án" src="{{ asset('image/property_status.png') }}">
									</div>
									<div class="media-body media-middle">
										<span>
											<span>Tình trạng bất động sản: </span>
											<span class="ad-param-value">Chưa bàn giao</span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-lg-6 ad-param-item">
								<div class="ad-media-param">
									<div class="media-left media-top">
										<img class="ad-param-icon" alt="Dự Án" src="{{ asset('image/size.png') }}">
									</div>
									<div class="media-body media-middle">
										<span>
											<span>Diện tích: </span>
											<span class="ad-param-value">65 m²</span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-lg-6 ad-param-item">
								<div class="ad-media-param">
									<div class="media-left media-top">
										<img class="ad-param-icon" alt="Dự Án" src="{{ asset('image/price_m2.png') }}">
									</div>
									<div class="media-body media-middle">
										<span>
											<span>Giá/m2: </span>
											<span class="ad-param-value">15,38 triệu/m²</span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-lg-6 ad-param-item">
								<div class="ad-media-param">
									<div class="media-left media-top">
										<img class="ad-param-icon" alt="Dự Án" src="{{ asset('image/rooms.png') }}">
									</div>
									<div class="media-body media-middle">
										<span>
											<span>Số phòng ngủ: </span>
											<span class="ad-param-value">2 phòng</span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-lg-6 ad-param-item">
								<div class="ad-media-param">
									<div class="media-left media-top">
										<img class="ad-param-icon" alt="Dự Án" src="{{ asset('image/direction.png') }}">
									</div>
									<div class="media-body media-middle">
										<span>
											<span>Hướng cửa chính: </span>
											<span class="ad-param-value">Đông Nam</span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-lg-6 ad-param-item">
								<div class="ad-media-param">
									<div class="media-left media-top">
										<img class="ad-param-icon" alt="Dự Án" src="{{ asset('image/block.png') }}">
									</div>
									<div class="media-body media-middle">
										<span>
											<span>Tên phân khu/Lô/Block/Tháp: </span>
											<span class="ad-param-value">2</span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-lg-6 ad-param-item">
								<div class="ad-media-param">
									<div class="media-left media-top">
										<img class="ad-param-icon" alt="Dự Án" src="{{ asset('image/floornumber.png') }}">
									</div>
									<div class="media-body media-middle">
										<span>
											<span>Tầng số: </span>
											<span class="ad-param-value">36</span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-lg-6 ad-param-item">
								<div class="ad-media-param">
									<div class="media-left media-top">
										<img class="ad-param-icon" alt="Dự Án" src="{{ asset('image/toilets.png') }}">
									</div>
									<div class="media-body media-middle">
										<span>
											<span>Số phòng vệ sinh: </span>
											<span class="ad-param-value">2</span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-lg-6 ad-param-item">
								<div class="ad-media-param">
									<div class="media-left media-top">
										<img class="ad-param-icon" alt="Dự Án" src="{{ asset('image/apartment_type.png') }}">
									</div>
									<div class="media-body media-middle">
										<span>
											<span>Loại hình căn hộ: </span>
											<span class="ad-param-value">Chung cư</span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-lg-6 ad-param-item">
								<div class="ad-media-param">
									<div class="media-left media-top">
										<img class="ad-param-icon" alt="Dự Án" src="{{ asset('image/property_legal_document.png') }}">
									</div>
									<div class="media-body media-middle">
										<span>
											<span>Giấy tờ pháp lý: </span>
											<span class="ad-param-value">Giấy tờ khác</span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-lg-6 ad-param-item">
								<div class="ad-media-param">
									<div class="media-left media-top">
										<img class="ad-param-icon" alt="Dự Án" src="{{ asset('image/furnishing_sell.png') }}">
									</div>
									<div class="media-body media-middle">
										<span>
											<span>Tình trạng nội thất: </span>
											<span class="ad-param-value">Nội thất cao cấp</span>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 p-0">
							<div class="chat-temp-wrapper">
								<div class="title-chat-temp">Hỏi người bán qua chat</div>
								<div class="template-item-wrapper">
									<ul class="template-message">
										<li class="template-item">Căn hộ này còn không ạ?</li>
										<li class="template-item">Tình trạng giấy tờ như thế nào ạ?</li>
										<li class="template-item">Tôi có thể trả góp không?</li>
										<li class="template-item">Giá bán đã bao gồm nội thất chưa?</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 p-0">
						<strong style="color:#777">Chia sẻ tin đăng này cho bạn bè:</strong>
						<div style="margin:5px 0 0 0;border-top:1px solid #eee"></div>
						<div style="margin-bottom: 10px;">
							<a href="" style="margin: 10px 10px 0; cursor: pointer; display: inline-block;">
								<img alt="facebook" src=" {{ asset('image/icon/circle-facebook.svg') }}" height="40" width="40">
							</a>
							<a href="" style="margin: 10px 10px 0; cursor: pointer; display: inline-block;">
								<img alt="facebook" src=" {{ asset('image/icon/circle-messenger.svg') }}" height="40" width="40">
							</a>
							<a href="" style="margin: 10px 10px 0; cursor: pointer; display: inline-block;">
								<img alt="facebook" src=" {{ asset('image/icon/circle-zalo.svg') }}" height="40" width="40">
							</a>
							<a href="" style="margin: 10px 10px 0; cursor: pointer; display: inline-block;">
								<img alt="facebook" src=" {{ asset('image/icon/circle-copylink.svg') }}" height="40" width="40">
							</a>
						</div>
					</div>
					<div>
						<div class="report-wrapper">
							<div class="report-btn">
								<button class="btn btn-default report-bad btn-xs">Báo tin không hợp lệ</button>
								<button class="btn btn-default report-bad btn-xs">Báo tin đã bán</button>
							</div>
							<div class="buyer-protect">
								<div style="border-top: 1px solid rgb(236, 236, 236); padding: 7px 5px 5px 0px !important;">
									<img class="buyer-protect-icon" src=" {{ asset('image/icon/shield-iconx4.png') }}">
									<div class="buyer-proctect-text">
										<em>Tin đăng này đã được kiểm duyệt. Nếu gặp vấn đề, vui lòng báo cáo tin đăng hoặc liên hệ CSKH để được trợ giúp.&nbsp;<a target="_blank" rel="noopener" href="http://trogiup.chotot.com/ban-hang-tai-chotot-vn/kiem-duyet-tin/tai-sao-chotot-vn-duyet-tin-truoc-khi-dang/?utm_source=chotot&amp;utm_medium=user_protection&amp;utm_campaign=user_protection_ad_view">Xem thêm ››</a></em>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 p-0">
					 <div></div>
					 <div style="position: sticky; top: 100px;">
					 	<div class="d-lg-block d-none">
					 		<div class="profile-wrapper">
					 			<a href="#" class="seller-wrapper" target="_blank">
					 				<div class="image-thumbnail default-size img-circle image-wrapper">
					 					<img class="default-size img-circle" src=" {{ asset('image/profile-avatar.jpg') }}">
					 				</div>
					 				<div class="name-bounder">
					 					<div class="d-flex">
					 						<div class="name-div">
					 							<b>Lê Nguyễn Duy</b>
					 						</div>
					 						<button class="secondary-button" type="button">Xem trang</button>
					 					</div>
					 					<div class="status-online-div">
					 						<div class="offline-bullet">•</div>
					 						Hoạt động 16 ngày trước
					 					</div>
					 				</div>
					 			</a>
					 			<div class="info-wrapper">
					 				<div class="info-item">
					 					<div class="info-text">
					 						<p>Môi Giới</p>
					 						<span class="info-value">
					 							<img src=" {{ asset('image/pro-grey-icon.png') }}">
					 						</span>
					 					</div>
					 				</div>
					 				<div class="seperate-line"></div>
					 				<a href="#" class="info-item">
					 					<div class="info-text">
					 						<p>Đánh giá</p>
					 						<span class="info-value">
					 							---
					 						</span>
					 					</div>
						 			</a>
					 				<div class="seperate-line"></div>
					 				<div class="info-item">
					 					<div class="info-text">
					 						<p>Phản hồi chat</p>
					 						<span class="info-value">
					 							---
					 						</span>
					 					</div>
					 				</div>
					 			</div>
					 		</div>
					 	</div>
					 	<div class="wrapper-lead-button-destop">
					 		<div class="show-phone-button">
					 			<div class="w-100">
					 				<div class="show-phone-wrapper">
					 					<div class="show-phone">
					 						<span>
						 						<img src=" {{ asset('image/icon/white-phone.svg') }}">
						 						<span class="phone-number-hide">0964922101</span>
						 					</span>
					 						&nbsp;&nbsp;
					 						<span>BẤM ĐỂ HIỆN SỐ</span>
					 					</div>
					 				</div>
					 			</div>
					 		</div>
					 		<div class="chat-destop-button">
					 			<div class="w-100">
					 				<a class="chat-destop-button-wrapper" href="#">
					 					<div>
					 						<span>
					 							<img src=" {{ asset('image/chat_green.png') }}">
					 						</span>
					 						<span>CHAT VỚI NGƯỜI BÁN</span>
					 					</div>
					 				</a>
					 			</div>
					 		</div>
					 	</div>
					 	<div class="wrapper-lead-button-mobile">
					 		<a href="tel" class="btn call-phone-btn button-success">
					 			<img src=" {{ asset('image/call.png') }}">
					 			<span>Gọi điện</span>
					 		</a>
					 		<a href="sms" class="btn sms-btn button-default">
					 			<img src=" {{ asset('image/sms.png') }}">
					 			<span>SMS</span>
					 		</a>
					 		<a href="#" class="btn chat-btn button-default">
					 			<img src=" {{ asset('image/chat.png') }}">
					 			<span>Chat</span>
					 		</a>
					 	</div>
					 	<div class="d-lg-block d-none">
					 		<div>
					 			<div class="safe-tips-wrapper">
					 				<img class="float-left" width="100" src=" {{ asset('image/1_PTY.png') }}">
					 				<div class="tip-text">
					 					<p style="font-size: 13px; margin: 0;">KHÔNG NÊN đặt cọc nếu chưa xác định được chủ nhà.</p>
					 					<a href="#" target="blank">Tìm hiểu thêm »</a>
					 				</div>
					 			</div>
					 		</div>
					 		<div class="right-hand-site-ad-view"></div>
					 	</div>
					 </div>
				</div>
			</div>
		</div>
	</main>
	<!-- Link Swiper's CSS -->
	<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
@endsection