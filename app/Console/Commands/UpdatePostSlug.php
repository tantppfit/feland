<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Post;

class UpdatePostSlug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:update-post-slug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update post slug';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Post::cursor() as $post) {
            if(is_null($post->slug) || empty($post->slug)){
                $post->slug = $post->id;
                $post->save();
            }            
        }
    }
}
