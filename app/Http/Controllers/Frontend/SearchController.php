<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Page;

class SearchController extends Controller
{
    protected $postModel;
    protected $pageModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Post $postModel, Page $pageModel )
    {
        $this->postModel = $postModel;
        $this->pageModel = $pageModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $page = $this->pageModel->getPageBySlug('search');
        if(!$page){
            abort(404);
        }
        $metaTitle = $page->title;
        return view('frontend.templates.search.index')->with([
            'metaTitle' => $metaTitle
        ]);
    }
    public function result(Request $request)
    {
        $posts = $this->postModel->getPosts($request, 1,20);
        $metaTitle = ($request->has('s')) ? $request->s : '';
        return view('frontend.templates.search.result')->with([
            'posts' => $posts,
            'metaTitle' => $metaTitle
        ]);
    }
    
}
