 @extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main">
		<div class="post-edit">
			<h3>サイト設定</h3>
			<p>メディアに表示されるサイト情報を編集します。</p>
			@include('admin.parts.alert')
		</div>
		<form action="{{ route('admin.site.update', $site->id) }}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="edit-img-info" style="background: #f8f8f8; padding: 1rem;margin-top: 60px; height: auto;">
				<div class="edit-img">
					{{-- <div class="form-group row">
						<div class="label-edit col-md-2 col-12" style="margin-top: -4px;">
							<label>ウェブサイト名</label>
						</div>
						<div class="col-12 col-md-10">
							<input type="text" class="form-control" name="title" value="{{ $site->title }}">
						</div>
					</div> --}}
					{{-- <div class="form-group row">
						<div class="label-edit col-md-2 col-12" style="margin-top: -4px;">
							<label>サイトの説明</label>
						</div>
						<div class="col-12 col-md-10">
							<textarea name="description" id="description" class="form-control" rows="10">{{ $site->description }}</textarea>
						</div>
					</div> --}}
					<div class="form-group row">
						<div class="label-edit col-lg-2 col-md-3 col-12" style="margin-top: -4px;">
							<label>カバー画像URL</label>
						</div>

						<div class="col-12 col-lg-7 col-md-9">
							<div class="text-info" style="width: 100%;">
								<p style="color: black;">
									<i class="fas fa-info-circle" style="margin-right: 15px; margin-left: 5px; color: gray; margin-top: 10px;"></i><span style="color: red;">サイトベージ</span>のメタ情報(<span class="btn btn-light" style="font-size: 12px;color:#549551;background-color: #F8F8F8;border: 1px solid #EDEDF1;">0GP image</span>)
								</p>

								<p style="margin-left: 37px; color: black">このサイト上で作成された画像なし記事に対する、一覧サムネイルおよびメタ夕情報(<span class="btn btn-light" style="font-size: 12px;color:#549551;background-color: #F8F8F8;border: 1px solid #EDEDF1;">0GP image</span>)
								</p>
							</div>
						</div>
					</div>
					<div class="full-site-setting form-group row">
						<div class="col-12 col-md-2">
							
						</div>

						<div class="col-12 col-md-10">
							@if(!empty($site->cover_image_url))
								<div style="margin-top: 16px;">
									<img src="{{ $site->cover_image_url }}" style="max-height: 200px;max-width: 350px;" data-image-input-preview="cover_image_url">
								</div>
							@endif
							<div style="margin-top: 5px;">
								<input name="cover_image_url" class="form-control" style="width:100%; border-radius: 5px; border: 1px solid #111111;font-size: 12px;" value="{{ $site->cover_image_url }}">
							</div>
							<div style="margin-top: 10px;">
								<button type="button" class="btn btn-no-radius open-library" data-input-name="cover_image_url" data-input-type="url" data-toggle="modal" data-target="#library-modal" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;"></i>メディアライブラリから選択</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="btn-group" style="margin-top: 20px;">
				<button class="btn-submit btn btn-no-radius">保存</button>						
			</div>
		</form>
		<div>
			<p>
				<i class="fas fa-info-circle" style="margin-right: 15px;margin-left: 5px;color: gray; float: left;"></i>
			</p>
		</div>
		<div class="text-info-text">
			<p>
				「<span style="color: red;">サイトページ</span>」は、被数のサイトを配下に持つメディアで利用するために用意<br>
				されているページです。サイト単位で絞り込んだストリームが表示されます。単<br>ーのサイトのみを配下に持つメティアでは、内容は通常の新槽膜ストリームと同<br>ーになります。
			</p>
		</div>
	</section>
	<!-----x----- admin-edit main section -----x----->
	<!-- Library modal -->
	@include('admin.templates.media.parts.modal-library')
	<!--x-- Library modal -->
@endsection