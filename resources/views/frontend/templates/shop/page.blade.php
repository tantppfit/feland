@extends('frontend.layouts.app')
@section('content')
	<main class="shop-page-container">
		<div class="shop-info-box">
			<div class="shop-cover-box">
				<img src="{{ asset('image/shop-1.jpg') }}">
			</div>
			<div class="shop-avatar-box">
				<img src="{{ asset('image/shop-avatar-1.jpg') }}">
			</div>
			<button class="shop-follow-button">
				<span>+</span>
				Theo dõi
			</button>
			<div class="shop-option-box">
				<div class="shop-drop-button"></div>
			</div>
			<div class="clear-fix"></div>
			<div class="shop-name-box">
				<h1>Chuyên cho thuê và chuyển nhượng BĐS Novaland</h1>
			</div>
			<div class="clear-fix"></div>
			<div class="shop-rating-box"></div>
			<div class="clear-fix"></div>
			<div class="shop-statistics-box">
				<ul>
					<li>
						<div class="shop-statistics-label">Số tin đăng đã bán</div>
						<div class="shop-statistics-result">19</div>
					</li>
					<li>
						<div class="shop-statistics-label">Thời gian hoạt động</div>
						<div class="shop-statistics-result">+ 6 tháng</div>
					</li>
					<li>
						<div class="shop-statistics-label">Số người theo dõi</div>
						<div class="shop-statistics-result">2</div>
					</li>
				</ul>
			</div>
			<div class="clear-fix"></div>
			<div class="shop-contact-box-desktop">
				<button class="shop-contact-green-button">
					<i class="white-phone-icon"></i>
					<div class="hidden-phone-number">0867741243</div>
					<div class="lead-for-shop">Bấm để hiện số</div>
				</button>
			</div>
			<div class="shop-contact-box-mobile">
				<div class="shop-contact-footer">
					<div class="sms-block-button">
						<a href="#">
							<i class="sms-block-button-icon"></i>
							<span>Nhắn tin</span>
						</a>
					</div>
					<div class="call-block-button">
						<a href="#">
							<i class="call-block-button-icon"></i>
							<span>Gọi điện</span>
						</a>
					</div>
				</div>
			</div>
			<div class="clear-fix"></div>
		</div>
		<div class="shop-content-box">
			<div class="shop-menu-tab-box">
				<ul>
					<li class="active">
						<a href="#shop-index">TRANG CHỦ</a>
					</li>
					<li>
						<a href="#shop-ads">TIN ĐĂNG</a>
					</li>
					<li>
						<a href="#shop-rating">ĐÁNH GIÁ</a>
					</li>
					<li>
						<a href="#shop-info">THÔNG TIN</a>
					</li>
				</ul>
			</div>
			<div class="clear-fix"></div>
			<div class="clear-fix"></div>
			<div class="tab-content">
				<div class="tab-item" id="shop-index">
					<div class="shop-section-box">
						<h2>Thông tin chi tiết</h2>
						<div>
							<div class="shop-detail-info-box">
								<div class="shop-detail-info-item">
									<div class="shop-address-box">
										<i class="shop-detail-info-icon icon-address"></i>
										<div class="shop-detail-info-text-box">
											<div class="shop-detail-info-label">
												Địa chỉ: <span class="shop-detail-info-value-clickable">08 Hoàng Minh Giám, phường 9, Phú Nhuận, Hồ Chí Minh, Việt Nam</span>
											</div>
											<div class="clear-fix"></div>
											<div class="shop-google-map-box">
												<iframe title="shop-map" width="100%" height="100%" frameborder="0" src="https://www.google.com/maps/embed/v1/search?&amp;q=08 Hoàng Minh Giám, phường 9, Phú Nhuận, Hồ Chí Minh, Việt Nam&amp;zoom=10&amp;key=AIzaSyBEWb603KEdyjuwiyeuWmUgVVhoLzXTmCs" allowfullscreen=""></iframe>
											</div>
										</div>
										<div class="clear-fix"></div>
									</div>
								</div>
								<div class="shop-detail-info-item">
									<div class="shop-phone-box">
										<i class="shop-detail-info-icon icon-phone"></i>
										<div class="shop-detail-info-text-box">
											<div class="shop-detail-info-label show-phone-number">
												Số điện thoại:
												<span class="shop-detail-info-value-black mini-phone">0867741243</span>
												<span class="shop-detail-info-value-clickable-small show-mini-phone">Bấm để hiện số</span>
											</div>
											<div class="clear-fix"></div>
										</div>
										<div class="clear-fix"></div>
									</div>
									<div class="shop-chat-box">
										<i class="shop-detail-info-icon icon-chat"></i>
										<div class="shop-detail-info-text-box">
											<div class="shop-detail-info-label">
												Phản hồi chat:
												<span class="shop-detail-info-value-black">74% (Trong 4 giờ )</span>
											</div>
											<div class="clear-fix"></div>
										</div>
										<div class="clear-fix"></div>
									</div>
									<div class="shop-entry-point-box">
										<i class="shop-detail-info-icon icon-info"></i>
										<div class="shop-detail-info-text-box">
											<div class="shop-detail-info-label">Đã cung cấp:</div>
											<div class="shop-entry-point-icon-box">
												<i class="facebook-inactive-icon"></i>
												<i class="location-inactive-icon"></i>
												<i class="call-active-icon"></i>
												<i class="mail-inactive-icon"></i>
											</div>
											<div class="clear-fix"></div>
										</div>
										<div class="clear-fix"></div>
									</div>
								</div>
								<div class="clear-fix"></div>
							</div>
						</div>
					</div>
					<div class="shop-section-box">
						<h2>Giới thiệu</h2>
						<div>
							<div class="shop-description-box">
								<p>
									<span>
										Chuyên cung cấp thông tin cho thuê căn hộ Novaland tại Phú Nhuận, Tân Bình, gần sân bay: Botanica Premier, The Botanica, Golden Mansion, Orchard Parkview, Orchard Garden, Garden Gate, Newston Residence, Kingston Residence
									</span>
								</p>
							</div>
						</div>
					</div>
					<div class="shop-section-box">
						<h2>Tin đăng nổi bật</h2>
						<div>
							<div class="shop-lastest-ad-box">
								<ul class="shop-lastest-ad-list">
									<!-- 5 ITEMS -->
									<li class="grid-ad-item-box">
										<a href="#">
											<div class="grid-ad-item-image">
												<img src="{{ asset('image/shop-project-1.jpg') }}" alt="">
											</div>
											<div class="grid-ad-item-title">Golden Mansion - 2PN 75m2 đầy đủ nội thất, căn góc</div>
											<div class="grid-ad-item-price">15.000.000 đ</div>
										</a>
									</li>
									<li class="grid-ad-item-box">
										<a href="#">
											<div class="grid-ad-item-image">
												<img src="{{ asset('image/shop-project-2.jpg') }}" alt="">
											</div>
											<div class="grid-ad-item-title">💥 Căn GÓC 3PN 93m2 tại Botanica Premier, view ĐẸP</div>
											<div class="grid-ad-item-price">20.500.000 đ</div>
										</a>
									</li>
									<li class="grid-ad-item-box">
										<a href="#">
											<div class="grid-ad-item-image">
												<img src="{{ asset('image/shop-project-3.jpg') }}" alt="">
											</div>
											<div class="grid-ad-item-title">NEWTON RESIDENCE - CHO THUÊ CĂN HỘ 1-2-3PN</div>
											<div class="grid-ad-item-price">15.000.000 đ</div>
										</a>
									</li>
									<li class="grid-ad-item-box">
										<a href="#">
											<div class="grid-ad-item-image">
												<img src="{{ asset('image/shop-project-4.jpg') }}" alt="">
											</div>
											<div class="grid-ad-item-title">ORCHARD GARDEN - Căn hộ mini 35m2 nội thất ĐẸP</div>
											<div class="grid-ad-item-price">10.500.000 đ</div>
										</a>
									</li>
									<li class="grid-ad-item-box">
										<a href="#">
											<div class="grid-ad-item-image">
												<img src="{{ asset('image/shop-project-5.jpg') }}" alt="">
											</div>
											<div class="grid-ad-item-title">Golden Mansion - 2PN 75m2 đầy đủ nội thất, căn góc</div>
											<div class="grid-ad-item-price">15.000.000 đ</div>
										</a>
									</li>
									<div class="clear-fix"></div>
								</ul>
							</div>
						</div>
					</div>
					<div class="shop-section-box">
						<h2>Tất cả tin đăng</h2>
						<div>
							<div class="shop-ad-box">
								<ul class="shop-ad-list">
									<!-- 5 ITEMS -->
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-1.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">Golden Mansion - 2PN 75m2 đầy đủ nội thất, căn góc</div>
												<div class="list-ad-item-price">
													<span class="price-content">15.000.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>17 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-2.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">💥 Căn GÓC 3PN 93m2 tại Botanica Premier, view ĐẸP</div>
												<div class="list-ad-item-price">
													<span class="price-content">20.500.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>17 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-3.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">NEWTON RESIDENCE - CHO THUÊ CĂN HỘ 1-2-3PN</div>
												<div class="list-ad-item-price">
													<span class="price-content">15.000.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>18 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-4.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">ORCHARD GARDEN - Căn hộ mini 35m2 nội thất ĐẸP</div>
												<div class="list-ad-item-price">
													<span class="price-content">10.500.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>18 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-5.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">🔥Golden Mansion - Căn góc 3PN view Nam thoáng mát</div>
												<div class="list-ad-item-price">
													<span class="price-content">20.000.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>22 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
								</ul>
								<div class="view-more-ad">
									<a href="#shop-ads">Xem thêm <i class="icon-more-ads"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-item" id="shop-ads">
					<div class="filter-cate-wrapper">
						<div class="filter-cate">
							<div class="filter-cate-item">Căn hộ/Chung cư</div>
							<div class="filter-cate-item">Nhà ở</div>
							<div class="filter-cate-item">Đất</div>
						</div>
					</div>
					<div class="clear-fix"></div>
					<div class="shop-section-box">
						<h2>Tất cả tin đăng</h2>
						<div>
							<div class="shop-ad-box">
								<ul class="shop-ad-list">
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-1.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">Golden Mansion - 2PN 75m2 đầy đủ nội thất, căn góc</div>
												<div class="list-ad-item-price">
													<span class="price-content">15.000.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>17 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-2.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">💥 Căn GÓC 3PN 93m2 tại Botanica Premier, view ĐẸP</div>
												<div class="list-ad-item-price">
													<span class="price-content">20.500.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>17 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-3.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">NEWTON RESIDENCE - CHO THUÊ CĂN HỘ 1-2-3PN</div>
												<div class="list-ad-item-price">
													<span class="price-content">15.000.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>18 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-4.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">ORCHARD GARDEN - Căn hộ mini 35m2 nội thất ĐẸP</div>
												<div class="list-ad-item-price">
													<span class="price-content">10.500.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>18 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-5.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">🔥Golden Mansion - Căn góc 3PN view Nam thoáng mát</div>
												<div class="list-ad-item-price">
													<span class="price-content">20.000.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>22 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-1.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">Golden Mansion - 2PN 75m2 đầy đủ nội thất, căn góc</div>
												<div class="list-ad-item-price">
													<span class="price-content">15.000.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>17 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-2.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">💥 Căn GÓC 3PN 93m2 tại Botanica Premier, view ĐẸP</div>
												<div class="list-ad-item-price">
													<span class="price-content">20.500.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>17 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-3.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">NEWTON RESIDENCE - CHO THUÊ CĂN HỘ 1-2-3PN</div>
												<div class="list-ad-item-price">
													<span class="price-content">15.000.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>18 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-4.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">ORCHARD GARDEN - Căn hộ mini 35m2 nội thất ĐẸP</div>
												<div class="list-ad-item-price">
													<span class="price-content">10.500.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>18 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
									<li class="list-ad-item-box">
										<a href="#">
											<div class="list-ad-item-image">
												<img src="{{ asset('image/shop-project-5.jpg') }}">
											</div>
											<div class="list-ad-item-body">
												<div class="list-ad-item-title">🔥Golden Mansion - Căn góc 3PN view Nam thoáng mát</div>
												<div class="list-ad-item-price">
													<span class="price-content">20.000.000 đ</span>
												</div>
												<div class="list-ad-item-info">
													<i class="list-ad-item-shop-icon"></i>
													<span>22 giờ trước</span>
												</div>
											</div>
											<div class="clear-fix"></div>
										</a>
										<i class="save-ad-icon"></i>
									</li>
									<p class="text-center">
										<b>Yay! Bạn đã xem hết tất cả tin đăng rồi!</b>
									</p>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-item" id="shop-rating">
					<div class="shop-rating-detail-box-with-notice">
						<div class="notification-box">Rất tiếc, Chuyên trang chưa có đánh giá nào!</div>
					</div>
				</div>
				<div class="tab-item" id="shop-info">
					<div class="shop-section-box">
						<h2>Thông tin chi tiết</h2>
						<div>
							<div class="shop-detail-info-box">
								<div class="shop-detail-info-item">
									<div class="shop-address-box">
										<i class="shop-detail-info-icon icon-address"></i>
										<div class="shop-detail-info-text-box">
											<div class="shop-detail-info-label">
												Địa chỉ: <span class="shop-detail-info-value-clickable">08 Hoàng Minh Giám, phường 9, Phú Nhuận, Hồ Chí Minh, Việt Nam</span>
											</div>
											<div class="clear-fix"></div>
											<div class="shop-google-map-box">
												<iframe title="shop-map" width="100%" height="100%" frameborder="0" src="https://www.google.com/maps/embed/v1/search?&amp;q=08 Hoàng Minh Giám, phường 9, Phú Nhuận, Hồ Chí Minh, Việt Nam&amp;zoom=10&amp;key=AIzaSyBEWb603KEdyjuwiyeuWmUgVVhoLzXTmCs" allowfullscreen=""></iframe>
											</div>
										</div>
										<div class="clear-fix"></div>
									</div>
								</div>
								<div class="shop-detail-info-item">
									<div class="shop-phone-box">
										<i class="shop-detail-info-icon icon-phone"></i>
										<div class="shop-detail-info-text-box">
											<div class="shop-detail-info-label show-phone-number">
												Số điện thoại:
												<span class="shop-detail-info-value-black mini-phone">0867741243</span>
												<span class="shop-detail-info-value-clickable-small show-mini-phone">Bấm để hiện số</span>
											</div>
											<div class="clear-fix"></div>
										</div>
										<div class="clear-fix"></div>
									</div>
									<div class="shop-chat-box">
										<i class="shop-detail-info-icon icon-chat"></i>
										<div class="shop-detail-info-text-box">
											<div class="shop-detail-info-label">
												Phản hồi chat:
												<span class="shop-detail-info-value-black">74% (Trong 4 giờ )</span>
											</div>
											<div class="clear-fix"></div>
										</div>
										<div class="clear-fix"></div>
									</div>
									<div class="shop-entry-point-box">
										<i class="shop-detail-info-icon icon-info"></i>
										<div class="shop-detail-info-text-box">
											<div class="shop-detail-info-label">Đã cung cấp:</div>
											<div class="shop-entry-point-icon-box">
												<i class="facebook-inactive-icon"></i>
												<i class="location-inactive-icon"></i>
												<i class="call-active-icon"></i>
												<i class="mail-inactive-icon"></i>
											</div>
											<div class="clear-fix"></div>
										</div>
										<div class="clear-fix"></div>
									</div>
								</div>
								<div class="clear-fix"></div>
							</div>
						</div>
					</div>
					<div class="shop-section-box">
						<h2>Giới thiệu</h2>
						<div>
							<div class="shop-description-box">
								<p>
									<span>
										Chuyên cung cấp thông tin cho thuê căn hộ Novaland tại Phú Nhuận, Tân Bình, gần sân bay: Botanica Premier, The Botanica, Golden Mansion, Orchard Parkview, Orchard Garden, Garden Gate, Newston Residence, Kingston Residence
									</span>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clear-fix"></div>
	</main>

@endsection