<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	@include('frontend.common.css_common')

</head>
<body>
	@include('frontend.common.news.header')

	@yield('content')

	@include('frontend.common.footer')

	@include('frontend.common.news.social')

	@include('frontend.common.js_common')
</body>
</html>