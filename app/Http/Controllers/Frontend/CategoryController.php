<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;

class CategoryController extends Controller
{
    protected $categoryModel;
    protected $postModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Category $categoryModel, Post $postModel )
    {
        $this->categoryModel = $categoryModel;
        $this->postModel = $postModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function show(Request $request, $id)
    {
        $category = $this->categoryModel->findOrFail($id);
        $posts = $this->postModel->getPostByCategory($category->id, 1);
        $metaTitle = $category->name;
        return view('frontend.templates.category.show')->with([
            'category' => $category,
            'posts' => $posts,
            'metaTitle' => $metaTitle
        ]);
    }
    
}
