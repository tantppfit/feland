<nav class="media-tabs-management" style="margin-bottom: 10px;">
	<div class="nav nav-tabs" id="nav-tab" role="tablist">
		<a class="nav-item nav-link {{ request()->is('admin/setting') ? 'active' : '' }}" href="{{ route('admin.setting.index') }}">一般設定</a>
		<a class="nav-item nav-link {{ request()->is('admin/setting/stylesheet') ? 'active' : '' }}" href="{{ route('admin.setting.stylesheet') }}">カスタム横域</a>
		<!-- <a class="nav-item nav-link" href="#nav-tab-2">カスタム横域</a> -->
		<a class="nav-item nav-link {{ request()->is('admin/setting/fixedpage') ? 'active' : '' }}" href="{{ route('admin.setting.fixedpage') }}">固定ページ</a>
		<!-- <a class="nav-item nav-link" href="#nav-tab-4">状態</a> -->
		<!-- <a class="nav-item nav-link" href="#nav-tab-5">ファイル</a> -->
		<a class="nav-item nav-link {{ request()->is('admin/setting/widget') ? 'active' : '' }}" href="{{ route('admin.setting.widget') }}">ウィジェット</a>
		<!-- <a class="nav-item nav-link" href="#nav-tab-7">キャッシュ更新</a> -->
		<a class="nav-item nav-link {{ request()->is('admin/setting/vote') ? 'active' : '' }}" href="{{ route('admin.setting.vote') }}">アンケート</a>
		<a class="nav-item nav-link {{ request()->is('admin/setting/footer') ? 'active' : '' }}" href="{{ route('admin.setting.footer') }}">フッター</a>
	</div>
</nav>