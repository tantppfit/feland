@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- main section ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3 class="font-weight-bold">コンテンツ管理</h3>
		</div>
		<nav class="content-tabs-management">
				<div class="nav nav-tabs" id="nav-tab" role="tablist">
  			<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">コンテンツ</a>
  			<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">使元コンテンン</a>
  			<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">ABテスト</a>
				</div>
		</nav>
		<p style="margin-top: 10px;">メティアで取り扱うコンテンツを営理します。</p>	
		<div class="col-12 dash-border-box" style="margin: 20px 0;">
			<div class="row">
				<div style="margin: 1rem;margin-left: 10px;">
					<i class="fas fa-search" style="font-size: 24px; color: #d8dce3;"></i>
				</div>

				<div style="margin: 1.4rem 0; margin-bottom: 15px;">
					<div>
						<select>
							<option value="">No Select</option>
							<option value="" selected>すべてのコンテンツ(新着順)</option>
						</select>
					</div>
					<button class="btn btn-no-radius" style="background: #111111; color: #ffffff;width: 7rem; margin-top: 25px;">検索</button>
				</div>
			</div>
		</div>
		
		<p style="color: #999999;">#{{ (($posts->currentPage() - 1) * $posts->perPage()) + 1 }}...#{{ ($posts->currentPage() * $posts->perPage()) <= $posts->total() ? $posts->currentPage() * $posts->perPage() : $posts->total() }} / {{ $posts->total() }}</p>
		<div class="pagination-menu d-flex">{{ $posts->onEachSide(1)->links() }}</div>

		<div class="content-table-management table-responsive-md" style="overflow: auto;">
				<table class="table">
					<tr>
						<th>コンテンツ</th>
						<th style="width: 100px;">公開日時</th>
						<th></th>
					</tr>
					@if(count($posts))
						@foreach($posts as $post)
							<tr>
								<td>
									<div style="float: left;">
										@if($post->featureImage)
											<img src="{{ Helper::getMediaUrl($post->featureImage, 'thumbnail') }}" style="width:50px;">
										@elseif(Helper::getDefaultCover($post))
											<img src="{{ Helper::getDefaultCover($post) }}" style="width:50px;">
										@endif
									</div>
									<p><span style="margin-left: 5px;">{{ $post->title }}</span></p>
									<div style="clear: both; "></div>
									<p style="border-bottom: 1px dashed #DDDDDD; margin: 10px 0;"></p>
									<p>ID:&nbsp;{{ $post->id }}</p>
									<p><span>{{ $post->site->title }}</span></p>
								</td>
								<td style="color: #111111;"><p style="margin-bottom: 90px; text-align: right;">{{ $post->created_at }}</p></td>
								<td>
									<a href="#" style="color: #C43638;text-decoration:none;"><p style="margin-bottom: 5px;"><i class="fas fa-map-pin" style="color: #C43638;margin-right: 5px;"></i>け優先コンテンツ設定</p></a>
									<a href="#" style="color: #C43638;text-decoration:none;"><p style="margin-bottom: 70px;"><i class="fas fa-caret-right" style="color: #C43638;margin-right: 5px;"></i>その他の操作</p></a>
								</td>
							</tr>
						@endforeach
					@endif
				</table>
			</div>
			<p style="color: #999999;">#{{ (($posts->currentPage() - 1) * $posts->perPage()) + 1 }}...#{{ ($posts->currentPage() * $posts->perPage()) <= $posts->total() ? $posts->currentPage() * $posts->perPage() : $posts->total() }} / {{ $posts->total() }}</p>
		<div class="pagination-menu d-flex">{{ $posts->onEachSide(1)->links() }}</div>
	</section>
	<!-----x----- main section -----x----->
@endsection