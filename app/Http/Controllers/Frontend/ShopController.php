<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

/**
 * 
 */
class ShopController extends Controller
{
	
	function __construct()
	{
		
	}
	
	public function index() 
	{
		return view('frontend.templates.shop.index');
	}

	public function page()
	{
		return view('frontend.templates.shop.page');
	}
}