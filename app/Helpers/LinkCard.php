<?php

namespace App\Helpers;
class LinkCard {
	public static function getHtml($url){
		$url_m = parse_url( $url );
		$scheme = $url_m['scheme'];
		$domain = $url_m['host'];
		$domain_url = $scheme.'://'.$url_m['host'];
		$location = substr($url, mb_strlen($domain_url));
		$data = array();
		$data['url'] = $url;
		$result = self::getCURL( $data );
		if(!isset($result['favicon']) || empty($result['favicon'])){
			$result['favicon'] ='https://www.google.com/s2/favicons?domain='. $domain_url;
		}
		$html = '<div class="linkcard">
						  <div class="lkc-external-wrap">
								<a class="lkc-link no_icon" href="'. $url .'" target="_blank">
								  <div class="lkc-card">
							      <div class="lkc-info"><img class="lkc-favicon" src="'. $result['favicon'] .'" alt="" width="16" height="16">
						          <div class="lkc-domain">'. $result['site_name'] .'</div>
							      </div>
							      <div class="lkc-content">
						          <figure class="lkc-thumbnail">
						          	<img class="lkc-thumbnail-img" src="'. $result['thumbnail'] .'" alt="" />
						          </figure>
						          <div class="lkc-title">
						            <div class="lkc-title-text">'. $result['title'] .'</div>
						          </div>
						          <div class="lkc-url"><cite>'. $url .'</cite></div>
						          <div class="lkc-excerpt">'. $result['excerpt'] .'</div>
							      </div>
							      <div class="clearfix"></div>
								  </div>
								</a>
							</div>
						</div>';
		return $html;
	}

	public static function getCURL( $data ) {
		$url = $data['url'];
		if (!isset( $url ) || $url == '') {
			return null;
		}
		
		$html = null;
		$error = true;
		
		$domain = null;
		$site_name = null;
		$title = null;
		$excerpt = null;
		$charset = null;
		$result_code = null;
		
		$url_m = parse_url( $url );
		$scheme = $url_m['scheme'];
		$domain = $url_m['host'];
		$domain_url= $scheme.'://'.$url_m['host'];
		$location = substr($url, mb_strlen($domain_url));
		
		if( function_exists( 'curl_init' ) ) {
			$result_code = 0;
			$ch = curl_init($url);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_TIMEOUT, 8 );
			curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, false );
			curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
			$html = curl_exec($ch);
			$errno = intval( curl_errno( $ch ) ); 
			if ( $errno ) {
				$html = '';
				$result_code = $errno;
				$error = true;
			} else {
				$header = curl_getinfo($ch);
				$result_code = $header['http_code'];
				$error = false;
			}
			curl_close($ch);
		} else {
			$html = '';
			$result_code = -1;
			$error = true;
		}
		
		$charset	= null;
		if ($html <> '') {
			if (preg_match('/charset\s*=\s*"*([^>\/\s"]*).*<\/head/si', $html, $m)) {
				$m[1] = trim(trim($m[1]), '\'\"');
				$charset = $m[1];
			} else {
				foreach(array('UTF-8','SJIS','EUC-JP','eucJP-win','ASCII','JIS','SJIS-win') as $c_charset) {
					if (mb_convert_encoding($html, 'UTF-8', $c_charset) == $html) {
						$charset = $c_charset;
						break;
					}
				}
			}
			if (is_null($charset)) {
				$charset = mb_detect_encoding($html, 'ASCII,JIS,UTF-7,EUC-JP,SJIS,UTF-8');
				$html = mb_convert_encoding($html, 'UTF-8', 'ASCII,JIS,UTF-7,EUC-JP,SJIS,UTF-8');
			} elseif ('UTF-8' <> $charset) {
				$html = mb_convert_encoding($html, 'UTF-8', $charset);
			}
			
			$head = null;
			$tags = null;
			if(preg_match('/<\s*head[^>]*>(.*)<\s*\/head\s*>/si', $html, $m)){
				$head	= $m[1];
				$tags	= self::getMeta($head);
			}
			
			if(isset($tags['og:title']) && $tags['og:title']){
				$title = $tags['og:title'];
			}elseif(isset($tags['twitter:title']) && $tags['twitter:title']){
				$title = $tags['twitter:title'];
			}elseif(isset($tags['title']) && $tags['title']){
				$title = $tags['title'];
			}
			
			if(isset($tags['og:description']) && $tags['og:description']){
				$excerpt = $tags['og:description'];
			}elseif(isset($tags['twitter:description']) && $tags['twitter:description']){
				$excerpt = $tags['twitter:description'];
			}elseif(isset($tags['description'])	&& $tags['description']) {
				$excerpt = $tags['description'];
			}
			
			if(isset(	$tags['og:image']) && $tags['og:image']){
				$thumbnail_url = $tags['og:image'];
			}elseif(isset($tags['twitter:image']) && $tags['twitter:image']) {
				$thumbnail_url = $tags['twitter:image'];
			} else {
				$thumbnail_url = '';
			}
			if($thumbnail_url && !preg_match('/^https*:\/\//', $thumbnail_url, $m) ) {
				$thumbnail_url = self::relToURL($url, $thumbnail_url);
			}
			
			if(isset($tags['icon']) && $tags['icon']) {
				$favicon_url = $tags['icon'];
			}elseif(isset($tags['shortcut icon']) && $tags['shortcut icon']){
				$favicon_url = $tags['shortcut icon'];
			}elseif(isset($tags['apple-touch-icon']) && $tags['apple-touch-icon']){
				$favicon_url = $tags['apple-touch-icon'];
			}else{
				$favicon_url = '';
			}
			if($favicon_url && !preg_match('/^https*:\/\//', $favicon_url, $m) ) {
				$favicon_url = self::relToURL($url, $favicon_url);
			}
			
			if(isset($tags['og:site_name']) && $tags['og:site_name']){
				$site_name = $tags['og:site_name']	;
			}
			
			if (isset($title)) {
				$str = $title;
				$str = strip_tags($str);
				$str = str_replace(array("\r", "\n"),	'', $str);
				$str = mb_strimwidth($str, 0, 200, '...');
				$title = $str;
			}
			
			if (isset($excerpt)) {
				$str = $excerpt;
				$str = strip_tags($str);
				$str = str_replace(array("\r", "\n"),	'', $str);
				$str = mb_strimwidth($str, 0, 250, '...');
				$excerpt = $str;
			}
			
			if(isset($data_id) && !is_null($data_id)){
				$data['id'] = $data_id;
			}
			if(isset($url_key) && !is_null($url_key)){
				$data['url_key'] = $url_key;
			}
			$data['site_name'] =	$site_name;
			$data['title'] =	$title;
			$data['excerpt'] =	$excerpt;
			$data['mod_title'] =	0;
			$data['mod_excerpt'] =	0;
			$data['charset'] =	$charset;
		}
		$data['url'] =	$url;
		$data['site_name'] = ( isset($site_name) ? $site_name : null );
		$data['thumbnail'] = ( isset($thumbnail_url) ? $thumbnail_url : null );
		$data['title'] = ( isset($title) ? $title : null );
		$data['excerpt'] = ( isset($excerpt) ? $excerpt : null );
		$data['result_code'] = $result_code;
		$data['alive_result'] = $result_code;
		$data['scheme'] = $scheme;
		$data['domain'] = $domain;
		$data['location'] = $location;
		$data['favicon'] = ( isset($favicon_url) ? $favicon_url : null );
		$data['sns_twitter'] = (isset($data['sns_twitter']) ? $data['sns_twitter'] : -1);
		$data['sns_facebook'] = (isset($data['sns_facebook']) ? $data['sns_facebook'] : -1);
		$data['sns_hatena'] = (isset($data['sns_hatena']) ? $data['sns_hatena'] : -1);
		$data['sns_nexttime'] = (isset( $data['sns_nexttime']) ? $data['sns_nexttime'] : 0);
		$data['alive_result'] = $result_code;
		return $data;
	}

	public static function relToURL( $base_url = '', $rel_path = '' ){
		if(preg_match('/^https?\:\/\//', $rel_path)){
			return $rel_path;
		}elseif(substr($rel_path, 0, 2) == '//'){
			return $rel_path;
		}
		$parse = parse_url($base_url);
		if (substr($rel_path, 0, 1) == '/' ) {
			return $parse['scheme'].'://'.$parse ['host'].$rel_path;
		}
		return $parse['scheme'].'://'.$parse['host'].dirname($parse['path'] ).'/'.$rel_path;
	}

	public static function getMeta( $html, $tags = null, $clear = false ) {
		if ($clear == true || !isset($tags)) {
			$tags = null;
			$tags = array('none' => 'none');
		}
		
		if (preg_match('/<\s*title\s*[^>]*>\s*([^<]*)\s*<\s*\/title\s*[^>]*>/si', $html, $m)) {
			$tags['title'] = $m[1];
		}
		
		$match = null;
		preg_match_all('/<\s*meta\s(?=[^>]*?\b(?:name|property)\s*=\s*(?|"\s*([^"]*?)\s*"|\'\s*([^\']*?)\s*\'|([^"\'>]*?)(?=\s*\/?\s*>|\s\w+\s*=)))[^>]*?\bcontent\s*=\s*(?|"\s*([^"]*?)\s*"|\'\s*([^\']*?)\s*\'|([^"\'>]*?)(?=\s*\/?\s*>|\s\w+\s*=))[^>]*>/is', $html, $match);
		if (isset($match) && is_array($match) && count($match) == 3 && count($match[1]) > 0) {
			foreach ($match[1] as &$m) {
				$m	= strtolower($m);
			}
			unset($m);
			$tags += array_combine($match[1], $match[2]);
		}
		
		$match = null;
		preg_match_all('/<\s*link\s(?=[^>]*?\brel\s*=\s*(?|"\s*([^"]*?)\s*"|\'\s*([^\']*?)\s*\'|([^"\'>]*?)(?=\s*\/?\s*>|\s\w+\s*=)))[^>]*?\bhref\s*=\s*(?|"\s*([^"]*?)\s*"|\'\s*([^\']*?)\s*\'|([^"\'>]*?)(?=\s*\/?\s*>|\s\w+\s*=))[^>]*>/is', $html, $match);
		if (isset($match) && is_array($match) && count($match) == 3 && count($match[1]) > 0) {
			foreach ($match[1] as &$m) {
				$m	= strtolower($m);
			}
			unset($m);
			$tags += array_combine($match[1], $match[2]);
		}
		
		return $tags;
	}
}