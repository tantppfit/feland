@extends('frontend.layouts.app')
@section('content')
	<main>
		<div class="shop-index-page">
			<div class="shop-container">
				<div class="row m-0">
					<div class="top-control w-100">
						<div class="row control-bar">
							<div class="col-12">
								<div class="top-nav">
									<ul>
										<li>
											<a href="#">
												<button>
													<div>
														<span>MUA BÁN</span>
													</div>
												</button>
											</a>
										</li>
										<li>
											<a href="#">
												<button>
													<div>
														<span>CHO THUÊ</span>
													</div>
												</button>
											</a>
										</li>
										<li>
											<a href="#">
												<button>
													<div>
														<span>DỰ ÁN</span>
													</div>
												</button>
											</a>
										</li>
										<li class="active">
											<a href="#">
												<button>
													<div>
														<span>CHUYÊN TRANG BĐS</span>
													</div>
												</button>
											</a>
										</li>
										<li>
											<a href="#">
												<button>
													<div>
														<span>KINH NGHIỆM MUA BÁN</span>
													</div>
												</button>
											</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="row m-0 col-12 main-filter">
								<div class="col-12 col-md-7 p-0">
									<div class="search-field">
										<form action="">
											<input class="search-input" type="text" name="" placeholder="Nhập từ khoá tìm kiếm">
											<span class="btn p-0">
												<button>
													<div>
														<i class="right-icon">
															<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="25" viewBox="0 0 512 512" fill="#fff">
																<path d="M344.5 298c15-23.6 23.8-51.6 23.8-81.7 0-84.1-68.1-152.3-152.1-152.3C132.1 64 64 132.2 64 216.3c0 84.1 68.1 152.3 152.1 152.3 30.5 0 58.9-9 82.7-24.4l6.9-4.8L414.3 448l33.7-34.3-108.5-108.6 5-7.1zm-43.1-166.8c22.7 22.7 35.2 52.9 35.2 85s-12.5 62.3-35.2 85c-22.7 22.7-52.9 35.2-85 35.2s-62.3-12.5-85-35.2c-22.7-22.7-35.2-52.9-35.2-85s12.5-62.3 35.2-85c22.7-22.7 52.9-35.2 85-35.2s62.3 12.5 85 35.2z"></path>
															</svg>
														</i>
													</div>
												</button>
											</span>
										</form>
									</div>
								</div>
								<div class="col-8 col-md-3 p-0 filter-tab region">
									<button>
										<div>
											<span style="position:relative;padding-left:16px;padding-right:8px;vertical-align:middle;letter-spacing:0;text-transform:capitalize;font-weight:500;font-size:14px">Toàn quốc</span>
											<span color="rgba(0, 0, 0, 0.87)" style="color:rgba(0, 0, 0, 0.87);position:absolute;font-size:13px;display:inline-block;user-select:none;transition:all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;vertical-align:middle;margin-left:0;margin-right:12px;right:0">
												<i class="fas fa-chevron-down"></i>
											</span>
										</div>
									</button>
								</div>
								<div class="col-4 col-md-2 p-0 justify-content-center filter-tab view">
									<button>
										<div>
											<!-- <span style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; overflow: hidden; pointer-events: none; z-index: 1;"></span> -->
											<svg width="37" height="30" viewBox="0 0 37 30" xmlns="http://www.w3.org/2000/svg" style="width:23px;height:23px">
												<title>Group 21</title>
												<g stroke-width="2" stroke="#979797" fill="none" fill-rule="evenodd" opacity=".5">
													<path d="M1 1h5.233v5.048H1zM11.126 1h24.037v5.048H11.126zM1 12.277h5.233v5.048H1zM11.126 12.277h24.037v5.048H11.126z"></path>
													<g>
														<path d="M1 23.554h5.233v5.048H1zM11.126 23.554h24.037v5.048H11.126z"></path>
													</g>
												</g>
											</svg>
										</div>
									</button>
									<button>
										<div>
											<svg width="36" height="30" viewBox="0 0 36 30" xmlns="http://www.w3.org/2000/svg" style="width:23px;height:23px">
												<title>Group 20</title>
												<g stroke-width="2" stroke="#CACACA" fill="none" fill-rule="evenodd">
													<path d="M1.711 1h13.394v10.658H1.711zM21.416 1H34.81v10.658H21.416z"></path>
													<g>
														<path d="M1.711 17.553h13.394V28.21H1.711zM21.416 17.553H34.81V28.21H21.416z"></path>
													</g>
												</g>
											</svg>
										</div>
									</button>
								</div>
							</div>
						</div>
						<div class="row content-header m-o d-none d-md-block">
							<div class="col-12">
								<div class="bread-crumb">
									<ul>
										<li><a href="#">Chợ Tốt Nhà</a></li>
										<li><a href="#">Toàn quốc</a></li>
										<li class="active"><a href="#">Tất cả loại bất động sản</a></li>
									</ul>
								</div>
								<h1 class="title">Danh bạ Chuyên trang BĐS tại<!-- --> <!-- -->Toàn quốc</h1>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="shop-container">
				<div style="color:rgba(0, 0, 0, 0.87);background-color:#ffffff;transition:all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;box-sizing:border-box;font-family:Verdana, Arial, sans-serif;-webkit-tap-highlight-color:rgba(0,0,0,0);box-shadow:0 1px 2px rgba(0,0,0,.1);border-radius:2px">
					<div class="row m-0" style="background-color: #fff;">
						<div class="col-12 p-0">
							<div class="horizontal-button">
								<ul>
									<li class="active">
										<a href="#">
											<button>
												<div>
													<span>hoạt động gần nhất</span>
												</div>
											</button>
										</a>
									</li>
									<li>
										<a href="#">
											<button>
												<div>
													<span>nhiều dự án nhất</span>
												</div>
											</button>
										</a>
									</li>
									<li>
										<a href="#">
											<button>
												<div>
													<span>nhiều tin đăng nhất</span>
												</div>
											</button>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="shop-main">
					<div class="row m-0">
						<!-- 12 items -->
						<div class="col-6 col-sm-6 col-md-3 shop-card">
							<div>
								<div class="pb-0">
									<a href="{{ route('frontend.shop.page') }}" class="shop-item">
										<div class="position-relative">
											<div>
												<img src="{{ asset('image/shop-1.jpg') }}" alt="" class="cover-photo" style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
												<div style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
													<img src="{{ asset('image/shop-avatar-1.jpg') }}" alt="" class="shop-avatar">
												</div>
											</div>
										</div>
										<div style="padding: 0px 10px; font-size: 14px; color: rgba(0, 0, 0, 0.87); text-align: center;">
											<p class="text primary-text">Chuyên cho thuê và chuyển nhượng BĐS Novaland</p>
											<p class="text secondary-text">
												<i class="fas fa-map-marker-alt"></i>
												<span class="address">08 Hoàng Minh Giám, phường 9, Phú Nhuận, Hồ Chí Minh, Việt Nam</span>
											</p>
										</div>
									</a>
									<div class="position-relative p-0">
										<a href="#" class="map grid-map with-market">
											<span class="overlay d-flex align-items-center justify-content-center text-center">
												<span>8 <span>dự án</span></span>
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-3 shop-card">
							<div>
								<div class="pb-0">
									<a href="{{ route('frontend.shop.page') }}" class="shop-item">
										<div class="position-relative">
											<div>
												<img src="{{ asset('image/shop-2.jpg') }}" alt="" class="cover-photo" style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
												<div style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
													<img src="{{ asset('image/shop-avatar-2.jpg') }}" alt="" class="shop-avatar">
												</div>
											</div>
										</div>
										<div style="padding: 0px 10px; font-size: 14px; color: rgba(0, 0, 0, 0.87); text-align: center;">
											<p class="text primary-text">Chuyển nhà đất thổ cư Hà Nội</p>
											<p class="text secondary-text">
												<i class="fas fa-map-marker-alt"></i>
												<span class="address">Quận Hai Bà Trưng, Hanoi Vietnam</span>
											</p>
										</div>
									</a>
									<div class="position-relative p-0">
										<a href="#" class="map grid-map no-market">
											<span class="overlay d-flex align-items-center justify-content-center text-center">
												<span>0 <span>dự án</span></span>
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-3 shop-card">
							<div>
								<div class="pb-0">
									<a href="{{ route('frontend.shop.page') }}" class="shop-item">
										<div class="position-relative">
											<div>
												<img src="{{ asset('image/shop-3.jpg') }}" alt="" class="cover-photo" style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
												<div style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
													<img src="{{ asset('image/shop-avatar-3.jpg') }}" alt="" class="shop-avatar">
												</div>
											</div>
										</div>
										<div style="padding: 0px 10px; font-size: 14px; color: rgba(0, 0, 0, 0.87); text-align: center;">
											<p class="text primary-text">KĐT Ven Sông - Thiên Đường Nghĩ Dưỡng</p>
											<p class="text secondary-text">
												<i class="fas fa-map-marker-alt"></i>
												<span class="address">số 1 trường chinh phường tây thạnh tân phú tphcm</span>
											</p>
										</div>
									</a>
									<div class="position-relative p-0">
										<a href="#" class="map grid-map no-market">
											<span class="overlay d-flex align-items-center justify-content-center text-center">
												<span>0 <span>dự án</span></span>
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-3 shop-card">
							<div>
								<div class="pb-0">
									<a href="{{ route('frontend.shop.page') }}" class="shop-item">
										<div class="position-relative">
											<div>
												<img src="{{ asset('image/shop-4.jpg') }}" alt="" class="cover-photo" style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
												<div style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
													<img src="{{ asset('image/shop-avatar-4.jpg') }}" alt="" class="shop-avatar">
												</div>
											</div>
										</div>
										<div style="padding: 0px 10px; font-size: 14px; color: rgba(0, 0, 0, 0.87); text-align: center;">
											<p class="text primary-text">Chuyên trang BĐS NHÀ ĐẸP GIÁ RẺ Q7 - NHÀ BÈ</p>
											<p class="text secondary-text">
												<i class="fas fa-map-marker-alt"></i>
												<span class="address">1979 Huỳnh Tấn Phát, TP.HCM</span>
											</p>
										</div>
									</a>
									<div class="position-relative p-0">
										<a href="#" class="map grid-map with-market">
											<span class="overlay d-flex align-items-center justify-content-center text-center">
												<span>20 <span>dự án</span></span>
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-3 shop-card">
							<div>
								<div class="pb-0">
									<a href="{{ route('frontend.shop.page') }}" class="shop-item">
										<div class="position-relative">
											<div>
												<img src="{{ asset('image/shop-1.jpg') }}" alt="" class="cover-photo" style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
												<div style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
													<img src="{{ asset('image/shop-avatar-1.jpg') }}" alt="" class="shop-avatar">
												</div>
											</div>
										</div>
										<div style="padding: 0px 10px; font-size: 14px; color: rgba(0, 0, 0, 0.87); text-align: center;">
											<p class="text primary-text">Chuyên cho thuê và chuyển nhượng BĐS Novaland</p>
											<p class="text secondary-text">
												<i class="fas fa-map-marker-alt"></i>
												<span class="address">08 Hoàng Minh Giám, phường 9, Phú Nhuận, Hồ Chí Minh, Việt Nam</span>
											</p>
										</div>
									</a>
									<div class="position-relative p-0">
										<a href="#" class="map grid-map with-market">
											<span class="overlay d-flex align-items-center justify-content-center text-center">
												<span>8 <span>dự án</span></span>
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-3 shop-card">
							<div>
								<div class="pb-0">
									<a href="{{ route('frontend.shop.page') }}" class="shop-item">
										<div class="position-relative">
											<div>
												<img src="{{ asset('image/shop-2.jpg') }}" alt="" class="cover-photo" style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
												<div style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
													<img src="{{ asset('image/shop-avatar-2.jpg') }}" alt="" class="shop-avatar">
												</div>
											</div>
										</div>
										<div style="padding: 0px 10px; font-size: 14px; color: rgba(0, 0, 0, 0.87); text-align: center;">
											<p class="text primary-text">Chuyển nhà đất thổ cư Hà Nội</p>
											<p class="text secondary-text">
												<i class="fas fa-map-marker-alt"></i>
												<span class="address">Quận Hai Bà Trưng, Hanoi Vietnam</span>
											</p>
										</div>
									</a>
									<div class="position-relative p-0">
										<a href="#" class="map grid-map no-market">
											<span class="overlay d-flex align-items-center justify-content-center text-center">
												<span>0 <span>dự án</span></span>
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-3 shop-card">
							<div>
								<div class="pb-0">
									<a href="{{ route('frontend.shop.page') }}" class="shop-item">
										<div class="position-relative">
											<div>
												<img src="{{ asset('image/shop-3.jpg') }}" alt="" class="cover-photo" style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
												<div style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
													<img src="{{ asset('image/shop-avatar-3.jpg') }}" alt="" class="shop-avatar">
												</div>
											</div>
										</div>
										<div style="padding: 0px 10px; font-size: 14px; color: rgba(0, 0, 0, 0.87); text-align: center;">
											<p class="text primary-text">KĐT Ven Sông - Thiên Đường Nghĩ Dưỡng</p>
											<p class="text secondary-text">
												<i class="fas fa-map-marker-alt"></i>
												<span class="address">số 1 trường chinh phường tây thạnh tân phú tphcm</span>
											</p>
										</div>
									</a>
									<div class="position-relative p-0">
										<a href="#" class="map grid-map no-market">
											<span class="overlay d-flex align-items-center justify-content-center text-center">
												<span>0 <span>dự án</span></span>
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-3 shop-card">
							<div>
								<div class="pb-0">
									<a href="{{ route('frontend.shop.page') }}" class="shop-item">
										<div class="position-relative">
											<div>
												<img src="{{ asset('image/shop-4.jpg') }}" alt="" class="cover-photo" style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
												<div style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
													<img src="{{ asset('image/shop-avatar-4.jpg') }}" alt="" class="shop-avatar">
												</div>
											</div>
										</div>
										<div style="padding: 0px 10px; font-size: 14px; color: rgba(0, 0, 0, 0.87); text-align: center;">
											<p class="text primary-text">Chuyên trang BĐS NHÀ ĐẸP GIÁ RẺ Q7 - NHÀ BÈ</p>
											<p class="text secondary-text">
												<i class="fas fa-map-marker-alt"></i>
												<span class="address">1979 Huỳnh Tấn Phát, TP.HCM</span>
											</p>
										</div>
									</a>
									<div class="position-relative p-0">
										<a href="#" class="map grid-map with-market">
											<span class="overlay d-flex align-items-center justify-content-center text-center">
												<span>20 <span>dự án</span></span>
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-3 shop-card">
							<div>
								<div class="pb-0">
									<a href="{{ route('frontend.shop.page') }}" class="shop-item">
										<div class="position-relative">
											<div>
												<img src="{{ asset('image/shop-1.jpg') }}" alt="" class="cover-photo" style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
												<div style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
													<img src="{{ asset('image/shop-avatar-1.jpg') }}" alt="" class="shop-avatar">
												</div>
											</div>
										</div>
										<div style="padding: 0px 10px; font-size: 14px; color: rgba(0, 0, 0, 0.87); text-align: center;">
											<p class="text primary-text">Chuyên cho thuê và chuyển nhượng BĐS Novaland</p>
											<p class="text secondary-text">
												<i class="fas fa-map-marker-alt"></i>
												<span class="address">08 Hoàng Minh Giám, phường 9, Phú Nhuận, Hồ Chí Minh, Việt Nam</span>
											</p>
										</div>
									</a>
									<div class="position-relative p-0">
										<a href="#" class="map grid-map with-market">
											<span class="overlay d-flex align-items-center justify-content-center text-center">
												<span>8 <span>dự án</span></span>
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-3 shop-card">
							<div>
								<div class="pb-0">
									<a href="{{ route('frontend.shop.page') }}" class="shop-item">
										<div class="position-relative">
											<div>
												<img src="{{ asset('image/shop-2.jpg') }}" alt="" class="cover-photo" style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
												<div style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
													<img src="{{ asset('image/shop-avatar-2.jpg') }}" alt="" class="shop-avatar">
												</div>
											</div>
										</div>
										<div style="padding: 0px 10px; font-size: 14px; color: rgba(0, 0, 0, 0.87); text-align: center;">
											<p class="text primary-text">Chuyển nhà đất thổ cư Hà Nội</p>
											<p class="text secondary-text">
												<i class="fas fa-map-marker-alt"></i>
												<span class="address">Quận Hai Bà Trưng, Hanoi Vietnam</span>
											</p>
										</div>
									</a>
									<div class="position-relative p-0">
										<a href="#" class="map grid-map no-market">
											<span class="overlay d-flex align-items-center justify-content-center text-center">
												<span>0 <span>dự án</span></span>
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-3 shop-card">
							<div>
								<div class="pb-0">
									<a href="{{ route('frontend.shop.page') }}" class="shop-item">
										<div class="position-relative">
											<div>
												<img src="{{ asset('image/shop-3.jpg') }}" alt="" class="cover-photo" style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
												<div style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
													<img src="{{ asset('image/shop-avatar-3.jpg') }}" alt="" class="shop-avatar">
												</div>
											</div>
										</div>
										<div style="padding: 0px 10px; font-size: 14px; color: rgba(0, 0, 0, 0.87); text-align: center;">
											<p class="text primary-text">KĐT Ven Sông - Thiên Đường Nghĩ Dưỡng</p>
											<p class="text secondary-text">
												<i class="fas fa-map-marker-alt"></i>
												<span class="address">số 1 trường chinh phường tây thạnh tân phú tphcm</span>
											</p>
										</div>
									</a>
									<div class="position-relative p-0">
										<a href="#" class="map grid-map no-market">
											<span class="overlay d-flex align-items-center justify-content-center text-center">
												<span>0 <span>dự án</span></span>
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-3 shop-card">
							<div>
								<div class="pb-0">
									<a href="{{ route('frontend.shop.page') }}" class="shop-item">
										<div class="position-relative">
											<div>
												<img src="{{ asset('image/shop-4.jpg') }}" alt="" class="cover-photo" style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
												<div style="vertical-align: top; max-width: 100%; min-width: 100%; width: 100%;">
													<img src="{{ asset('image/shop-avatar-4.jpg') }}" alt="" class="shop-avatar">
												</div>
											</div>
										</div>
										<div style="padding: 0px 10px; font-size: 14px; color: rgba(0, 0, 0, 0.87); text-align: center;">
											<p class="text primary-text">Chuyên trang BĐS NHÀ ĐẸP GIÁ RẺ Q7 - NHÀ BÈ</p>
											<p class="text secondary-text">
												<i class="fas fa-map-marker-alt"></i>
												<span class="address">1979 Huỳnh Tấn Phát, TP.HCM</span>
											</p>
										</div>
									</a>
									<div class="position-relative p-0">
										<a href="#" class="map grid-map with-market">
											<span class="overlay d-flex align-items-center justify-content-center text-center">
												<span>20 <span>dự án</span></span>
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div>
						<div class="text-center">
							<ul class="project-pagination">
								<li class="project-pagination-item d-inline">
									<a class="first" href="#">«</a>
								</li>
								<li class="project-pagination-item d-inline">
									<a class="prev" href="#">‹</a>
								</li>
								<li class="project-pagination-item d-inline">
									<a href="#">1</a>
								</li>
								<li class="project-pagination-item d-inline">
									<a href="#">2</a>
								</li>
								<li class="project-pagination-item d-inline">
									<a href="#">3</a>
								</li>
								<li class="project-pagination-item d-inline">
									<a class="active" href="#">4</a>
								</li>
								<li class="project-pagination-item d-inline">
									<a href="#">5</a>
								</li>
								<li class="project-pagination-item d-inline">
									<a href="#">6</a>
								</li>
								<li class="project-pagination-item d-inline">
									<a href="#">7</a>
								</li>
								<li class="project-pagination-item d-inline">
									<a href="#">›</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@endsection