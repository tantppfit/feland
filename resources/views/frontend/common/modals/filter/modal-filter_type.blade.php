<!-- MODAL FILTER TYPE -->
<div class="modal fade" id="dynamic-filter-type" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="dynamic-filter-modal-dialog modal-dialog" role="document">
		<div class="dynamic-filter-modal-content modal-content">
			<div class="dynamic-filter-modal-header">
				<h4 class="text-center dymanic-filter-modal-title">Mục</h4>
				<button type="button" class="close float-left" data-dismiss="modal" aria-label="Close">
					<span class="close-btn" aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="dynamic-filter-modal-body">
				<div>
					<h2 class="h2-label"></h2>
					<div class="without-modal-group">
						<div class="layout-group">
							<div class="layout-group-wrapper">
								<div class="layout-group-item">
									<div class="label-item">
										Tất cả
									</div>
									<div class="radio-button">
										<!-- <label for="" class="label-button label-button-checked"></label> -->
										<input type="radio" name="1" value="" checked="">
									</div>
								</div>
							</div>
						</div>
						<div class="layout-group">
							<div class="layout-group-wrapper">
								<div class="layout-group-item">
									<div class="label-item">
										Căn hộ/Chung cư
									</div>
									<div class="radio-button">
										<!-- <label for="" class="label-button"></label> -->
										<input type="radio" name="1" value="">
									</div>
								</div>
							</div>
						</div>
						<div class="layout-group">
							<div class="layout-group-wrapper">
								<div class="layout-group-item">
									<div class="label-item">
										Nhà ở
									</div>
									<div class="radio-button">
										<!-- <label for="" class="label-button"></label> -->
										<input type="radio" name="1" value="">
									</div>
								</div>
							</div>
						</div>
						<div class="layout-group">
							<div class="layout-group-wrapper">
								<div class="layout-group-item">
									<div class="label-item">
										Đất
									</div>
									<div class="radio-button">
										<!-- <label for="" class="label-button"></label> -->
										<input type="radio" name="1" value="">
									</div>
								</div>
							</div>
						</div>
						<div class="layout-group">
							<div class="layout-group-wrapper">
								<div class="layout-group-item">
									<div class="label-item">
										Văn phòng, Mặt bằng kinh doanh
									</div>
									<div class="radio-button">
										<!-- <label for="" class="label-button"></label> -->
										<input type="radio" name="1" value="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="margin-bottom: 30px;"></div>
			</div>
		</div>
	</div>
</div>