<?php
namespace App\Policies\AbstractPolicy;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

abstract class AbstractPolicy {
	use HandlesAuthorization;
	protected $model;
	abstract protected function index(User $user);
	abstract protected function edit(User $user, $model);
	abstract protected function create(User $user);
	abstract protected function update(User $user, $model);
	abstract protected function store(User $user);
	abstract protected function destroy(User $user, $model);
}
