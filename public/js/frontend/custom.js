jQuery('document').ready(function($){
	var MAX_HEIGHT_CONTENT_TAB = 2000;	
	if($('.article-content.post-content').length){
		if($('.article-content.post-content .tab-contents').length){
			//splitContent($('.article-content.post-content'));
      setupSlider($('.tab-content.active'));
		}else{
      setupSlider($('.article-content.post-content'));
    }		
	}

	$('body').delegate('.tab-links a', 'click', function(e){
		e.preventDefault();
		$(this).addClass('active').siblings().removeClass('active');
		var postContent = $(this).parents('.post-content');
		var tabIndex = $(this).data('tab');
		var tabContent = $(postContent).find('.tab-content[data-tab='+tabIndex+']');
		$(tabContent).show().siblings().hide();
    setupSlider(tabContent);
		$('html, body').animate({
      scrollTop: $(tabContent).offset().top
    }, 500);
	});

  $('body').delegate('.next-tab', 'click', function(e){
    e.preventDefault();
    var postContent = $(this).parents('.post-content');
    var tabIndex = $(this).data('tab');
    var tabLink = $(postContent).find('.tab-links a[data-tab='+tabIndex+']');
    $(tabLink).trigger('click');
  });
	
  function getWidthCarousel(carousel){
  	//var width = $('.post-slide-images #carousel').width() / 5;
    var width = ($(carousel).width() - 20 ) / 5;
  	return width;
  }
  function setupSlider(container) {
    $(container).find('.post-slide-images').each(function() {
      var _this = $(this);
      var sliderId = $(this).data('slider-id');
      $('#carousel-'+sliderId).flexslider('destroy');
      $('#slider-'+sliderId).flexslider('destroy');

      $('#carousel-'+sliderId).flexslider({
        animation: "slide",
        controlNav: false,
        slideshow: false,
        itemMargin: 5,
        itemWidth: getWidthCarousel('#carousel-'+sliderId),
        asNavFor: '#slider-'+sliderId
      });

      $('#slider-'+sliderId).flexslider({
        animation: "slide",
        controlNav: false,
        slideshow: true,
        slideshowSpeed: 3000,
        sync: "#carousel-"+sliderId,
        start: function(slider) {
          $('.post-slide-images #slider-'+sliderId+' .flex-pauseplay .flex-pause').trigger('click');
        }
      });
    });
  }

  $('.post-slide-images .wrap-control .prev-slide').click( function(e){
  	e.preventDefault();
    var postSlider = $(this).parents('.post-slide-images');
  	$(postSlider).find('.slider .flex-prev').trigger('click');
  });

  $('.post-slide-images .wrap-control .pauseplay-slide').click( function(e){
  	e.preventDefault();
    var postSlider = $(this).parents('.post-slide-images');
  	$(this).find('i').toggleClass('fa-play').toggleClass('fa-pause');
  	$(postSlider).find('.slider .flex-pauseplay a').trigger('click');
  });
  // main slider
  $('.post-main-flexslider').flexslider({
    animation: "fade",
    slideshow: false,
    controlNav: "thumbnails",
    startAt: ($('#slide-focus').index() > 0) ? $('#slide-focus').index() : 0,
    start: function(slider){
      $(slider).find('.slides').wrapAll('<div class="wrap-slides"></div>');
      $(slider).find('.flex-direction-nav').appendTo('.wrap-slides');
    }
  });
  //clickToCopy
  function clickToCopy($input) {
    $input.select();
    document.execCommand("copy");
  }
  $('body').delegate('.btn-copy-text', 'click', function(e){
    e.preventDefault();
    var inputId = $(this).data('input');
    var input = $('#'+inputId);
    clickToCopy(input);
  });
  //post vote
  $('body').delegate('#form-vote', 'change', function(e){
    $(this).trigger('submit');
  });
  $('body').delegate('#form-vote', 'submit', function(e){
    e.preventDefault();
    var _this = $(this);
    var voteSection = $(this).parents('.vote-section');
    var radioId = $(this).find('input[name="vote_opt"]:checked').attr('id');
    $(this).addClass('submitting');
    var action = $(this).attr('action');
    var postData = $(this).serializeArray();
    $.ajax({
      url: action,
      method: "POST",
      data: postData,
      dataType: 'JSON',
      success: function(data)
      {
        if(data.status){
          $(voteSection).html(data.html);
          if(data.isVote){
            var input = $(voteSection).find('#'+radioId);
            $(input).parents('.vote-select').siblings().each(function(e){
              $(this).find('input').prop('disabled', true);
              $(this).find('label').addClass('disabled');
            });
            $(input).prop("checked", true).addClass('checked');
            $(voteSection).find('.message').show();
            $(voteSection).find('.message-1').hide();
          }else{
            $(voteSection).find('.message').hide();
            $(voteSection).find('.message-1').show();
          }
        }
      },
      error: function(data){
        alert('Error');
        $(_this).removeClass('submitting');
      }
    });
  });
  $('body').delegate('#form-vote .vote-select input', 'click', function(e){
    e.preventDefault();
    var input = $(this);
    var _thisForm = $(this).parents('#form-vote');
    if($(input).hasClass('checked')) {
      var voteOpt = $(input).val();
      $(_thisForm).find('.un-vote').val(voteOpt);
      $(input).prop('checked', false);
    }
    $(_thisForm).trigger('submit');
  });

  //single load more post
  var lastScrollTop = 0;
  $(window).on('scroll', function () {
    var st = $(document).scrollTop();
    if (st > lastScrollTop){
      var winHeight = $(window).height(),
        docHeight = $(document).height(),
        docScroll = $(document).scrollTop() + winHeight,
        docPer = docHeight * 80 / 100;
      if ($('#form-load-more').length !== 0 && docScroll >= docPer) {
        var loading = $('body').data('ajax');
        if (!loading) {
          $('#form-load-more').trigger('submit');
          
        }
      }
    }
    lastScrollTop = st;
  });
  $('#form-load-more').submit(function(e){
    e.preventDefault();
    var _thisForm = $(this);
    var action = $(this).attr('action');
    var postData = $(this).serializeArray();
    $.ajax({
      url: action,
      method: "POST",
      data: postData,
      dataType: 'JSON',
      beforeSend: function () {
        $('body').data('ajax', 1);
      },
      error: function () {
        $('body').data('ajax', 0);
      },
      success: function (data) {
        if(data.status){
          $('.main-article-details-page-content').append(data.html);
          $(_thisForm).append('<input type="hidden" name="list_post_id[]" value="'+ data.post_id +'" />');
          if($('#article-content-' + data.post_id).find('.tab-content.active').length){
            setupSlider($('#article-content-' + data.post_id).find('.tab-content.active'));
          }
        }
        $('body').data('ajax', 0);
      }
    });
  });
});