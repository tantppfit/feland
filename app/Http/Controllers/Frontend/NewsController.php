<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

/**
 * 
 */
class NewsController extends Controller
{
	
	function __construct()
	{
		
	}
	
	public function index() 
	{
		return view('frontend.templates.news.index');
	}

	public function page()
	{
		return view('frontend.templates.news.page');
	}

	public function post()
	{
		return view('frontend.templates.news.post');
	}
}