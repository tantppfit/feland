;(function () {
	
	'use strict';

	var readMore = function() {
		$('#btn-read-more').on('click', function() {
			$('.home-content-less-info').toggleClass('read-more');
			$('.description-read-less-gradient').toggleClass('active');
			$('#btn-read-more').css('display','none');
			$('#btn-read-less').css('display','block');
		})
	};

	var readLess = function() {
		$('#btn-read-less').on('click', function() {
			$('.home-content-less-info').removeClass('read-more');
			$('.description-read-less-gradient').removeClass('active');
			$('#btn-read-more').css('display','block');
			$('#btn-read-less').css('display','none');
		})
	};

	var menuMore = function() {
		$('#app-header-menu-more').on('click', function() {
			$('.app-header-menu').toggleClass('active');
		});
		$(document).on('click', function(e) {
			var container = $('#app-header-menu-more');
			if (!container.is(e.target) && container.has(e.target).length === 0) {
				$('.app-header-menu').removeClass('active');
			}
		});
	};

	var scrollSlipDynamicFilter = function() {
		let zero = 0;
		$(window).on('scroll', function() {
			$('.dynamic-filter-wrapper').toggleClass('dynamic-filter-slip-up', $(window).scrollTop() > zero);
			zero = $(window).scrollTop();
		});
	};

	var viewMode = function() {
		$('.view-mode').delegate('.grid-mode','click', function() {
			$('.grid-mode').addClass('list-mode');
			$('.grid-mode').removeClass('grid-mode');
			if ('.list-mode') {
				$('.list-view-wrapper-list').css('display','none');
				$('.grid-view-wrapper-list').css('display','flex');
			}
		});
		$('.view-mode').delegate('.list-mode','click', function() {
			$('.list-mode').addClass('grid-mode');
			$('.list-mode').removeClass('list-mode');
			if ('.grid-mode') {
				$('.list-view-wrapper-list').css('display','block');
				$('.grid-view-wrapper-list').css('display','none');
			}
		});
	};

	var seeMore = function() {
		let moreText = 'Mở rộng';
		let lessText = 'Thu gọn';
		$('.btn-see-more').on('click', function() {
			$('.content-cat-wrapper').toggleClass('see-more');
			if ($('.content-cat-wrapper').hasClass('see-more')) {
				$('.btn-see-more').html(lessText);
			} else {
				$('.btn-see-more').html(moreText);
			}
		});
	};

	var scrollToTop = function() {
		$(window).on('scroll', function() {
			let y = $(this).scrollTop();
			if (y > 500) {
				$('.scroll-top').addClass('active');
			} else {
				$('.scroll-top').removeClass('active');
			}
		});
		$('.scroll-top').on('click', function(e) {
			$('html, body').animate({scrollTop: 0}
			, 800);
		});
	};

	var showPhoneNumber = function() {
		let showChar = 6;
		let ellipsestext = " ***";
		let phoneNumber = $('.phone-number-hide').html();
		if (phoneNumber > showChar) {
			let hideChar = phoneNumber.substr(0, showChar);
			let text = hideChar + ellipsestext;
			$('.phone-number-hide').html(text);
		};
		$('.show-phone-button').on('click', function() {
			let html = '<span class="phone-number">' + phoneNumber + '</span>';
			$('.show-phone-wrapper').remove();
			$('.show-phone-button .w-100').html(html);
		});
	};

	var showMiniContract = function() {
		let showChar = 6;
		let ellipsestext = " ***";
		let phoneNumber = $('.mini-contract span strong').html();
		if (phoneNumber > showChar) {
			let hideChar = phoneNumber.substr(0, showChar);
			let text = hideChar + ellipsestext;
			$('.mini-contract span strong').html(text);
		};
		$('.mini-contract').on('click', function() {
			$('.mini-contract span strong').html(phoneNumber);
		});
	};

	var showPhoneNumberShop = function() {
		let showChar = 4;
		let ellipsestext = "******";
		let phoneNumber = $('.hidden-phone-number').html();
		if (phoneNumber > showChar) {
			let hideChar = phoneNumber.substr(0, showChar);
			let text = hideChar + ellipsestext;
			$('.hidden-phone-number').html(text);
		};
		$('.shop-contact-green-button').on('click', function() {
			let html = '<div class="shop-contact-pure-phone">' + phoneNumber + '</div>';
			$('.shop-contact-green-butto').remove();
			$('.shop-contact-box-desktop').html(html);
		});
	};

	var showMiniPhoneNumberShop = function() {
		let showChar = 4;
		let ellipsestext = "****";
		let phoneNumber = $('.mini-phone').html();
		if (phoneNumber > showChar) {
			let hideChar = phoneNumber.substr(0, showChar);
			let text = hideChar + ellipsestext;
			$('.mini-phone').html(text);
		};
		$('.show-mini-phone').on('click', function() {
			let html = 'Số điện thoại: <span class="shop-detail-info-value-black">' + phoneNumber + '</span>';
			$('.mini-phone').remove();
			$('.show-phone-number').html(html);
		});
	};

	var menuTabs = function() {
		function activeTab(obj) {
			$('.shop-menu-tab-box ul li').removeClass('active');
			$(obj).addClass('active');
			var id = $(obj).find('a').attr('href');
			$('.tab-item').hide();
			$(id).show();
		}

		$('.shop-menu-tab-box ul li').click(function() {
			activeTab(this);
			$("html, body").animate({ scrollTop: 0 }, 200);
			return false;
		});

		$('.view-more-ad a').click(function() {
			activeTab('.shop-menu-tab-box ul li:nth-child(2)');
			$("html, body").animate({ scrollTop: 0 }, 200);
			return false;
		});

		activeTab($('.shop-menu-tab-box ul li:first-child'));
	};

	var selectFilterCateItem = function() {
		$('.filter-cate-item').on('click', function() {
			$(this).toggleClass('active');
		});
	};

	var mobileMenuMore = function() {
		$('.app-header-nav-item-link').on('click', function() {
			$('.app-header-mobile-menu-more').toggleClass('active');
		});
	};

	var priceRange = function() {
		$(".range-price").slider({
			range:true,
			orientation:"horizontal",
			min: 0,
			max: 30000000000,
			values: [0, 30000000000],
			step: 100000000,

			slide:function (event, ui) {
				if (ui.values[0] == ui.values[1]) {
					return false;
				}
				$(".price-min").html(ui.values[0] + " ₫");
				$(".price-max").html(ui.values[1] + " ₫");
			}
		});
	};

	var swiper = new Swiper('.swiper-container-ads-slider-purchase', {
		slidesPerView: 'auto',
		slidesPerGroup: 4,
		loop: true,
		navigation: {
			nextEl: '#ads-slider-purchase .icon-next',
			prevEl: '#ads-slider-purchase .icon-prev',
		},
		breakpoints: {
			//when window width is <= 320px
			320: {
				slidesPerGroup: 1,
			},
			//when window width is <= 425px
			425: {
				slidesPerGroup: 2,
			},
			//when window width is <= 768px
			768: {
				slidesPerGroup: 3,
			},
			// when window width is <= 992px
	        992: {
				slidesPerGroup: 4,
			}
		}
	});

	var swiper = new Swiper('.swiper-container-ads-slider-lease', {
		slidesPerView: 'auto',
		slidesPerGroup: 4,
		loop: true
,		navigation: {
			nextEl: '#ads-slider-lease .icon-next',
			prevEl: '#ads-slider-lease .icon-prev',
		},
		breakpoints: {
			//when window width is <= 320px
			320: {
				slidesPerGroup: 1,
			},
			//when window width is <= 425px
			425: {
				slidesPerGroup: 2,
			},
			//when window width is <= 768px
			768: {
				slidesPerGroup: 3,
			},
			// when window width is <= 992px
	        992: {
				slidesPerGroup: 4,
			}
		}
	});

	var swiper = new Swiper('.swiper-container-ads-slider-interested', {
		slidesPerView: 'auto',
		slidesPerGroup: 4,
		loop: true,
		navigation: {
			nextEl: '#ads-slider-interested .icon-next',
			prevEl: '#ads-slider-interested .icon-prev',
		},
		breakpoints: {
			//when window width is <= 320px
			320: {
				slidesPerGroup: 1,
			},
			//when window width is <= 425px
			425: {
				slidesPerGroup: 2,
			},
			//when window width is <= 768px
			768: {
				slidesPerGroup: 3,
			},
			// when window width is <= 992px
	        992: {
				slidesPerGroup: 4,
			}
		}
	});

	var swiper = new Swiper('.swiper-container-ads-slider-page', {
		slidesPerView: 'auto',
		slidesPerGroup: 4,
		loop: true,
		navigation: {
			nextEl: '#ads-slider-page .icon-next',
			prevEl: '#ads-slider-page .icon-prev',
		},
		breakpoints: {
			//when window width is <= 320px
			320: {
				slidesPerGroup: 1,
			},
			//when window width is <= 425px
			425: {
				slidesPerGroup: 2,
			},
			//when window width is <= 768px
			768: {
				slidesPerGroup: 3,
			},
			// when window width is <= 992px
	        992: {
				slidesPerGroup: 4,
			}
		}
	});

	var swiper = new Swiper('.swiper-container-ads-slider-news', {
		slidesPerView: 'auto',
		slidesPerGroup: 4,
		loop: true,
		navigation: {
			nextEl: '#ads-slider-news .icon-next',
			prevEl: '#ads-slider-news .icon-prev',
		},
		breakpoints: {
			//when window width is <= 320px
			320: {
				slidesPerGroup: 1,
			},
			//when window width is <= 425px
			425: {
				slidesPerGroup: 2,
			},
			//when window width is <= 768px
			768: {
				slidesPerGroup: 3,
			},
			// when window width is <= 992px
	        992: {
				slidesPerGroup: 4,
			}
		}
	});

	var swiper = new Swiper('.ad-slider-container', {
		loop: true,
		navigation: {
			nextEl: '.ad-next',
			prevEl: '.ad-prev',
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
	});

	// var swiper = new Swiper('.project-img-slide-swiper-container', {
	// 	loop: false,
	// 	navigation: {
	// 		nextEl: '.project-img-slide-swiper-container>.ad-next',
	// 		prevEl: '.project-img-slide-swiper-container>.ad-prev',
	// 	},
	// 	pagination: {
	// 		el: '.project-img-slide-swiper-container>.project-swiper-pagination',
	// 		clickable: true,
	// 	},
	// });

	// function validateNumbertoPrice(input) {
	// 	return /\d{1,3}(?:[.,]\d{3})*(?:[.,]\d{2})/g.exec(input);
	// };

	// Document on load.
	$(function(){
		readMore();
		readLess();
		menuMore();
		scrollSlipDynamicFilter();
		viewMode();
		seeMore();
		scrollToTop();
		showPhoneNumber();
		showMiniContract();
		showPhoneNumberShop();
		showMiniPhoneNumberShop();
		menuTabs();
		selectFilterCateItem();
		mobileMenuMore();
		priceRange();
	});

}());
