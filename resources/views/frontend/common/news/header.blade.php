<header class="header">
	<div class="top-head">
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<div class="col-4 d-md-none"></div>
				<div class="col-md-2 col-4">
					<a href="{{ route('frontend.home.index') }}" class="d-block text-center text-md-left logo-site">
						<img src="{{ asset('image/logo-demo.png') }}" alt="Tin tức Bất động sản mới nhất của Chợ Tốt Nhà">
					</a>
				</div>
				<div class="col-4 d-md-none">
					<ul class="text-right toolbar">
						<li>
							<a href="#">
								<img src="{{ asset('image/icon/social.png') }}">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="{{ asset('image/icon/search.png') }}">
							</a>
						</li>
					</ul>
				</div>
				<div class="col-md-auto">
					<div class="d-none d-md-flex align-items-center">
						<ul class="second-menu">
							<li class="menu-item">
								<a href="#">Mua Bán BĐS</a>
							</li>
							<li class="menu-item">
								<a href="#">Cho Thuê BĐS</a>
							</li>
							<li class="menu-item">
								<a href="#">Dự Án</a>
							</li>
						</ul>
						<div class="app-header-nav-item-sub">
							<a class="app-header-nav-primary-button" href="#">
								<svg class="app-header-nav-item-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><g fill="none" fill-rule="evenodd"><path stroke="#FFBA00" stroke-width=".5" d="M1 1h28v28H1z" opacity=".01"></path><path class="svg-fill" fill="#fff" d="M16.765 2.312a.759.759 0 1 1 0 1.518H6.873a1.88 1.88 0 0 0-1.877 1.877v16.438a1.88 1.88 0 0 0 1.877 1.877H23.31a1.88 1.88 0 0 0 1.877-1.877V11.8a.76.76 0 0 1 1.518 0v10.344a3.399 3.399 0 0 1-3.395 3.395H6.873a3.4 3.4 0 0 1-3.396-3.395V5.707a3.4 3.4 0 0 1 3.396-3.395h9.892zm6.022.21c.273-.1.564-.078.835-.038.276.042.57.205.83.461l.54.54 1.117 1.117c.24.24.394.497.46.766a1.68 1.68 0 0 1-.4 1.545l-.058.062c-.344.352-.7.707-1.048 1.05l-.631.63-6.33 6.328-.488.493-.038.04c-.307.31-.621.628-.939.932-.153.148-.339.219-.619.236l-3.014.184h-.03a.719.719 0 0 1-.484-.218c-.158-.156-.249-.358-.24-.543l.135-3.097c.016-.253.095-.443.248-.598l.157-.16.003-.002.082-.081 5.416-5.415c.576-.577 1.166-1.167 1.747-1.745l1.68-1.682c.144-.146.27-.275.397-.396.188-.181.388-.304.672-.408zm.493 1.428l-.221.219c-.153.151-.306.305-.457.456l-.536.537-8.151 8.152-.086 1.957 1.906-.115.312-.312.226-.224.05-.049.385-.38 8.401-8.403-1.211-1.209a8.233 8.233 0 0 1-.172-.175l-.027-.029c-.065-.068-.13-.137-.2-.206l-.22-.219z"></path></g></svg>
								<span>Đăng Tin</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="bottom-head">
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<div class="col-md-9">
					<div class="menu-nowrap">
						<ul class="primary-menu">
							<li class="menu-item {{ request()->is('news') ? 'current-menu-item' : ''}}">
								<a href="{{ route('frontend.news.index') }}">Trang Chủ</a>
							</li>
							<li class="menu-item {{ request()->is('news/page*') ? 'current-menu-item' : ''}}">
								<a href="{{ route('frontend.news.page') }}">Thị trường</a>
							</li>
							<li class="menu-item">
								<a href="#">Cẩm nang</a>
							</li>
							<li class="menu-item">
								<a href="#">Phong thủy</a>
							</li>
							<li class="menu-item">
								<a href="#">Thủ tục</a>
							</li>
							<li class="menu-item">
								<a href="#">Tư vấn</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-auto" style="padding: 0 15px 0 0;">
					<form class="form-inline search-form d-none d-md-flex" action="">
						<input type="text" name="s" class="form-control" value="" placeholder="Tìm theo bài viết...">
						<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
					</form>
				</div>
			</div>
		</div>
	</div>
</header>