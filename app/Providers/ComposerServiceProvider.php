<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //settings
        View::composer(
            ['admin.*', 'frontend.*'],
            'App\Http\ViewComposers\SettingsComposer'
        );
        //menu
        View::composer(
            'frontend.common.*', 'App\Http\ViewComposers\MenuComposer'
        );
        //footer menu
        View::composer(
            'frontend.common.footer', 'App\Http\ViewComposers\FooterMenuComposer'
        );
        //right sidebar
        View::composer(
            'frontend.parts.right_sidebar', 'App\Http\ViewComposers\RightSidebarComposer'
        );
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
