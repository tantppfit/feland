@extends('frontend.layouts.app')
@section('content')
	<div class="login-wrapper">
		<div class="container">
			<div class="form-login-container">
				<div class="form-login-title">ĐĂNG NHẬP</div>
				<form>
					<input type="tel" class="form-control" name="" placeholder="Nhập số điện thoại" required="">
					<input type="password" placeholder="Nhập mật khẩu" class="form-control" required="">
					<div class="form-group">
						<input type="checkbox" id="remember-me" value="on" checked="">
						<label for="remember-me">Ghi nhớ tài khoản</label>
					</div>
					<button type="submit" class="btn btn-submit">ĐĂNG NHẬP</button>
					<button type="button" class="btn btn-forgot-password">Quên mật khẩu?</button>
					<div class="form-separator">
						<span>hoặc</span>
					</div>
					<div class="form-group group-2">
						<button type="button" class="btn btn-login-fb">Đăng nhập bằng <img src="{{ asset('image/icon/fb.svg') }}"></button>
						<button type="button" class="btn btn-success">Đăng ký</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection