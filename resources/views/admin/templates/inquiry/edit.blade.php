@extends('admin.layouts.app')

@section('content')
@include('admin.parts.left-menu')
<!---------- admin-edit main section ---------->
    <section class="main">
        <div class="list-of-content-title">
            <h3 class="font-weight-bold">お問い合わせの詳細な</h3>
            <h6 class="font-weight-bold">詳細なお問い合わせを確認して、状態が変更できます。</h6>
            @include('admin.parts.alert')
        </div>

        <form class="edit-inquiry" action="{{ route('admin.inquiry.update', $inquiry) }}" method="POST">
            {{ csrf_field()}}
            <div class="form-group d-flex flex-wrap" style="background: #f8f8f8; padding: 1rem;margin-top:65px;">
                <div class="label-admin-edit col-lg-2 col-md-3 col-12">
                    <label style="margin-right: 10px;">メールアドレス</label>
                </div>
                <div class="col-md-9 col-12">
                    <input type="email" name="email" class="form-control" style="margin-left:-10px;" placeholder="メールアドレス" value="{{ old('email', $inquiry->email) }}">
                </div>
            </div>

            <div class="form-group d-flex flex-wrap" style="margin-top: 2.5rem;">
                <div class="label-admin-edit col-lg-2 col-md-3 col-12">
                    <label>内容</label>
                </div>
                <div class="col-md-9 col-12">
                    <textarea class="form-control" rows="8" placeholder="内容" name="content">{{ old('content', $inquiry->content) }}</textarea>
                </div>
            </div>

            <div class="form-group d-flex flex-wrap" style="margin-top: 3rem; background: #f8f8f8; padding: 1rem 0;">
                <div class="label-admin-edit col-lg-2 col-md-3 col-12">
                    <label>状態</label>
                </div>

                <div class="col-6 col-md-3">
                    <select class="form-control" name="work_status">
                        <option value="incompatible" {{ ($inquiry->work_status == 'incompatible') ? 'selected' : '' }}>未対応</option>
                        <option value="acknowledged" {{ ($inquiry->work_status == 'acknowledged') ? 'selected' : '' }}>対応済み</option>
                        <option value="ignore" {{ ($inquiry->work_status == 'ignore') ? 'selected' : '' }}>無視する</option>
                    </select>
                </div>
            </div>
            <div class="d-flex">
                <a href="{{ route('admin.inquiry.index') }}" class="btn-back btn btn-no-radius mr-2" style="font-size: 12px; width: 100px; line-height: 28px;">
                    <i class="fas fa-arrow-left" aria-hidden="true"></i>
                    戻る
                </a>
                <button class="btn btn-no-radius" style="background: #111; color: #fff; width: 150px">状態を変更</button>
            </div>

        </form>

        

    </section>
<!---------- admin-edit main section ---------->
@endsection