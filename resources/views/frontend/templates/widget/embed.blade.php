<!DOCTYPE html>
<html>
<head>
	<title>Widget</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/frontend/widget/style.css') }}">
	<meta charset="UTF-8">
</head>
<body>
	<div class="container-fluid widget-content">
		<div class="row">
  		 	<div class="col-12 p-0 cover">
  		 		@if(isset($settings['widget_image']) && !empty($settings['widget_image']))
						<img src="{{ $settings['widget_image'] }}">
					@else
						<img src="{{ asset('image/widget_cover.jpg') }}">
					@endif
    		</div>
  		  {{-- <div class="line"></div> --}}
  		</div>
	  <div class="row row-cols-2 post-items">	  	
	    	@if(isset($posts) && count($posts))
	    		@foreach($posts as $post)
		    		<div class="col post-item">
				    	<a href="{{ route('frontend.post.show', $post->slug) }}" class="d-flex text-decoration-none">
				    		@if($post->featureImage)
									<img src="{{ Helper::getMediaUrl($post->featureImage, 'thumbnail') }}" />
								@else
									<img src="{{ Helper::getDefaultCover($post) }}">
								@endif
					  		<h1>{{ $post->title }}</h1>
					  	</a>
    				</div>
				  @endforeach
			  @endif
	  </div>
	</div>
</body>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>