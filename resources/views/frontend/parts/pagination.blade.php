@if ($paginator->hasPages())
    <div class="new-content-pagination no-radius" style="font-size: 14px;">
        <nav aria-label="pavigation">
            <ul class=" pagination pagination-full" role="navigation">
                @if (!$paginator->onFirstPage() && $paginator->lastPage() > 5)
                    <li class="page-item">
                        <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="« 前">‹</a>
                    </li>
                @endif
                @php
                    if($paginator->lastPage() <= 5 ){
                        $min = 1;
                        $max = $paginator->lastPage();
                    }elseif($paginator->lastPage() - $paginator->currentPage() >= 4){
                        $min = $paginator->currentPage();
                        $max = $paginator->currentPage() + 4;
                    }else{
                        $max = $paginator->lastPage();
                        $min = $paginator->lastPage() - 4;
                    }
                @endphp
                @foreach(range($min, $max) as $i)
                    @if ($i == $paginator->currentPage())
                        <li class="page-item active" aria-current="page">
                            <span class="page-link">{{ $i }}</span>
                        </li>
                    @else
                        <li class="page-item">
                            <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                        </li>
                    @endif
                @endforeach
                @if ($paginator->currentPage() + 4 < $paginator->lastPage())
                    <li class="page-item">
                        <a class="page-link" href="{{ $paginator->url($paginator->currentPage()+5) }}" rel="next" aria-label="次 »">›</a>
                    </li>
                @endif
            </ul>
        </nav>
    
        <nav aria-label="pavigation">
            <p class="page-responsive-number text-center" style="color: #fff;">{{ $paginator->currentPage() }}/{{ $paginator->lastPage() }}</p>
            <ul class="pagination pavigation-responsive">
                @foreach(range($min, $max) as $i)
                    @if ($i == $paginator->currentPage())
                        @if (!$paginator->onFirstPage() && $paginator->lastPage() > 2)
                        <li class="page-item">
                            <a class="page-link" href="{{ $paginator->url($paginator->currentPage() - 1) }}">{{$i - 1}}</a>
                        </li>
                        @endif
                    @endif
                @endforeach
                @php
                    if($paginator->lastPage() <= 2 ){
                        $min = 1;
                        $max = $paginator->lastPage();
                    }elseif($paginator->lastPage() - $paginator->currentPage() >=1 ){
                        $min = $paginator->currentPage();
                        $max = $paginator->currentPage() + 1;
                    }else{
                        $max = $paginator->lastPage();
                        $min = $paginator->lastPage();
                    }
                @endphp
                @foreach(range($min, $max) as $i)
                    @if ($i == $paginator->currentPage())
                        <li class="page-item active" aria-current="page">
                            <span class="page-link">{{ $i }}</span>
                        </li>
                    @else
                        <li class="page-item">
                            <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </nav>
    </div>    
@endif