<footer>
	<!-- DESTOP -->
	<div class="footer-wrapper-destop">
		<div class="footer-wrapper-destop-container">
			<section class="footer-wrapper-destop-container-top">
				<div class="footer-wrapper-destop-item">
					<p class="footer-wrapper-destop-item-heading">Tải ứng dụng chợ tốt</p>
					<div class="footer-wrapper-destop-item-row">
						<div class="footer-wrapper-destop-item-in-row">
							<img alt="Chợ Tốt" class="appWrapper-img-qr" src="https://static.chotot.com/storage/default/group-qr.jpeg">
						</div>
						<div class="footer-wrapper-destop-item-in-row">
							<ul class="footer-wrapper-destop-ul">
								<li class="footer-wrapper-destop-li">
									<a href="https://itunes.apple.com/us/app/chotot.vn/id790034666" target="_blank" rel="noopener noreferrer">
										<img alt="App Store" height="32" src="https://static.chotot.com/storage/default/ios.svg"></a>
									</li>
									<li class="footer-wrapper-destop-li">
										<a href="https://play.google.com/store/apps/details?id=com.chotot.vn" target="_blank" rel="noopener noreferrer"><img alt="Google Play" height="32" src="https://static.chotot.com/storage/default/android.svg"></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="footer-wrapper-destop-item">
						<p class="footer-wrapper-destop-item-heading">Hỗ trợ khách hàng</p>
						<ul class="footer-wrapper-destop-ul">
							<li class="footer-wrapper-destop-li">
								<a href="#" target="_blank">Trung tâm trợ giúp</a>
							</li>
							<li class="footer-wrapper-destop-li">
								<a href="#" target="_blank">An toàn mua bán</a>
							</li>
							<li class="footer-wrapper-destop-li">
								<a href="#" target="_blank">Quy định cần biết</a>
							</li>
							<li class="footer-wrapper-destop-li">
								<a href="#" target="_blank">Quy chế quyền riêng tư</a>
							</li>
							<li class="footer-wrapper-destop-li">
								<a href="#" target="_blank">Liên hệ hỗ trợ</a>
							</li>
						</ul>
					</div>
					<div class="footer-wrapper-destop-item">
						<p class="footer-wrapper-destop-item-heading">Về Chợ Tốt</p>
						<ul class="footer-wrapper-destop-ul">
							<li class="footer-wrapper-destop-li">
								<a href="#" target="_blank">Giới thiệu</a>
							</li>
							<li class="footer-wrapper-destop-li">
								<a href="#" target="_blank">Tuyển dụng</a>
							</li>
							<li class="footer-wrapper-destop-li">
								<a href="#" target="_blank">Truyền thông</a>
							</li>
							<li class="footer-wrapper-destop-li">
								<a href="#" target="_blank">Blog</a>
							</li>
						</ul>
					</div>
					<div class="footer-wrapper-destop-item-col">
						<div class="footer-wrapper-destop-item">
							<p class="footer-wrapper-destop-item-heading">Liên kết</p>
							<ul class="footer-wrapper-destop-ul footer-wrapper-destop-ul-row">
								<li class="footer-wrapper-destop-li">
									<a href="#" target="_blank"><img class="footer-wrapper-destop-icon " src="{{ asset('image/icon/facebook.svg') }}"></a>
								</li>
								<li class="footer-wrapper-destop-li">
									<a href="#" target="_blank"><img class="footer-wrapper-destop-icon " src="{{ asset('image/icon/youtube.svg') }}"></a>
								</li>
								<li class="footer-wrapper-destop-li">
									<a href="#" target="_blank"><img class="footer-wrapper-destop-icon " src="{{ asset('image/icon/google.svg') }}"></a>
								</li>
							</ul>
						</div>
						<div class="footer-wrapper-destop-item">
							<p class="footer-wrapper-destop-item-heading">Chứng nhận</p>
							<ul class="footer-wrapper-destop-ul footer-wrapper-destop-ul-row">
								<li class="footer-wrapper-destop-li">
									<a href="#" target="_blank"><img src="{{ asset('image/cerfiticate.png') }}"></a>
								</li>
							</ul>
						</div>
					</div>
				</section>
			</div>
			<hr class="footer-wrapper-destop-hr-bottom">
			<section class="footer-wrapper-destop-container-bottom">
				<address class="footer-wrapper-destop-address">CÔNG TY TNHH CHỢ TỐT - Địa chỉ: Phòng 1808, Tầng 18, Mê Linh Point Tower, 02 Ngô Đức Kế, Phường Bến Nghé, Quận 1, TP Hồ Chí Minh <br>Giấy chứng nhận đăng ký doanh nghiệp số 0312120782 do Sở Kế Hoạch và Đầu Tư TPHCM cấp ngày 11/01/2013 <br>Email: trogiup@chotot.vn - Đường dây nóng: (028)38664041
				</address>
			</section>
		</div>

		<!-- MOBILE -->
		<div class="footer-wrapper-mobile">
			<div class="footer-wrapper-mobile-section">
				<ul class="footer-wrapper-mobile-section-ul">
					<li class="footer-wrapper-mobile-section-li">
						<a href="#" target="_blank">TRỢ GIÚP</a>
					</li>
					<li class="footer-wrapper-mobile-section-li">&nbsp;-&nbsp;</li>
					<li class="footer-wrapper-mobile-section-li">
						<a href="#" target="_blank">QUY ĐỊNH CẦN BIẾT</a>
					</li>
					<li class="footer-wrapper-mobile-section-li">&nbsp;-&nbsp;</li>
					<li class="footer-wrapper-mobile-section-li">
						<a href="#" target="_blank">QUY CHẾ QUYỀN RIÊNG TƯ</a>
					</li>
				</ul>
			</div>
			<div class="footer-wrapper-mobile-section sub-ul">
				<ul class="footer-wrapper-mobile-section-ul">
					<li class="footer-wrapper-mobile-section-li">
						<a href="#" target="_blank">LIÊN HỆ</a>
					</li>
					<li class="footer-wrapper-mobile-section-li">&nbsp;-&nbsp;</li>
					<li class="footer-wrapper-mobile-section-li">
						<a href="#" target="_blank">VỀ CHỢ TỐT</a>
					</li>
				</ul>
			</div>
			<div class="footer-wrapper-mobile-section">
				<a href="#" target="_blank">
					<img src="{{ asset('image/cerfiticate.png') }}">
				</a>
			</div>
		</div>
	</footer>