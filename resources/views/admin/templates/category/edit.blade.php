@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main" >
		<div class="post-edit" style="padding: 25px;">
			<h3 class="font-weight-bolder">カテゴリー管理</h3>
      <h6 style="color: black; font-size: 14px;">記事作成時に入力候補として使用するカテゴリーを管理します。</h6>
      <h6 style="color: black; font-size: 14px;">あらかじめカテゴリーを登録しておくことにより、記事を作成する際にカテゴリーの入力候補として利用できます。</h6>

      @include('admin.parts.alert')

    </div>
    <div class="tag-edit">
			<form action="{{ route('admin.category.update', $category) }}" method="POST">
				<div class="form-group row" style="padding: 1rem 0 2rem 0;">
					<div class="col-lg-2 col-md-3 text-left text-md-right">
						<label>カテゴリー名</label>
					</div>
					<div class="col-12 col-lg-10 col-md-9">
						<input type="text" class="form-control" name="name" value="{{ old('name', $category->name) }}" required="" />
					</div>
				</div>
				<div class="form-group row" style="padding: 1rem 0 2rem 0;">
					<div class="col-lg-2 col-md-3 text-left text-md-right">
						<label>カテゴリの親</label>
					</div>
					<div class="col-12 col-lg-10 col-md-9">
						<select name="parent_id" class="form-control">
							<option value="">カテゴリの親を選択</option>
							@if(isset($parentCategories) && count(($parentCategories)))
								@foreach($parentCategories as $parentCategory)
									@if($parentCategory->id != $category->id)
										<option value="{{ $parentCategory->id }}" {{ (old('parent_id', $category->parent_id) == $parentCategory->id ) ? 'selected' : '' }}>{{ $parentCategory->name }}</option>
									@endif
								@endforeach
							@endif
						</select>
					</div>
				</div>
				<div class="form-group row" style="padding: 1rem 0 2rem 0;">
					<div class="col-lg-2 col-md-3 text-left text-md-right">
						<label>アイコン</label>
					</div>
					<div class="col-12 col-lg-10 col-md-9">
						<div style="opacity: 0.5;">
							<p>この画像はカテゴリー一覧ページにイコンとして表示されます。</p>
						</div>
						@if(old('featured_image', null) != null || $category->featured_image)
							<div class="mb-3" style="width: 240px;">
								<img src="{{ old('featured_image', $category->featured_image) }}" data-image-input-preview="featured_image">
							</div>
						@endif
						
						<input type="text" class="form-control" placeholder="" name="featured_image" value="{{ old('featured_image', $category->featured_image) }}" style="margin-bottom: .5rem;">

						<button type="button" class="btn btn-no-radius open-library" data-input-name="featured_image" data-input-type="url" data-toggle="modal" data-target="#library-modal" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;"></i>メディアライブラリから選択</button>
					</div>
				</div>
				<div class="form-group row" style="padding: 1rem 0 2rem 0;">
					<div class="col-lg-2 col-md-3 text-left text-md-right">
						<label>サイドバーに表示の画像</label>
					</div>
					<div class="col-12 col-lg-10 col-md-9">
						<div style="opacity: 0.5;">
							<p>この画像はサイドバーのリストに表示されます。</p>
						</div>
						@if(old('sub_featured_image', null) != null || $category->sub_featured_image)
							<div class="mb-3" style="width: 240px;">
								<img src="{{ old('sub_featured_image', $category->sub_featured_image) }}" data-image-input-preview="sub_featured_image">
							</div>
						@endif
						
						<input type="text" class="form-control" placeholder="" name="sub_featured_image" value="{{ old('sub_featured_image', $category->sub_featured_image) }}" style="margin-bottom: .5rem;">

						<button type="button" class="btn btn-no-radius open-library" data-input-name="sub_featured_image" data-input-type="url" data-toggle="modal" data-target="#library-modal" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;"></i>メディアライブラリから選択</button>
					</div>
				</div>
				<div style="margin-top: 70px;">
					<a href="{{ route('admin.category.index') }}" class="btn btn-no-radius" style="font-size: 12px; background: #111; color: #fff;"><i class="fas fa-arrow-left" style="color: #fff;"></i> 戻る</a>
					<button class="btn btn-no-radius" style="background: #111; color: #fff;">更新</button>
				</div>
				{{ csrf_field() }}
			</form>
		</div>
		
	</section>
	<!---------- admin-edit main section ---------->
	<!-- Library modal -->
	@include('admin.templates.media.parts.modal-library')
	<!--x-- Library modal -->
@endsection