<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->text('content')->nullable();
            $table->unsignedBigInteger('cover_image')->nullable();
            $table->string('cover_image_style')->nullable();
            $table->string('cover_via_text')->nullable();
            $table->string('cover_via_href')->nullable();
            $table->boolean('post_status')->default(false);
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('site_id');
            $table->foreign('site_id')->references('id')->on('sites')->onDelete('cascade');
            $table->string('html_title')->nullable();
            $table->string('html_meta_description')->nullable();
            $table->string('html_meta_keyword')->nullable();
            $table->string('canonical_url')->nullable();
            $table->string('ogp_title')->nullable();
            $table->string('ogp_description')->nullable();
            $table->string('ogp_image_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
