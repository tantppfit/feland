<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

/**
 * 
 */
class ProjectController extends Controller
{
	
	function __construct()
	{
		
	}
	
	public function index() 
	{
		return view('frontend.templates.project.index');
	}
}