<!-- MODAL FILTER PROJECT -->
<div class="modal fade" id="dynamic-filter-project" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="dynamic-filter-modal-dialog modal-dialog" role="document">
		<div class="dynamic-filter-modal-content modal-content">
			<div class="dynamic-filter-modal-header">
				<h4 class="text-center dymanic-filter-modal-title">Bạn muốn</h4>
				<button type="button" class="close float-left" data-dismiss="modal" aria-label="Close">
					<span class="close-btn" aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="dynamic-filter-modal-body">
				<div>
					<div class="suggestion-filter">
						<div class="suggestion-search-box">
							<span class="suggestion-icon"></span>
							<input class="suggestion-input" type="text" name="" value="" placeholder="Nhập từ khoá để lọc">
						</div>

						<div class="layout-group">
							<div class="layout-group-wrapper">
								<div class="layout-group-item">
									<div class="label-item">
										Tất cả
									</div>
									<div class="radio-button">
										<!-- <label for="" class="label-button label-button-checked"></label> -->
										<input type="radio" name="1" value="" checked="">
									</div>
								</div>
							</div>
						</div>
						<div class="layout-group">
							<div class="layout-group-wrapper">
								<div class="layout-group-item">
									<div class="label-item">
										152 Điện Biên Phủ
									</div>
									<div class="radio-button">
										<!-- <label for="" class="label-button label-button-checked"></label> -->
										<input type="radio" name="1" value="" checked="">
									</div>
								</div>
							</div>
						</div>
						<div class="layout-group">
							<div class="layout-group-wrapper">
								<div class="layout-group-item">
									<div class="label-item">
										2T Corporation
									</div>
									<div class="radio-button">
										<!-- <label for="" class="label-button label-button-checked"></label> -->
										<input type="radio" name="1" value="" checked="">
									</div>
								</div>
							</div>
						</div>
						<div class="layout-group">
							<div class="layout-group-wrapper">
								<div class="layout-group-item">
									<div class="label-item">
										319 Bồ Đề
									</div>
									<div class="radio-button">
										<!-- <label for="" class="label-button label-button-checked"></label> -->
										<input type="radio" name="1" value="" checked="">
									</div>
								</div>
							</div>
						</div>
						<div class="layout-group">
							<div class="layout-group-wrapper">
								<div class="layout-group-item">
									<div class="label-item">
										4S Riverside Garden Bình Triệu
									</div>
									<div class="radio-button">
										<!-- <label for="" class="label-button label-button-checked"></label> -->
										<input type="radio" name="1" value="" checked="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="margin-bottom: 30px;"></div>
			</div>
		</div>
	</div>
</div>