@extends('frontend.layouts.app')
@section('content')
	<main>
		<div class="all-container" style="padding: 0 15px; margin: 0 auto;">
			<div class="row">
				<div class="col-12" style="background-color: #fff;">
					<div class="row project-search-bar">
						<div class="col-12 p-0">
							<div class="search-bar-nav-wrapper">
								<div class="search-bar-nav">
									<a href="#" class="search-bar-nav-item">
										<div class="search-bar-nav-item-text">MUA BÁN</div>
									</a>
									<a href="#" class="search-bar-nav-item">
										<div class="search-bar-nav-item-text">CHO THUÊ</div>
									</a>
									<a href="#" class="search-bar-nav-item">
										<div class="search-bar-nav-item-text active">DỰ ÁN</div>
									</a>
									<a href="#" class="search-bar-nav-item">
										<div class="search-bar-nav-item-text">CHUYÊN TRANG BĐS</div>
									</a>
									<a href="#" class="search-bar-nav-item">
										<div class="search-bar-nav-item-text">KINH NGHIỆM MUA BÁN</div>
									</a>
								</div>
							</div>
							<div class="row m-0">
								<div class="col-12 search-bar-filter-wrapper">
									<div class="main-quick-filter" id="main-quick-filter">
										<div class="search-ref" id="search-ref">
											<div>
												<div style="width: 100%; background: rgb(255, 255, 255); height: 100%; border-radius: 4px; position: relative;">
													<div>
														<div class="position-relative">
															<button class="search-ref-button">
																<svg style="vertical-align: unset;" width="18px" height="18px" viewBox="0 0 30 30" version="1.1"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="List-view-msite-" transform="translate(-20.000000, -118.000000)" fill="#979797"><path d="M31.4322828,138.019826 C26.684097,138.019826 22.8341133,134.171603 22.8341133,129.426153 C22.8341133,124.679787 26.684097,120.83248 31.4322828,120.83248 C36.1804687,120.83248 40.0309105,124.680245 40.0309105,129.426153 C40.0304524,134.171603 36.1804687,138.019826 31.4322828,138.019826 M49.3677699,145.123005 L40.5532011,136.313515 C42.0031681,134.398256 42.8645657,132.012825 42.8645657,129.425695 C42.8645657,123.115269 37.7463477,118 31.4322828,118 C25.118218,118 20,123.115269 20,129.425695 C20,135.736122 25.118218,140.85139 31.4322828,140.85139 C34.0118177,140.85139 36.3913703,139.9969 38.3040684,138.556773 L47.120928,147.368094 C47.8663374,148.113074 48.9727686,148.213504 49.5931795,147.593832 C50.2141249,146.974236 50.1127211,145.867603 49.3677699,145.123005" id="Search"></path></g></g></svg>
															</button>
															<div class="search-ref-input-wrapper">
																<input type="text" class="search-ref-input" placeholder="Nhập tên dự án hoặc quận">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="region-ref" id="region-ref">
											<div class="region-ref-select">
												<div>
													<img class="region-location-icon" height="40" src="image/icon/region.svg">
													<span class="region-location-text">Toàn quốc</span>
													<img class="region-select-icon" height="40" src="image/icon/down-chevron.svg">
												</div>
											</div>
										</div>
										<div class="price-and-year-ref">
											<div class="price-ref">
												<div class="price-ref-select" height="40" width="100%">
													<div>
														<span class="price-ref-text" height="40">Chọn khoảng giá/m²</span>
														<img class="price-ref-icon" height="40" src="image/icon/down-chevron.svg">
													</div>
												</div>
											</div>
											<div class="year-ref">
												<div class="year-ref-select" height="40" width="100%">
													<div>
														<span class="year-ref-text" height="40">Năm bàn giao</span>
														<img class="year-ref-icon" height="40" src="image/icon/down-chevron.svg">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="project-main-wrapper">
						<div class="row m-0">
							<div class="col-lg-8 p-0">
								<div>
									<div class="base-project-breadcrumb-wrapper">
										<ol class="base-project-breadcrumb hidden-xs">
											<li class="breadcrumb-item"><a href="#">Chợ Tốt Nhà</a></li>
											<li class="breadcrumb-item"><a href="#">Toàn quốc</a></li>
											<li class="breadcrumb-item active"><a href="#">Tất cả loại bất động sản</a></li>
										</ol>
									</div>
									<div>
										<h1 class="project-title"> Dự án  tại Toàn quốc</h1>
									</div>
								</div>
								<div style="background-color: white;">
									<ul style="list-style: none; padding: 0">
										<!-- 10 ITEM -->
										<li class="project-item-wrapper">
											<a href="#" class="project-item">
												<div class="row m-0 w-100 h-100">
													<div class="col-12 col-sm-8 p-0 project-item-thumbnail-wrapper" data-toggle="modal" data-target="#modal-project">
														<div class="project-item-main-thumbnail">
															<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-1.jpg') }}">
														</div>
														<div class="project-item-sub-thumbnail">
															<div class="project-item-sub-thumbnail-1">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-1.jpg') }}">
															</div>
															<div class="project-item-sub-thumbnail-2">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-2.jpg') }}">
															</div>
														</div>
													</div>
													<div class="col-12 col-sm-4 project-item-info-wrapper">
														<div>
															<div class="project-item-info-title-wrapper">
																<h3 class="project-item-info-title">Vinhomes Grand Park (Vincity Quận 9)</h3>
															</div>
															<div class="project-item-info-price-wrapper">
																<div class="project-item-info-price">
																	<span>14.7 - 44.4 triệu/m²</span>
																</div>
															</div>
															<div class="project-item-info-condition-wrapper">
																<div>Bàn giao 2021 <span style="color: rgba(0, 0, 0, 0.5);">|</span> Quận 9, Tp Hồ Chí Minh</div>
															</div>
														</div>
														<div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin mua bán</button>
															</div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin cho thuê</button>
															</div>
														</div>
													</div>
												</div>
											</a>
										</li>
										<li class="project-item-wrapper">
											<a href="#" class="project-item">
												<div class="row m-0 w-100 h-100">
													<div class="col-12 col-sm-8 p-0 project-item-thumbnail-wrapper" data-toggle="modal" data-target="#modal-project">
														<div class="project-item-main-thumbnail">
															<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-2.jpg') }}">
														</div>
														<div class="project-item-sub-thumbnail">
															<div class="project-item-sub-thumbnail-1">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-3.jpg') }}">
															</div>
															<div class="project-item-sub-thumbnail-2">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-4.jpg') }}">
															</div>
														</div>
													</div>
													<div class="col-12 col-sm-4 project-item-info-wrapper">
														<div>
															<div class="project-item-info-title-wrapper">
																<h3 class="project-item-info-title">Vinhomes Central Park</h3>
															</div>
															<div class="project-item-info-price-wrapper">
																<div class="project-item-info-price">
																	<span>57.5 - 97.6 triệu/m²</span>
																</div>
															</div>
															<div class="project-item-info-condition-wrapper">
																<div>Bàn giao 2018 <span style="color: rgba(0, 0, 0, 0.5);">|</span> Quận Bình Thạnh, Tp Hồ Chí Minh</div>
															</div>
														</div>
														<div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin mua bán</button>
															</div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin cho thuê</button>
															</div>
														</div>
													</div>
												</div>
											</a>
										</li>
										<li class="project-item-wrapper">
											<a href="#" class="project-item">
												<div class="row m-0 w-100 h-100">
													<div class="col-12 col-sm-8 p-0 project-item-thumbnail-wrapper" data-toggle="modal" data-target="#modal-project">
														<div class="project-item-main-thumbnail">
															<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-1.jpg') }}">
														</div>
														<div class="project-item-sub-thumbnail">
															<div class="project-item-sub-thumbnail-1">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-1.jpg') }}">
															</div>
															<div class="project-item-sub-thumbnail-2">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-2.jpg') }}">
															</div>
														</div>
													</div>
													<div class="col-12 col-sm-4 project-item-info-wrapper">
														<div>
															<div class="project-item-info-title-wrapper">
																<h3 class="project-item-info-title">Vinhomes Grand Park (Vincity Quận 9)</h3>
															</div>
															<div class="project-item-info-price-wrapper">
																<div class="project-item-info-price">
																	<span>14.7 - 44.4 triệu/m²</span>
																</div>
															</div>
															<div class="project-item-info-condition-wrapper">
																<div>Bàn giao 2021 <span style="color: rgba(0, 0, 0, 0.5);">|</span> Quận 9, Tp Hồ Chí Minh</div>
															</div>
														</div>
														<div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin mua bán</button>
															</div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin cho thuê</button>
															</div>
														</div>
													</div>
												</div>
											</a>
										</li>
										<li class="project-item-wrapper">
											<a href="#" class="project-item">
												<div class="row m-0 w-100 h-100">
													<div class="col-12 col-sm-8 p-0 project-item-thumbnail-wrapper" data-toggle="modal" data-target="#modal-project">
														<div class="project-item-main-thumbnail">
															<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-2.jpg') }}">
														</div>
														<div class="project-item-sub-thumbnail">
															<div class="project-item-sub-thumbnail-1">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-3.jpg') }}">
															</div>
															<div class="project-item-sub-thumbnail-2">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-4.jpg') }}">
															</div>
														</div>
													</div>
													<div class="col-12 col-sm-4 project-item-info-wrapper">
														<div>
															<div class="project-item-info-title-wrapper">
																<h3 class="project-item-info-title">Vinhomes Central Park</h3>
															</div>
															<div class="project-item-info-price-wrapper">
																<div class="project-item-info-price">
																	<span>57.5 - 97.6 triệu/m²</span>
																</div>
															</div>
															<div class="project-item-info-condition-wrapper">
																<div>Bàn giao 2018 <span style="color: rgba(0, 0, 0, 0.5);">|</span> Quận Bình Thạnh, Tp Hồ Chí Minh</div>
															</div>
														</div>
														<div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin mua bán</button>
															</div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin cho thuê</button>
															</div>
														</div>
													</div>
												</div>
											</a>
										</li>
										<li class="project-item-wrapper">
											<a href="#" class="project-item">
												<div class="row m-0 w-100 h-100">
													<div class="col-12 col-sm-8 p-0 project-item-thumbnail-wrapper" data-toggle="modal" data-target="#modal-project">
														<div class="project-item-main-thumbnail">
															<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-1.jpg') }}">
														</div>
														<div class="project-item-sub-thumbnail">
															<div class="project-item-sub-thumbnail-1">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-1.jpg') }}">
															</div>
															<div class="project-item-sub-thumbnail-2">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-2.jpg') }}">
															</div>
														</div>
													</div>
													<div class="col-12 col-sm-4 project-item-info-wrapper">
														<div>
															<div class="project-item-info-title-wrapper">
																<h3 class="project-item-info-title">Vinhomes Grand Park (Vincity Quận 9)</h3>
															</div>
															<div class="project-item-info-price-wrapper">
																<div class="project-item-info-price">
																	<span>14.7 - 44.4 triệu/m²</span>
																</div>
															</div>
															<div class="project-item-info-condition-wrapper">
																<div>Bàn giao 2021 <span style="color: rgba(0, 0, 0, 0.5);">|</span> Quận 9, Tp Hồ Chí Minh</div>
															</div>
														</div>
														<div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin mua bán</button>
															</div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin cho thuê</button>
															</div>
														</div>
													</div>
												</div>
											</a>
										</li>
										<li class="project-item-wrapper">
											<a href="#" class="project-item">
												<div class="row m-0 w-100 h-100">
													<div class="col-12 col-sm-8 p-0 project-item-thumbnail-wrapper" data-toggle="modal" data-target="#modal-project">
														<div class="project-item-main-thumbnail">
															<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-2.jpg') }}">
														</div>
														<div class="project-item-sub-thumbnail">
															<div class="project-item-sub-thumbnail-1">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-3.jpg') }}">
															</div>
															<div class="project-item-sub-thumbnail-2">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-4.jpg') }}">
															</div>
														</div>
													</div>
													<div class="col-12 col-sm-4 project-item-info-wrapper">
														<div>
															<div class="project-item-info-title-wrapper">
																<h3 class="project-item-info-title">Vinhomes Central Park</h3>
															</div>
															<div class="project-item-info-price-wrapper">
																<div class="project-item-info-price">
																	<span>57.5 - 97.6 triệu/m²</span>
																</div>
															</div>
															<div class="project-item-info-condition-wrapper">
																<div>Bàn giao 2018 <span style="color: rgba(0, 0, 0, 0.5);">|</span> Quận Bình Thạnh, Tp Hồ Chí Minh</div>
															</div>
														</div>
														<div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin mua bán</button>
															</div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin cho thuê</button>
															</div>
														</div>
													</div>
												</div>
											</a>
										</li>
										<li class="project-item-wrapper">
											<a href="#" class="project-item">
												<div class="row m-0 w-100 h-100">
													<div class="col-12 col-sm-8 p-0 project-item-thumbnail-wrapper" data-toggle="modal" data-target="#modal-project">
														<div class="project-item-main-thumbnail">
															<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-1.jpg') }}">
														</div>
														<div class="project-item-sub-thumbnail">
															<div class="project-item-sub-thumbnail-1">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-1.jpg') }}">
															</div>
															<div class="project-item-sub-thumbnail-2">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-2.jpg') }}">
															</div>
														</div>
													</div>
													<div class="col-12 col-sm-4 project-item-info-wrapper">
														<div>
															<div class="project-item-info-title-wrapper">
																<h3 class="project-item-info-title">Vinhomes Grand Park (Vincity Quận 9)</h3>
															</div>
															<div class="project-item-info-price-wrapper">
																<div class="project-item-info-price">
																	<span>14.7 - 44.4 triệu/m²</span>
																</div>
															</div>
															<div class="project-item-info-condition-wrapper">
																<div>Bàn giao 2021 <span style="color: rgba(0, 0, 0, 0.5);">|</span> Quận 9, Tp Hồ Chí Minh</div>
															</div>
														</div>
														<div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin mua bán</button>
															</div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin cho thuê</button>
															</div>
														</div>
													</div>
												</div>
											</a>
										</li>
										<li class="project-item-wrapper">
											<a href="#" class="project-item">
												<div class="row m-0 w-100 h-100">
													<div class="col-12 col-sm-8 p-0 project-item-thumbnail-wrapper" data-toggle="modal" data-target="#modal-project">
														<div class="project-item-main-thumbnail">
															<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-2.jpg') }}">
														</div>
														<div class="project-item-sub-thumbnail">
															<div class="project-item-sub-thumbnail-1">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-3.jpg') }}">
															</div>
															<div class="project-item-sub-thumbnail-2">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-4.jpg') }}">
															</div>
														</div>
													</div>
													<div class="col-12 col-sm-4 project-item-info-wrapper">
														<div>
															<div class="project-item-info-title-wrapper">
																<h3 class="project-item-info-title">Vinhomes Central Park</h3>
															</div>
															<div class="project-item-info-price-wrapper">
																<div class="project-item-info-price">
																	<span>57.5 - 97.6 triệu/m²</span>
																</div>
															</div>
															<div class="project-item-info-condition-wrapper">
																<div>Bàn giao 2018 <span style="color: rgba(0, 0, 0, 0.5);">|</span> Quận Bình Thạnh, Tp Hồ Chí Minh</div>
															</div>
														</div>
														<div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin mua bán</button>
															</div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin cho thuê</button>
															</div>
														</div>
													</div>
												</div>
											</a>
										</li>
										<li class="project-item-wrapper">
											<a href="#" class="project-item">
												<div class="row m-0 w-100 h-100">
													<div class="col-12 col-sm-8 p-0 project-item-thumbnail-wrapper" data-toggle="modal" data-target="#modal-project">
														<div class="project-item-main-thumbnail">
															<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-1.jpg') }}">
														</div>
														<div class="project-item-sub-thumbnail">
															<div class="project-item-sub-thumbnail-1">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-1.jpg') }}">
															</div>
															<div class="project-item-sub-thumbnail-2">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-2.jpg') }}">
															</div>
														</div>
													</div>
													<div class="col-12 col-sm-4 project-item-info-wrapper">
														<div>
															<div class="project-item-info-title-wrapper">
																<h3 class="project-item-info-title">Vinhomes Grand Park (Vincity Quận 9)</h3>
															</div>
															<div class="project-item-info-price-wrapper">
																<div class="project-item-info-price">
																	<span>14.7 - 44.4 triệu/m²</span>
																</div>
															</div>
															<div class="project-item-info-condition-wrapper">
																<div>Bàn giao 2021 <span style="color: rgba(0, 0, 0, 0.5);">|</span> Quận 9, Tp Hồ Chí Minh</div>
															</div>
														</div>
														<div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin mua bán</button>
															</div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin cho thuê</button>
															</div>
														</div>
													</div>
												</div>
											</a>
										</li>
										<li class="project-item-wrapper">
											<a href="#" class="project-item">
												<div class="row m-0 w-100 h-100">
													<div class="col-12 col-sm-8 p-0 project-item-thumbnail-wrapper" data-toggle="modal" data-target="#modal-project">
														<div class="project-item-main-thumbnail">
															<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-2.jpg') }}">
														</div>
														<div class="project-item-sub-thumbnail">
															<div class="project-item-sub-thumbnail-1">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-3.jpg') }}">
															</div>
															<div class="project-item-sub-thumbnail-2">
																<img class="project-item-thumbnail-image" src="{{ asset('image/thumbnail-project-4.jpg') }}">
															</div>
														</div>
													</div>
													<div class="col-12 col-sm-4 project-item-info-wrapper">
														<div>
															<div class="project-item-info-title-wrapper">
																<h3 class="project-item-info-title">Vinhomes Central Park</h3>
															</div>
															<div class="project-item-info-price-wrapper">
																<div class="project-item-info-price">
																	<span>57.5 - 97.6 triệu/m²</span>
																</div>
															</div>
															<div class="project-item-info-condition-wrapper">
																<div>Bàn giao 2018 <span style="color: rgba(0, 0, 0, 0.5);">|</span> Quận Bình Thạnh, Tp Hồ Chí Minh</div>
															</div>
														</div>
														<div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin mua bán</button>
															</div>
															<div style="color: inherit; margin-top: 10px;">
																<button class="project-item-info-button">Xem tin cho thuê</button>
															</div>
														</div>
													</div>
												</div>
											</a>
										</li>
									</ul>
								</div>
								<div>
									<div class="text-center">
										<ul class="project-pagination">
											<li class="project-pagination-item d-inline">
												<a class="first" href="#">«</a>
											</li>
											<li class="project-pagination-item d-inline">
												<a class="prev" href="#">‹</a>
											</li>
											<li class="project-pagination-item d-inline">
												<a href="#">1</a>
											</li>
											<li class="project-pagination-item d-inline">
												<a class="active" href="#">2</a>
											</li>
											<li class="project-pagination-item d-inline">
												<a href="#">3</a>
											</li>
											<li class="project-pagination-item d-inline">
												<a href="#">4</a>
											</li>
											<li class="project-pagination-item d-inline">
												<a href="#">5</a>
											</li>
											<li class="project-pagination-item d-inline">
												<a href="#">6</a>
											</li>
											<li class="project-pagination-item d-inline">
												<a href="#">7</a>
											</li>
											<li class="project-pagination-item d-inline">
												<a href="#">8</a>
											</li>
											<li class="project-pagination-item d-inline">
												<a href="#">9</a>
											</li>
											<li class="project-pagination-item d-inline">
												<a href="#">›</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-4"></div>
						</div>
					</div>
				</div>
				<div class="cat-description col-12">
					<div class="content-cat-wrapper">
						<div class="block4 row">
							<div class="col-12">
								<h2 style="text-align: center;"><strong>DỰ ÁN CĂN HỘ CHUNG CƯ, KHU PHỨC HỢP, SHOPHOUSE THÁNG 12/2020</strong></h2>

								<p>Chọn <strong>mua dự án</strong> không phải là chuyện dễ dàng, ngay cả khi bạn là người mua lần đầu hay là nhà đầu tư bất động sản chuyên nghiệp. Bên cạnh việc xác định vị trí, giá bán còn có nhiều yếu tố quan trọng khác bạn cần quan tâm trước khi chọn <strong>mua dự án.</strong></p>

								<h3><strong>Giá các dự án</strong></h3>

								<p><strong>Giá dự án</strong> sẽ chịu ảnh hưởng bởi các yếu tố:</p>

								<ul style="list-style-type: circle;">
									<li>Vị trí dự án: Vị trí sẽ là yếu tố quan trọng hàng đầu để định <strong>giá dự án</strong>. Bao gồm luôn cả hướng căn hộ chung cư, cảnh quan và tầm nhìn xung quanh. Chắc chắn với 1 căn hộ có vị trí tốt sẽ có tính thanh khoản cao trong tương lai.</li>
									<li>Tính pháp lý: để đảm bảo an toàn thì nên chọn các dự án đã hoàn thành được 20% phần thô và chủ đầu tư dự án đã và đang vận hành các dự án chung cư hoàn thiện khác. Tốt nhất vẫn là chọn các căn hộ đã hoàn thiện và cấp sổ hồng rõ ràng.</li>
									<li>Uy tín chủ đầu tư: Thông thường, những căn hộ chung cư có chủ đầu tư lớn, uy tín trong ngành với những dự án thành công thì sẽ có giá cao hơn so với các căn hộ đến từ các chủ đầu tư nhỏ và mới.</li>
									<li>Diện tích căn hộ: Dĩ nhiên rằng <strong>giá dự án</strong> có diện tích lớn hơn sẽ cao hơn. Bên cạnh đó, vị trí tầng của căn hộ cũng sẽ ảnh hưởng ít nhiều đến giá bán. Ví dụ tầng trệt (có thể kết hợp kinh doanh) và các tầng ở giữa toà nhà (có view đẹp) sẽ bán với giá cao hơn so với các tầng trên cùng của <strong>dự án</strong>.</li>
									<li>Chất lượng thi công: Công trình với chất lượng thi công tốt và đúng hạn sẽ có giá bán cao hơn.</li>
								</ul>

								<h3><strong>Những lưu ý khi mua dự án bất động sản</strong></h3>

								<ul>
									<li>1. Kiểm tra tính pháp lý của <strong>dự án Toàn quốc</strong>, bao gồm: giấy tờ về quyền sử dụng đất, hồ sơ dự án, thiết kế bản vẽ thi công có cơ quan thẩm quyền phê duyệt, giấy phép xây dựng, giấy tờ nghiệm thu hoàn thành xây dựng cơ sở hạ tầng kỹ thuật, ngân hàng thương mại bảo lãnh cho tiến độ dự án (nếu có),...</li>
									<li>2. Chọn chủ đầu tư uy tín dựa theo các tiêu chí sau: chủ đầu tư có thông tin rõ ràng, lịch sử "sạch"; Chất lượng các dự án trước đó; Tiến độ thực hiện dự án và các cam kết; Quá trình hợp tác với các đơn vị phân phối; Năng lực tài chính,...</li>
									<li>3. Chính sách vay mua nhà từ ngân hàng: Thông thường các dự án được hình thành đều có ngân hàng liên kết, đưa ra chính sách cho vay với chiết khấu cực kỳ ưu đãi. Nhưng người mua cũng cần xác định khoản vay cho hợp lý, hiểu rõ cách tính lãi suất và các cam kết, ràng buộc,...</li>
									<li>4. Quan tâm bản vẽ chi tiết dự án, đừng bị lóa mắt bởi hình ảnh phối cảnh: Cần xem xét bản đồ quy hoạch 1/500 một cách kỹ lưỡng và cần xác nhận của cơ quan có thẩm quyền về hạng mục trên phối cảnh 3D.</li>
									<li>5. Xem xét tiến độ thực hiện dự án.</li>
									<li>6. Xem kỹ các điều khoản trong hợp đồng <strong>mua bán căn hộ.</strong></li>
									<li>7. Kiểm tra kỹ lưỡng khi nhận bàn giao bất động sản (tường, trần, sàn nhà, hệ thống nội thất đi kèm nếu có, nhà vệ sinh, hệ thống điện nước, hệ thống an ninh...).</li>
									<li>8. Chú ý thời gian nhận sổ hồng của dự án.</li>
								</ul>

								<h3><strong>Mua bán dự án bất động sản</strong></h3>

								<p>Để tiến hành <strong>mua bán bất động sản </strong>được thuận lợi, người mua nên chú ý các vấn đề sau:</p>

								<ul style="list-style-type: circle;">
									<li>Cẩn thận với các khuyến mãi và chiết khấu của bên bán.</li>
									<li>Nếu vay tiền ngân hàng, nên chọn ngân hàng uy tín hoặc ngân hàng liên kết với dự án. Không nên vay quá 50% giá trị dự án để tránh áp lực nợ nần.</li>
									<li>Cần đọc kỹ hợp đồng: Các điều khoản và giao kết cũng như phí dịch vụ quản lý căn cứ vào diện tích căn hộ. Cần phải tiến hành đăng ký hợp đồng <strong>mua bán bất động sản </strong>với bộ công thương để tránh trình trạng các chủ dự án áp đặt mong muốn đơn phương lên hợp đồng.</li>
									<li>Truy cập vào <strong>Chợ Tốt Nhà</strong> nếu muốn mua bán bất động sản dự án để nắm bắt được tình trạng thực tế của căn hộ và giá bán trước khi quyết định chọn mua. <strong>Chợ Tốt Nhà</strong> sẽ mang đến cái nhìn tổng quan với các <strong>thông tin về dự án </strong>rõ ràng và chi tiết.</li>
								</ul>

								<p style="text-align: right;"><em><strong>--- Chợ Tốt Nhà ---</strong></em></p>
							</div>
						</div>
					</div>
					<p class="text-center">
						<a class="btn-see-more">Mở rộng</a>
					</p>
				</div>
			</div>
		</div>
	</main>

	@include('frontend.common.modals.project.modal-project')
	
@endsection