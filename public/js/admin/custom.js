jQuery('document').ready(function ($) {
	var _token = $('meta[name=csrf-token]').attr('content');
	$(".check-all").click(function () {
		$(".check").prop('checked', $(this).prop('checked'));
	});
	//add and remove tag on edit post page.
	$('.not-post-tags').delegate('.add-tag', 'click', function (e) {
		if ($(this).hasClass('selected') || ($('.post-tags .tag-items').length >= 10))
			return false;

		var tagId = $(this).data('tag-id');
		var tagName = $(this).data('tag-name');
		var html = '<div class="tag-items remove-tag" data-tag-id="' + tagId + '">' +
			'<input type="hidden" name="tags[]" value="' + tagId + '">' +
			'<div><i class="fas fa-minus"></i></div>' +
			'<label>' + tagName + '</label>' +
			'</div>';
		$('.tags.post-tags .tag-menu').append(html);
		$(this).addClass('selected');
	});
	$('.post-tags').delegate('.remove-tag', 'click', function (e) {
		var tagId = $(this).data('tag-id');
		$('.tags.not-post-tags .tag-menu').find('.tag-items[data-tag-id=' + tagId + ']').removeClass('selected');
		$(this).remove();
	});
	$('.post-tags .tag-menu').sortable();

	$('#form-feature-image').on('change', function (e) {
		e.preventDefault();
		$(this).trigger('submit');
	});
	$('#form-feature-image').submit(function (e) {
		e.preventDefault();
		var action = $(this).attr('action');
		$.ajax({
			url: action,
			method: "POST",
			data: new FormData(this),
			dataType: 'JSON',
			contentType: false,
			cache: false,
			processData: false,
			success: function (data) {
				if (data.uploaded) {
					var html = '<div class="cover-img-radio" style="display: grid; margin-left: 1rem;">' +
						'<input id="cover-img-' + data.id + '" type="radio" name="cover_image" value="' + data.id + '" checked>' +
						'<label for="cover-img-' + data.id + '">' +
						'<a href="#" class="delete-cover" data-action="' + data.action_delete + '"><i class="fa fa-times"></i></a>' +
						'<img src="' + data.url + '" style="width: 200px; height: 150px;">' +
						'</label>' +
						'</div>';
					$('.cover-img-radio-choice').append(html);
					reloadIframePreview();
				}
			},
			error: function (data) {
				alert('Error');
			}
		});
	});

	//change cover image
	$('.cover-img-radio-choice').delegate('input[type=radio][name=cover_image]', 'change', function (e) {
		setFeatureImage();
	});
	function setFeatureImage() {
		var media_id = $('input[type=radio][name=cover_image]:checked').val();
		$.ajax({
			url: _post_update_feature_image_action,
			method: "POST",
			data: {
				media_id: media_id,
				_token: _token
			},
			dataType: 'JSON',
			success: function (data) {
				if (data.status) {
					reloadIframePreview();
				}
			},
			error: function (data) {
				console.log('Error');
			}
		});
	}
	//change cover style
	$('.cover-image-style').change(function (e) {
		setFeatureStyle($(this));
	});
	function setFeatureStyle(el) {
		var cover_image_style = $(el).val();
		$.ajax({
			url: _post_update_feature_style_action,
			method: "POST",
			data: {
				cover_image_style: cover_image_style,
				_token: _token
			},
			dataType: 'JSON',
			success: function (data) {
				if (data.status) {
					reloadIframePreview();
				}
			},
			error: function (data) {
				console.log('Error');
			}
		});
	}

	function reloadIframePreview() {
		$("#iframe-preview-desktop").attr("src", function (index, attr) {
			return attr;
		});
		$("#iframe-preview-mobile").attr("src", function (index, attr) {
			return attr;
		});
	}

	//delete cover image
	$('.cover-img-radio-choice').delegate('.delete-cover', 'click', function (e) {
		e.preventDefault();
		var _this = $(this);
		var action = $(_this).data('action');
		$.ajax({
			url: action,
			method: "GET",
			dataType: 'JSON',
			success: function (data) {
				if (data.status) {
					$(_this).parents('.cover-img-radio').remove();
					reloadIframePreview();
				} else {
					alert('Error');
				}
			},
			error: function (data) {
				alert('Error');
			}
		});
	});

	//slide image feature
	$('.form-slide-images').on('change', function (e) {
		e.preventDefault();
		$(this).trigger('submit');
	});
	$('.form-slide-images').submit(function (e) {
		e.preventDefault();
		var action = $(this).attr('action');
		$.ajax({
			url: action,
			method: "POST",
			data: new FormData(this),
			dataType: 'JSON',
			contentType: false,
			cache: false,
			processData: false,
			success: function (data) {
				if (data.status) {
					if (data.is_new_slider) {
						$('.block-sliders .sliders').append(data.slider);
					} else {
						var html = '';
						$.each(data.images, function (key, image) {
							html += '<div class="slider-image">' +
								'<a href="#" class="remove" data-media-id="' + key + '"><i class="fa fa-times"></i></a>' +
								'<div style="background-image: url(' + image + '); "></div>' +
								'</div>';
						});
						$('.slider-' + data.slider_id).find('.slider-images').append(html);
					}

				}
				$('.form-slide-images .input-files').val('');
				$('.form-slide-images .slider-id').val('');
			},
			error: function (data) {
				alert('Error');
			}
		});
	});

	// add image to upload click
	$('.block-sliders').delegate('.add-slide-files', 'click', function (e) {
		var sliderId = $(this).data('slider-id');
		console.log(sliderId);
		$('.form-slide-images .slider-id').val(sliderId);
	});

	// delete slider
	$('.block-sliders').delegate('.delete-slider', 'click', function (e) {
		e.preventDefault();
		var sliderItem = $(this).parents('.slider-item');
		var action = $(sliderItem).data('action-delete-slider');
		$.ajax({
			url: action,
			method: "GET",
			dataType: 'JSON',
			success: function (response) {
				if (response.status) {
					$(sliderItem).remove();
				}
			},
			error: function (response) {
				alert('Error');
			}
		});
	});

	$('.block-sliders').delegate('.slider-image a.remove', 'click', function (e) {
		e.preventDefault();
		var _this = $(this);
		var sliderItem = $(this).parents('.slider-item');
		var action = $(sliderItem).data('action-delete-image');
		var mediaId = $(this).data('media-id');
		$.ajax({
			url: action,
			method: "POST",
			data: {
				media_id: mediaId,
				_token: _token
			},
			dataType: 'JSON',
			success: function (data) {
				if (data.status) {
					$(_this).parents('.slider-image').remove();
				}
			},
			error: function (data) {
				alert('Error');
			}
		});
	});

	//add previewer
	$('#wrap-preview #submit-email-preview').click(function (e) {
		e.preventDefault();
		$('#wrap-preview .message').empty();
		var action = $(this).data('action');
		var email = $('#wrap-preview #invite-email').val();
		$.ajax({
			url: action,
			method: "POST",
			data: {
				email: email,
				_token: _token
			},
			dataType: 'JSON',
			success: function (data) {
				if (data.status) {
					$('#wrap-preview .message').text(data.message);
				} else {
					alert('Error');
				}
			},
			error: function (data) {
				alert('Error');
			}
		});
	});

	//add tags
	$('.btn-add-tags').click(function (e) {
		e.preventDefault();
		var inputTags = $('.input-tags');
		var action = $(this).data('action');
		var _token = $('meta[name=csrf-token]').attr('content');
		$(inputTags).css('border-color', '#111111');
		var tagsName = $(inputTags).val();
		if (tagsName == '') {
			$(inputTags).css('border-color', '#c50000');
		} else {
			$.ajax({
				url: action,
				method: "POST",
				data: {
					name: tagsName,
					_token: _token
				},
				dataType: 'JSON',
				success: function (data) {
					if (data.tags) {
						var postTags = notPostTags = '';
						jQuery.each(data.tags, function (i, tag) {
							postTags += '<div class="tag-items remove-tag" data-tag-id="' + tag.id + '">' +
								'<input type="hidden" name="tags[]" value="' + tag.id + '">' +
								'<div><i class="fas fa-minus"></i></div>' +
								'<label>' + tag.name + '</label>' +
								'</div>';
							notPostTags += '<div class="tag-items add-tag selected" data-tag-id="' + tag.id + '" data-tag-name="' + tag.name + '">' +
								'<div><i class="fas fa-plus"></i></div>' +
								'<label>' + tag.name + '</label>' +
								'</div>';
						});
						$('.tags.post-tags .tag-menu').append(postTags);
						$('.tags.not-post-tags .tag-menu').append(notPostTags);
						$(inputTags).val('');
					}
				},
				error: function (data) {
					alert('Error');
				}
			});
		}
	});

	$('.post-links').click(function (e) {
		e.preventDefault();
		var url = $(this).attr('href');
		window.open(url);
	});

	//List Media
	$("#post-media-modal").on('show.bs.modal', function () {
		var _thisModal = $(this);
		var action = $(this).data('action');
		$.ajax({
			url: action,
			method: "GET",
			dataType: 'JSON',
			success: function (response) {
				if (response.status) {
					$(_thisModal).find('#library-main').html(response.html);
				}
			},
			error: function (response) {
				alert('Error');
			}
		});
	});
	$("#post-media-modal").on('hide.bs.modal', function () {
		$(this).find('#library-main').html('<div class="modal-body pt-4"><p>読み込み中...</p></div>');
	});
	$('#post-media-modal').delegate('.media-item', 'click', function (e) {
		e.preventDefault();
		var input = $('#post-media-modal').find('.input-media-images');
		var mediaId = $(this).data('id');
		var mediaUrl = $(this).data('url');

		var mediaImages = JSON.parse($(input).val());
		if($(this).hasClass('active')){
			mediaImages = mediaImages.filter(function( obj ) {
			    return obj.id !== mediaId;
			});
			$(this).removeClass('active');
		}else{
			mediaImages.push({id: mediaId, url: mediaUrl});
			$(this).addClass('active');
		}
		$(input).val(JSON.stringify(mediaImages));
	});
	$('#post-media-modal').delegate('.btn-update-media-images', 'click', function(e){
		e.preventDefault();
		var input = $('#post-media-modal').find('.input-media-images');
		var mediaImages = $(input).val();
		$.ajax({
			url: _post_update_media_images_action,
			method: "POST",
			data: {
				media_images: mediaImages,
				_token: _token
			},
			dataType: 'JSON',
			success: function (data) {
				if (data.status) {
					var mediaItemHtml = '';
					for(const [key, value] of Object.entries(JSON.parse(mediaImages))){
						mediaItemHtml += '<div class="media-image-item" data-media-id="'+ value.id +'" data-media-url="'+ value.url +'">'+
							'<a href="#" class="remove"><i class="fa fa-times"></i></a>'+
							'<div style="background-image: url('+ value.url +'); "></div>'+
						'</div>';
					}
					$('.block-medias .media-images').html(mediaItemHtml);
					$('#post-media-modal').modal('hide');
				}
			},
			error: function (data) {
				console.log('Error');
			}
		});
	});
	$('.block-medias .media-images').sortable({
		items: '.media-image-item',
		update: function( event, ui ) {
			handleUpdatePostMedia();
		}
	});
	$('.block-medias .media-images').delegate('.remove', 'click', function(e){
		e.preventDefault();
		$(this).parents('.media-image-item').remove();
		handleUpdatePostMedia();
	});
	function handleUpdatePostMedia() {
		var mediaImages = [];
		$('.block-medias .media-images .media-image-item').each(function(e){
			var mediaId = $(this).data('media-id');
			var mediaUrl = $(this).data('media-url');
			mediaImages.push({id: mediaId, url: mediaUrl});
		});
		$.ajax({
			url: _post_update_media_images_action,
			method: "POST",
			data: {
				media_images: JSON.stringify(mediaImages),
				_token: _token
			},
			dataType: 'JSON',
			success: function (data) {
				if (data.status) {
					console.log('success');
				}
			},
			error: function (data) {
				console.log('error');
			}
		});
	}

	//Statistic page
	$('.post-view-statistic').click(function (e) {
		e.preventDefault();
		$('#chartdiv').empty();
		$('#chartTitle').empty();
		$(this).addClass('table-active').siblings().removeClass('table-active');
		$('html, body').animate({
			scrollTop: $('#chartdiv').offset().top
		}, 500);
		var action = $(this).data('action');
		$.ajax({
			url: action,
			method: "GET",
			dataType: 'JSON',
			success: function (response) {
				if (response.status) {
					var postStatistic = response.data;
					$('#chartTitle').text(postStatistic.permalink + ' - ' + postStatistic.title);
					am4core.ready(function () {

						// Themes begin
						am4core.useTheme(am4themes_animated);
						// Themes end

						var chart = am4core.create("chartdiv", am4charts.XYChart);
						chart.language.locale = am4lang_ja_JP;

						var data = [];
						var value = 0;

						if (typeof (postStatistic.view_log) != "undefined" && postStatistic.view_log !== null) {
							data = postStatistic.view_log;
						}
						chart.data = data;

						// Create axes
						var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
						dateAxis.renderer.minGridDistance = 150;

						var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
						valueAxis.min = 0;
						valueAxis.renderer.labels.template.adapter.add("text", function (text, target) {
							return text.match(/\./) ? "" : text;
						});
						// Create series
						var series = chart.series.push(new am4charts.LineSeries());
						series.dataFields.valueY = "views";
						series.dataFields.dateX = "date";
						series.tooltipText = "{views} views"

						series.tooltip.pointerOrientation = "vertical";

						chart.cursor = new am4charts.XYCursor();
						chart.cursor.snapToSeries = series;
						chart.cursor.xAxis = dateAxis;

						chart.scrollbarX = new am4charts.XYChartScrollbar();
						chart.scrollbarX.series.push(series);

					}); // end am4core.ready()
				}
			},
			error: function (response) {
				alert('Error');
			}
		});
	});
	$('.user-view-statistic').click(function (e) {
		e.preventDefault();
		$('#chartdiv').empty();
		$('#chartTitle').empty();
		$(this).addClass('table-active').siblings().removeClass('table-active');
		$('html, body').animate({
			scrollTop: $('#chartdiv').offset().top
		}, 500);
		var action = $(this).data('action');
		$.ajax({
			url: action,
			method: "GET",
			dataType: 'JSON',
			success: function (response) {
				if (response.status) {
					var userStatistic = response.data;
					$('#chartTitle').text(userStatistic.name);
					am4core.ready(function () {

						// Themes begin
						am4core.useTheme(am4themes_animated);
						// Themes end

						var chart = am4core.create("chartdiv", am4charts.XYChart);
						chart.language.locale = am4lang_ja_JP;

						var data = [];
						var value = 0;

						if (typeof (userStatistic.view_log) != "undefined" && userStatistic.view_log !== null) {
							data = userStatistic.view_log;
						}
						chart.data = data;

						// Create axes
						var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
						dateAxis.renderer.minGridDistance = 150;

						var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
						valueAxis.min = 0;
						valueAxis.renderer.labels.template.adapter.add("text", function (text, target) {
							return text.match(/\./) ? "" : text;
						});
						// Create series
						var series = chart.series.push(new am4charts.LineSeries());
						series.dataFields.valueY = "views";
						series.dataFields.dateX = "date";
						series.tooltipText = "{views} views"

						series.tooltip.pointerOrientation = "vertical";

						chart.cursor = new am4charts.XYCursor();
						chart.cursor.snapToSeries = series;
						chart.cursor.xAxis = dateAxis;

						chart.scrollbarX = new am4charts.XYChartScrollbar();
						chart.scrollbarX.series.push(series);

					}); // end am4core.ready()
				}
			},
			error: function (response) {
				alert('Error');
			}
		});
	});
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#changeimg').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#file").change(function () {
		readURL(this);
	});
	$('.datepicker').datepicker({
		// dateFormat: 'mm/dd/yy'
		dateFormat: 'yy/mm/dd'
	});
	$('.datetimepicker').datetimepicker({
		// dateFormat: 'mm/dd/yy',
		dateFormat: 'yy/mm/dd',
		timeFormat: 'HH:mm:ss'
	});
	$('.datetimepicker-nos').datetimepicker({
		// dateFormat: 'mm/dd/yy',
		dateFormat: 'yy/mm/dd',
		timeFormat: 'HH:mm'
	});
	$('[data-toggle="tooltip"]').tooltip();
	//hold to copy
	function holdToCopy($linkTag) {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val($linkTag.attr('href')).select();
		document.execCommand("copy");
		$temp.remove();
		$linkTag.addClass('copied');
	}
	var timeoutHoldToCopy = 0;
	$('.link-hold-to-copy').on('click', function (e) {
		e.preventDefault();
	});
	$('.link-hold-to-copy').on('mousedown', function (e) {
		var _this = $(this);
		timeoutHoldToCopy = setTimeout(function () {
			holdToCopy($(_this));
		}, 1000);
	}).on('mouseup mouseleave', function () {
		$(this).removeClass('copied');
		clearTimeout(timeoutHoldToCopy);
	});

	/*$('#delete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) 
		var id = button.data('user_id') 
		var modal = $(this)
		modal.find('.modal-body #user_id').val(id);
		console.log(id);
	});*/

	//library modal
	$('.open-library').on('click', function (e) {
		e.preventDefault();
		var inputName = $(this).data('input-name');
		var inputType = $(this).data('input-type');
		$("#library-modal").data('input-name', inputName);
		$("#library-modal").data('input-type', inputType);
	});
	$("#library-modal").on('show.bs.modal', function () {
		var _thisModal = $(this);
		var action = $(this).data('action');
		$.ajax({
			url: action,
			method: "GET",
			dataType: 'JSON',
			success: function (response) {
				if (response.status) {
					$(_thisModal).find('#library-main').html(response.html);
				}
			},
			error: function (response) {
				alert('Error');
			}
		});
	});
	$("#library-modal").on('hide.bs.modal', function () {
		$(this).find('#library-main').html('<div class="modal-body pt-4"><p>読み込み中...</p></div>');
	});
	$('#library-modal').delegate('.media-item', 'click', function (e) {
		e.preventDefault();
		var _this = $(this);
		var inputName = $('#library-modal').data('input-name');
		console.log(inputName);
		var inputType = $('#library-modal').data('input-type');
		if (inputType == 'id') {
			$('[name ="' + inputName + '"]').val($(_this).data('id'));
		} else {
			$('[name ="' + inputName + '"]').val($(_this).data('url'));
		}
		//set preview
		if($('img[data-image-input-preview="' + inputName + '"]').length == 0 && $('div[data-image-input-preview="' + inputName + '"]').length == 0){
			var preview = '<div style="margin-top: 16px; margin-bottom: 16px;">'+
											'<img src="'+ $(_this).data('url') +'" style="max-height: 200px;max-width: 350px;" data-image-input-preview="'+inputName+'">'+
										'</div>';
			$(preview).insertBefore($('[name ="' + inputName + '"]'));

		}else{
			$('img[data-image-input-preview="' + inputName + '"]').attr('src', $(_this).data('url'));
			$('div[data-image-input-preview="' + inputName + '"]').css('background', 'url(' + $(_this).data('url') + ')');
		}
		
		$('#library-modal').modal('hide');
	});
	$('#library-modal').delegate('#form-library-uploader', 'submit', function (e) {
		e.preventDefault();
		var _thisForm = $(this);
		var action = $(this).attr('action');
		$.ajax({
			url: action,
			method: "POST",
			data: new FormData(this),
			dataType: 'JSON',
			contentType: false,
			cache: false,
			processData: false,
			success: function (response) {
				if (response.status) {
					$('#library-modal').find('.list-medias').prepend(response.html);
					$(_thisForm).find('input[name="file"]').val('');
					$('#link-library-media').trigger('click');
				} else {
					alert('Error');
				}
			},
			error: function (data) {
				alert('Error');
			}
		});
	});

	// form submit update menu tag
	$('.form-menu-tag').change(function (e) {
		$(this).trigger('submit');
	});
	$('.form-menu-tag').submit(function (e) {
		e.preventDefault();
		var _thisForm = $(this);
		var action = $(this).attr('action');
		$.ajax({
			url: action,
			method: "POST",
			data: new FormData(this),
			dataType: 'JSON',
			contentType: false,
			cache: false,
			processData: false,
			success: function (response) {
				if (response.status) {

				} else {
					alert('Error');
				}
			},
			error: function (data) {
				alert('Error');
			}
		});
	});
	// posts bulk action
	$("#setting-post").change(function(){
    $(this).find("option:selected").each(function(){
      var optionValue = $(this).attr("value");
      if(optionValue){
        $(".box-setting-post").not("." + optionValue).hide();
        $("." + optionValue).show();
      } else{
        $(".box-setting-post").hide();
      }
    });
	}).change();
	
	$('.bulk-checkbox#check-all').change(function(e){
		var ids = [];
		var titles = [];
    	var $checkedBoxes = $('.bulk-checkbox').not('#check-all');
		if($(this).is(':checked')){
			$('.bulk-checkbox').not('#check-all').prop('checked', true);
			var count = $checkedBoxes.length;
			if (count) {
				$.each($checkedBoxes, function () {
		        	var id = $(this).data('post-id');
		        	var title = $(this).data('post-title');
		        	ids.push(id);
		       		titles.push('<p class="mb-3">'+ title +'</p>');
		        })
			}
			$('input[name="post_ids"]').val(ids);
			$('.setting-post-delete .list-post').html(titles);
			$('.setting-post-delete .modal-title').html('<p style="text-align:center;margin-left: 50px;">削除した場合、復元は出来ません。<br>一括で選択した内容を全て削除しますか？</p>');
			// console.log('true');
		}else{
			$('.bulk-checkbox').not('#check-all').prop('checked', false);
			$('input[name="post_ids"]').val('');
			$('.setting-post-delete .list-post').html(titles);
			$('.setting-post-delete .modal-title').html('');
			// console.log('false');
		}
		// $('.modal-title').append(text);
	});
	$('.bulk-checkbox').not('#check-all').change(function(e){
		var ids = [];
		var titles = [];
    	var $checkedBoxes = $('.bulk-checkbox:checked').not('#check-all');
		var count = $checkedBoxes.length;
		if (count > 1) {
			// console.log(count);
			$.each($checkedBoxes, function () {
		        var id = $(this).data('post-id');
		        var title = $(this).data('post-title');
		        ids.push(id);
		        titles.push('<p class="mb-3">'+ title +'</p>');
		    });
		    $('.setting-post-delete .modal-title').html('<p style="text-align:center;margin-left: 50px;">削除した場合、復元は出来ません。<br>一括で選択した内容を全て削除しますか？</p>');
		} else {
			$.each($checkedBoxes, function () {
		        var id = $(this).data('post-id');
		        var title = $(this).data('post-title');
		        ids.push(id);
		        titles.push('<p class="mb-3">'+ title +'</p>');
		    });
			$('.setting-post-delete .modal-title').html('<p style="text-align:center;margin-left: 80px;">削除した場合、復元は出来ません。<br>それでも削除で宜しいでしょうか？</p>');
		}
		$('input[name="post_ids"]').val(ids);
		$('.setting-post-delete .list-post').html(titles);
	});
	//open modal single delete post
	$('.post-single-del').click(function(e){
		e.preventDefault();
		var ids = [];
		var titles = [];
		var id = $(this).data('post-id');
	    var title = $(this).data('post-title');
	    ids.push(id);
	    titles.push('<p>'+ title +'</p>');
		$('#delete-single-post-box input[name="post_ids"]').val(ids);
	    $('#delete-single-post-modal .list-post').html(titles);
	    $('#delete-single-post-modal').modal('show');
	});
});