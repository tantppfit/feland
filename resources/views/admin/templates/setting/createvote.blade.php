@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- main ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3 class="font-weight-bold">メディア設定</h3>
			@include('admin.parts.alert')

		</div>
		@include('admin.templates.setting.parts.setting-tabs')
        
        <section id="create-vote">
                <div class="create-vote-title">
                    <p>Create vote</p>
                </div>
                <!-- <a href="{{ route('admin.page.create') }}"><button class="btn btn-no-radius" style="background: #111111; color: #ffffff;"><i class="fas fa-plus" style="width: 25px; color: #ffffff;" aria-hidden="true"></i>固定ページを追加</button></a> -->
                
                <div class="vote-option">
                    <div class="form-group row" style="margin-top: 2rem;">
						<div class="vote-name font-weight-bold text-left col-12 col-md-1">
							<label>Name</label>
						</div>
						<div class="col-8 col-md-3 vote-input">
							<input type="text" class="form-control" name="" placeholder="Name...">
						</div>
                    </div>
                    
                    <div class="form-group row" style="margin-top: 1rem;">
						<div class="option-text font-weight-bold text-left col-12 col-md-1">
							<label>Option 1</label>
						</div>
						<div class="col-12 col-md-6 vote-input">
							<input type="text" class="form-control" name="" placeholder="Option 1...">
						</div>
                    </div>
                    
                    <div class="form-group row" style="margin-top: 1rem;">
						<div class="option-text font-weight-bold text-left col-12 col-md-1">
							<label>Option 2</label>
						</div>
						<div class="col-12 col-md-6 vote-input">
							<input type="text" class="form-control" name="" placeholder="Option 2...">
						</div>
                    </div>
                    
                    <div class="form-group row" style="margin-top: 1rem;">
						<div class="option-text font-weight-bold text-left col-12 col-md-1">
							<label>Option 3</label>
						</div>
						<div class="col-12 col-md-6 vote-input">
							<input type="text" class="form-control" name="" placeholder="Option 3...">
						</div>
                    </div>
                    
                    <div class="form-group row" style="margin-top: 1rem;">
						<div class="option-text font-weight-bold text-left col-12 col-md-1">
							<label>Option 4</label>
						</div>
						<div class="col-12 col-md-6 vote-input">
							<input type="text" class="form-control" name="" placeholder="Option 4...">
						</div>
                    </div>
                    
                    <div class="form-group row" style="margin-top: 1rem;">
						<div class="option-text font-weight-bold text-left col-12 col-md-1">
							<label>Option 5</label>
						</div>
						<div class="col-12 col-md-6 vote-input">
							<input type="text" class="form-control" name="" placeholder="Option 5...">
						</div>
                    </div>
                    
                    <button class="btn btn-submit btn-no-radius btn-vote" style="margin-bottom: 1rem;">Submit</button>
                </div>

                <div class="vote-list content-table-list table-responsive-md">
                    <h2>List Form Vote</h2>

                    <table class="table">
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Option 1</th>
                            <th>Option 2</th>
                            <th>Option 3</th>
                            <th>Option 4</th>
                            <th>Option 5</th>
                            <th>Action</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>AAA</td>
                            <td>xxx</td>
                            <td>xxx</td>
                            <td>xxx</td>
                            <td>xxx</td>
                            <td>xxx</td>
                            <td>
                                <a href="" style="display: inline-block; color: #C43638; margin-right: 25px;"><p style="margin-bottom: 50px;"><i class="far fa-edit" style="color: #C43638;margin-right: 5px;" aria-hidden="true"></i>設定と編集</p></a>
                                <a href=""><i class="fas fa-trash-alt" style="color: gray; font-size: 20px; vertical-align: middle;" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>BBB</td>
                            <td>xxx</td>
                            <td>xxx</td>
                            <td>xxx</td>
                            <td>xxx</td>
                            <td>xxx</td>
                            <td>
                                <a href="" style="display: inline-block; color: #C43638; margin-right: 25px;"><p style="margin-bottom: 50px;"><i class="far fa-edit" style="color: #C43638;margin-right: 5px;" aria-hidden="true"></i>設定と編集</p></a>
                                <a href=""><i class="fas fa-trash-alt" style="color: gray; font-size: 20px; vertical-align: middle;" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>CCC</td>
                            <td>xxx</td>
                            <td>xxx</td>
                            <td>xxx</td>
                            <td>xxx</td>
                            <td>xxx</td>
                            <td>
                                <a href="" style="display: inline-block; color: #C43638; margin-right: 25px;"><p style="margin-bottom: 50px;"><i class="far fa-edit" style="color: #C43638;margin-right: 5px;" aria-hidden="true"></i>設定と編集</p></a>
                                <a href=""><i class="fas fa-trash-alt" style="color: gray; font-size: 20px; vertical-align: middle;" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    </table>
                </div>

                <!-- <div style="margin-top: 20px;">
                    <p style="color: #999999;">#{{ (($pages->currentPage() - 1) * $pages->perPage()) + 1 }}...#{{ ($pages->currentPage() * $pages->perPage()) <= $pages->total() ? $pages->currentPage() * $pages->perPage() : $pages->total() }} / {{ $pages->total() }}</p>
                    <div class="pagination-menu d-flex"></div>
                </div> -->

                <div class="pagination-menu d-flex">
                    <select style="width: 50px; height: 33px !important; margin-right: 5px;">
                          <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1" selected>1</option>
                    </select>
    
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                        <li class="page-item"><a class="page-link" href="#">6</a></li>
                        <li class="page-item"><a class="page-link" href="#">7</a></li>
                        <li class="page-item"><a class="page-link" href="#">8</a></li>
                        <li class="page-item"><a class="page-link" href="#">9</a></li>
                        <li class="page-item"><a class="page-link" href="#">10</a></li>
                        <li class="page-item"><a class="page-link" href="#">11</a></li>
                        <li class="page-item" style="margin-left: .5rem;"><a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a></li>
                    </ul>
                </div>
            </section>
	</section>
	<!-----x----- main -----x----->
@endsection