@extends('admin.layouts.app')

@section('content')
@include('admin.parts.left-menu')
<!---------- admin-edit main section ---------->
    <section class="main">
        <div class="list-of-content-title">
            <h3>メニュー管理</h3>
            @include('admin.parts.alert')
        </div>

        <form class="edit-inquiry mt-5" action="{{ route('admin.menu.update', $menu) }}" method="POST">
            {{ csrf_field()}}
            <div class="form-group d-flex flex-wrap">
                <div class="label-admin-edit col-lg-2 col-md-3 col-12">
                    <label>タグ名</label>
                </div>
                <div class="col-md-9 col-12">
                    <input type="text" name="name" class="form-control" placeholder="タグ名" value="{{ old('name', $menu->name) }}" required="" style="font-size: 12px;">
                </div>
            </div>

            <div class="form-group d-flex flex-wrap">
                <div class="label-admin-edit col-lg-2 col-md-3 col-12">
                    <label>スラッグ</label>
                </div>
                <div class="col-md-9 col-12">
                    <input type="text" name="url" class="form-control"  placeholder="スラッグ" value="{{ old('url', $menu->url) }}" required="" style="font-size: 12px;">
                </div>
            </div>

            <div class="form-group d-flex flex-wrap">
                <div class="label-admin-edit col-lg-2 col-md-3 col-12">
                    <label>メニュー</label>
                </div>

                <div class="col-md-9 col-12">
                    <select class="form-control" name="location" required="" style="font-size: 12px;">
                        <option value="">メニュー</option>
                        @if(isset($menuLocations) && is_array($menuLocations))
                            @foreach($menuLocations as $key => $location)
                                <option value="{{ $key }}" {{ (old('location', $menu->location) == $key ) ? 'selected' : '' }}>{{ $location }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group d-flex flex-wrap">
                <div class="label-admin-edit col-lg-2 col-md-3 col-12">
                    <label>表示設定</label>
                </div>

                <div class="col-md-9 col-12">
                    <label>
                        <input type="checkbox" class="checkbox" name="show" value="1" {{ (old('show', $menu->show)) ? 'checked' : '' }}>
                        <span>表示</span>
                    </label>
                </div>
            </div>
            <div class="d-flex">
                <a href="{{ route('admin.menu.index') }}" class="btn-back btn btn-no-radius mr-2" style="font-size: 12px; width: 100px; line-height: 28px;"><i class="fas fa-arrow-left" aria-hidden="true"></i>戻る</a>
                <button class="btn btn-no-radius" style="background: #111; color: #fff;">更新メニュー</button>
            </div>

        </form>

        

    </section>
<!---------- admin-edit main section ---------->
@endsection