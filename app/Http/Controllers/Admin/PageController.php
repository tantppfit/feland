<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Page;
use App\Models\Media;
use Helper;
use Auth;

class PageController extends Controller
{
    protected $pageModel;
    protected $mediaModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Page $pageModel, Media $mediaModel)
    {
        $this->pageModel = $pageModel;
        $this->mediaModel = $mediaModel;
    }

    public function create(Request $request)
    {
        $this->authorize('create', Page::class);
        return view('admin.templates.page.create');
    }

    public function store(Request $request)
    {
        $this->authorize('store', Page::class);
        $this->validate($request, [
          'title' => 'required',
        ],[],[
            'title' =>  trans('table_fields.pages.title')
        ]);
        $pageData = $request->only(['title', 'slug', 'content']);
        $user = Auth::user();
        $pageData['user_id'] = $user->id;
        if(!empty($pageData['slug'])){
            $slug = self::incrementSlug(Helper::toSlug($pageData['slug']));
        }else{
            $slug = self::incrementSlug(Helper::toSlug($pageData['title']));
        }
        $pageData['slug'] = $slug;
        $page = $this->pageModel->create($pageData);
        if($request->has('file')){
            $imageUploaded = Helper::uploadMedia($request->file);
            $attributes = [
                'name' => $imageUploaded['file_name'],
                'info' => $imageUploaded['info']
            ];
            foreach ($imageUploaded['size_paths'] as $key => $path) {
                $attributes[$key . '_path'] = $path;
            }
            $media = $this->mediaModel->create($attributes);
            $page->cover_image = $media->id;
            $page->save();
        }
        return redirect()->route('admin.setting.fixedpage')->with('success', trans('messages.success.create', ['Module' => trans('module.entry.page')]));
    }

    public function edit(Request $request, $id)
    {
        $page = $this->pageModel->findOrFail($id);
        $this->authorize('edit', $page);
        return view('admin.templates.page.edit')->with([
            'page' => $page
        ]);
    }

    public function update(Request $request, $id)
    {
        $page = $this->pageModel->findOrFail($id);
        $this->authorize('update', $page);
        $this->validate($request, [
          'title' => 'required',
        ],[],[
            'title' =>  trans('table_fields.pages.title')
        ]);
        $pageData = $request->only(['title', 'slug', 'content', 'status']);
        if(in_array($page->slug, config('adiva.page_defaults'))){
            $slug = $page->slug;
        }else{
            if(!empty($pageData['slug'])){
                if($pageData['slug'] != $page->slug){
                    $slug = self::incrementSlug(Helper::toSlug($pageData['slug']));
                }else{
                    $slug = $page->slug;
                }
            }else{
                $slug = self::incrementSlug(Helper::toSlug($pageData['title']));
            }
        }
        
        $pageData['slug'] = $slug;
        if($request->has('file')){
            if($page->featureImage){
                Helper::deleteMedia($page->featureImage);
                $this->mediaModel->destroy($page->featureImage->id);
            }
            // upload new feature image
            $imageUploaded = Helper::uploadMedia($request->file);
            $attributes = [
                'name' => $imageUploaded['file_name'],
                'info' => $imageUploaded['info']
            ];
            foreach ($imageUploaded['size_paths'] as $key => $path) {
                $attributes[$key . '_path'] = $path;
            }
            $media = $this->mediaModel->create($attributes);
            $pageData['cover_image'] = $media->id;
        }
        $page->fill($pageData)->save();
        return redirect()->route('admin.page.edit', $page)->with('success', trans('messages.success.update', ['Module' => trans('module.entry.page')]));
    }

    public function changeStatus(Request $request, $id)
    {
        $page = $this->pageModel->findOrFail($id);
        $this->authorize('change_status', $page);
        $pageData = ['status' => !$page->status];
        $page->fill($pageData)->save();
        return redirect()->route('admin.setting.fixedpage')->with('success', trans('messages.success.update', ['Module' => trans('module.entry.page')]));
    }

    //slug functions
    public function incrementSlug($slug) {
        $original = $slug;
        $count = 2;
        while ($this->pageModel->whereSlug($slug)->exists()) {
            $slug = "{$original}-" . $count++;
        }
        return $slug;

    }

}
