@extends('frontend.layouts.app')
@section('content')
	<div class="main-content">
		<div class="profile-container">
			<div class="profile-bread-crumb">
				<ul>
					<li><a href="#">Chợ Tốt</a></li>
					<li><a href="#" class="active">Trang cá nhân của Đặng Gia Huy</a></li>
				</ul>
			</div>
			<div class="paper-container text-center" style="padding-bottom: 15px;">
				<div class="paper-info-wrapper text-left">
					<div class="row ml-0 mr-0">
						<div class="col-12 col-md-6 basic-info">
							<div class="avatar-wrapper">
								<img src="{{ asset('image/profile-user-avatar.jpg') }}">
							</div>
							<div class="info-user-wrapper">
								<span class="name">Đặng Gia Huy</span>
								<div class="follow-row">
									<div>
										<a href="#"><b>1</b> Người theo dõi</a>
									</div>
									<div>
										<a href="#"><b>0</b> Đang theo dõi</a>
									</div>
								</div>
								<div class="ulti-row">
									<button class="main-function-button follow">
										<span></span>
										 &nbsp; Theo dõi
									</button>
									<button class="circle-button">
										<span><i class="fas fa-ellipsis-h"></i></span>
									</button>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-6 extra-info">
							<div class="item-row">
								<img src="{{ asset('image/icon/pf_rating_icon.svg') }}">
								Đánh giá:&nbsp;
								<div class="rating-info">
									<span>Chưa có đánh giá</span>
								</div>
							</div>
							<div class="item-row">
								<img src="{{ asset('image/icon/calendar.png') }}" width="16">
								Ngày tham gia: &nbsp; 
								<div class="rating-info">
									<span>18/05/2020</span>
								</div>
							</div>
							<div class="item-row">
								<img src="{{ asset('image/icon/location.png') }}" width="16">
								Địa chỉ: &nbsp; 
								<div class="rating-info">
									<span>Chưa cung cấp</span>
								</div>
							</div>
							<div class="item-row">
								<img src="{{ asset('image/icon/chat.png') }}" width="16">
								Phản hồi chat: &nbsp; 
								<div class="rating-info">
									<span>Thỉnh thoảng (Trong 14 giờ)</span>
								</div>
							</div>
							<div class="item-row">
								<img src="{{ asset('image/icon/check.png') }}" width="16">
								Đã cung cấp: &nbsp; 
								<div class="position-relative info-icon">
									<img src="{{ asset('image/icon/facebook_active.png') }}" height="26">
								</div>
								<div class="position-relative info-icon">
									<img src="{{ asset('image/icon/address_default.png') }}" height="26">
								</div>
								<div class="position-relative info-icon">
									<img src="{{ asset('image/icon/phone_active.png') }}" height="26">
								</div>
								<div class="position-relative info-icon">
									<img src="{{ asset('image/icon/email_active.png') }}" height="26">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="user-index-page" style="padding-bottom: 15px;">
				<div class="paper-wrapper">
					<h4 class="title-heading">
						Đang bán 
						<span>- 13 tin</span>
					</h4>
					<div class="row ml-0 mr-0">
						<div style="padding:8px 0; width:100%">
							<!-- 3 ITEMS -->
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Biệt thự CC (6x16) 1trệt 4Lầu Hxh Phan Văn Trị P.7</h3>
														</div>
														<div class="user-ad-item-price">
															<span>13.200.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(image/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>hôm qua</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image-2.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Nhà góc 2 mặt tiền hẻm xe hơi 5m Phạm Văn Chiêu P9</h3>
														</div>
														<div class="user-ad-item-price">
															<span>4.460.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(image/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>2 ngày trước</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image-3.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Nhà phố hxh 4m (4*20) trệt 3lầu Quang Trung P.8 GV</h3>
														</div>
														<div class="user-ad-item-price">
															<span>7.200.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(image/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>2 ngày trước</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="see-more-user-ad-item">
									<div>
										<div class="text-center ml-0 position-relative" style="padding: 15px;">
											<div>
												<a href="{{ route('frontend.user.sale') }}">Xem tất cả &nbsp;<i class="fas fa-chevron-right"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
@endsection