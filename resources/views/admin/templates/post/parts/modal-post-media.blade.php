<div class="modal fade" id="post-media-modal" data-action="{{ route('admin.post.modalGetMedias', $post) }}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
    <div id="library-main" class="modal-content">
      <div class="modal-body pt-4">
	      <p>読み込み中...</p>
	    </div>
    </div>
  </div>
</div>