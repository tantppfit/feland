<!-- MODAL FILTER PRICE-->
<div class="modal fade" id="dynamic-filter-price" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="dynamic-filter-modal-dialog modal-dialog" role="document">
		<div class="dynamic-filter-modal-content modal-content">
			<div class="dynamic-filter-modal-header">
				<h4 class="text-center dymanic-filter-modal-title">Giá</h4>
				<button type="button" class="close float-left" data-dismiss="modal" aria-label="Close">
					<span class="close-btn" aria-hidden="true">&times;</span>
				</button>
				<button type="button" class="cancel-filter-btn" data-dismiss="modal" aria-label="Close">
					<span class="cancel-filter" aria-hidden="true">Bỏ lọc</span>
				</button>
			</div>
			<div class="dynamic-filter-modal-body">
				<div class="price-range-wrapper">
					<div class="price-from-to">
						<div>Giá từ <b class="price-min">0₫</b> đến <b class="price-max">30.000.000.000+₫</b></div>
					</div>
					<div class="range-price"></div>
				</div>
				<div style="margin-bottom: 30px;"></div>
			</div>
			<div class="dynamic-filter-modal-footer">
				<button class="filter-submit" type="submit">
					<span aria-hidden="true">Áp dụng </span>
				</button>
			</div>
		</div>
	</div>
</div>