<!-- MODAL PROJECT -->
<div class="modal fade" id="modal-project" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-project-dialog modal-dialog" role="document">
		<div class="modal-project-content modal-content">
			<div class="modal-project-header modal-header">
				<button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
					<span class="close-btn" aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-project-title">Vinhomes Grand Park (Vincity Quận 9)</h4>
			</div>
			<div class="modal-project-body modal-body">
				<div class="m-0">
					<div>
						<div class="m-0">
							<div class="d-block ml-auto mr-auto modal-project-image-wrapper">
								<div id="modal-project-image-container" class="carousel slide" data-ride="carousel">
									<!-- <ol class="carousel-indicators">
										<li data-target="#modal-project-image-wrapper" data-slide-to="0" class="active"></li>
										<li data-target="#modal-project-image-wrapper" data-slide-to="1"></li>
										<li data-target="#modal-project-image-wrapper" data-slide-to="2"></li>
									</ol> -->
									<div class="carousel-inner">
										<div class="carousel-item active">
											<img src="{{ asset('image/1317_overview_1.jpg') }}" class="d-block w-100" alt="...">
										</div>
										<div class="carousel-item">
											<img src="{{ asset('image/1317_overview_2.jpg') }}" class="d-block w-100" alt="...">
										</div>
										<div class="carousel-item">
											<img src="{{ asset('image/1317_facilities_3.jpg') }}" class="d-block w-100" alt="...">
										</div>
									</div>
									<ol class="carousel-indicators">
										<li data-target="#modal-project-image-container" data-slide-to="0" class="active"></li>
										<li data-target="#modal-project-image-container" data-slide-to="1"></li>
										<li data-target="#modal-project-image-container" data-slide-to="2"></li>
									</ol>
									<a class="ad-next-prev-btn ad-prev" href="#modal-project-image-container" role="button" data-slide="prev"><i></i></a>
									<a class="ad-next-prev-btn ad-next" href="#modal-project-image-container" role="button" data-slide="next"><i></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-project-address">
						<img src="image/icon/location.svg" alt="">
						<span class="street-address">Đường Nguyễn Xiển</span>
					</div>
					<div class="modal-project-info-wrapper">
						<div class="modal-project-main-info">
							<span>Thông tin chính</span>
						</div>
						<div class="row m-0 modal-project-sub-info">
							<div class="col-12 col-md-4">
								<span>
									<img src="{{ asset('image/icon/c6dad03ed0511a58cc24008efa63f2e2.svg') }}" alt="">
								</span>
								<span class="modal-project-sub-info-text">Giá: 14.73 triệu - 44.39 triệu/m2</span>
							</div>
							<div class="col-12 col-md-4">
								<span>
									<img src="{{ asset('image/icon/d3dbab00e33f82dc12be78801b0eb24f.svg') }}" alt="">
								</span>
								<span class="modal-project-sub-info-text">Bàn giao: 01-10-2021</span>
							</div>
							<div class="col-12 col-md-4">
								<span>
									<img src="{{ asset('image/icon/d27303818ce9020daa6db73bd9b410f4.svg') }}" alt="">
								</span>
								<span class="modal-project-sub-info-text">Chủ đầu tư: Tập đoàn Vingroup</span>
							</div>
						</div>
						<div>
							<div class="modal-line"></div>
						</div>
						<h2 class="modal-project-name">Vinhomes Grand Park (Vincity Quận 9)</h2>
						<div class="row m-0">
							<div class="col-12">
								<div style="text-align: justify;">
									<p>
										<p><strong>1.Vị trí:</strong></p>
										<p style="padding-left: 30px;">
											<span style="font-weight: 400;">Vincity Quận 9 tọa lạc ngay trục đường Nguyễn Xiển trung tâm Quận 9. Giáp các trục đường lớn như: Cao tốc Sài Gòn – Dầu Giây, đường Mai Chí Thọ, Vành đai 2…. tuyến Metro số 1</span>
										</p>
										<ul>
											<li>10 phút để tới khu du lịch Suối Tiên</li>
											<li>5 phút để tới khu trung tâm công nghệ cao của Tp.Hồ Chí Minh</li>
											<li>10 phút để tới khu thể thao sân Golf Rạch Chiếc</li>
											<li>15 phút để đến trung tâm Quận 2.</li>
											<li>6 phút để đến AEON Mall và Trường Đại Học Fulbright tương lai.</li>
										</ul>
										<p><strong>2.Tiện ích:</strong></p>
										<ul>
											<li>Hệ Thống trường đào tạo liên cấp quốc tế Vinschool</li>
											<li>Hệ thống bệnh viện chăm sóc sức khỏe Vinmec</li>
											<li>Hệ thống công viên ngoài trời với khu chạy bộ, khu thư giản, khu tổ chức sự kiện, khu thể dục thể thao, khu ngắm cảnh ven sông</li>
											<li>Hệ thống trung tâm thương mại: Nhà hàng, rạp chiếu phim, khu ẩm thực, khu mua sắm, khu giải trí trong nhà…</li>
											<li>Hệ thống quản lý, dịch vụ căn hộ tại Vinhomes và nhiều tiện ích nội khu khác</li>
										</ul>
										<p><strong>3.Chủ đầu tư:</strong></p>
										<ul>
											<li><span style="font-weight: 400;">Tập đoàn Vingroup - </span><span style="font-weight: 400;">Chủ đầu tư uy tín</span></li>
											<li>Dự&nbsp;án khác:Khu Đô thị Sinh thái Vinhomes Riverside, Trung tâm thương mại Vincom Center Bà Triệu, Vincom Center Đồng Khởi,&nbsp;Vincom Center Long Biên…<span style="font-weight: 400;">&nbsp;</span></li>
										</ul>
										<p><strong>4. Chính sách ưu đãi:</strong></p>
										<ul>
											<li>Ngân hàng Tec<span>hcom</span>bank cho vay 80% <span>giá</span> trị căn hộ</li>
											<li>Thời gian vay linh hoạt, kéo dài: Lên tới 35 năm tạo giảm áp lực trả nợ (Chỉ từ mức 4 triệu đồng/tháng)</li>
											<li>Đặc biệt trong thời gian chờ nhận nhà (<span>≈</span>24 tháng) sẽ không phải thanh toán nợ với ngân hàng</li>
										</ul>
										<p><strong> 5.Quy mô dự án: </strong></p>
										<ul>
											<li style="font-weight: 400;">Tổng diện tích: 365ha</li>
											<li style="font-weight: 400;">Quy mô&nbsp;: 71 toà Block, cao từ&nbsp;22 – 30 tầng</li>
											<li style="font-weight: 400;">Gồm 44.000 căn hộ, 700 căn Shophouse, 1.700 nhà phố và biệt thự. Với các diện tích:
												<ul>
													<li style="font-weight: 400;">Căn hộ&nbsp;: 30 – 90m2</li>
													<li style="font-weight: 400;">Shophouse&nbsp;: 100 – 200m2</li>
													<li style="font-weight: 400;">Nhà phố&nbsp;: 70 – 120m2</li>
													<li style="font-weight: 400;">Biệt thự&nbsp;: 200 – 500m2</li>
												</ul>
											</li>
										</ul>
										<p><strong>6.Tiến độ dự án:</strong></p>
										<ul>
											<li style="font-weight: 400;"><span style="font-weight: 400;">Năm khởi công: Quý IV/2018</span></li>
											<li style="font-weight: 400;"><span style="font-weight: 400;">Năm hoàn thành: 2021</span></li>
											<li style="font-weight: 400;"><span style="font-weight: 400;">Thời gian bàn giao: Quý IV/2021</span></li>
										</ul>
										<p>&nbsp;</p>
										<p>&nbsp;</p>
										<p>&nbsp;</p>
										<p>&nbsp;</p>
										<p>&nbsp;</p>
										<p>&nbsp;</p>
										<p>&nbsp;</p>
									</p>
									<div style="font-size: 13px; padding-bottom: 10px; line-height: 25px;">
										<div class="row m-0">
											<div class="col-6" style="text-align: right;"><span>Loại dự án</span></div>
											<div class="col-6" style="opacity: .5;"><span>Khu phức hợp</span></div>
										</div>
									</div>
									<div class="clear-fix"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-project-item-wrapper">
						<div class="modal-project-item-title">
							<span>Mặt bằng, Nhà mẫu</span>
						</div>
						<ul class="modal-project-ul row ml-0 mr-0">
							<li class="modal-project-li col-12 col-md-6">
								<span>Mặt bằng tổng thể</span>
								<img src="{{ asset('image/1317_floor_plan_project_12.jpg') }}">
							</li>
							<li class="modal-project-li col-12 col-md-6">
								<span>Mặt bằng tổng thể</span>
								<img src="{{ asset('image/1317_floor_plan_project_13.jpg') }}">
							</li>
							<li class="modal-project-li col-12 col-md-6">
								<span>Mặt bằng tổng thể</span>
								<img src="{{ asset('image/1317_floor_plan_project_12.jpg') }}">
							</li>
							<li class="modal-project-li col-12 col-md-6">
								<span>Mặt bằng tổng thể</span>
								<img src="{{ asset('image/1317_floor_plan_project_13.jpg') }}">
							</li>
							<li class="modal-project-li col-12 col-md-6">
								<span>Mặt bằng tổng thể</span>
								<img src="{{ asset('image/1317_floor_plan_project_12.jpg') }}">
							</li>
							<li class="modal-project-li col-12 col-md-6">
								<span>Mặt bằng tổng thể</span>
								<img src="{{ asset('image/1317_floor_plan_project_13.jpg') }}">
							</li>
							<li class="modal-project-li col-12 col-md-6">
								<span>Mặt bằng tổng thể</span>
								<img src="{{ asset('image/1317_floor_plan_project_12.jpg') }}">
							</li>
							<li class="modal-project-li col-12 col-md-6">
								<span>Mặt bằng tổng thể</span>
								<img src="{{ asset('image/1317_floor_plan_project_13.jpg') }}">
							</li>
							<li class="modal-project-li col-12 col-md-6">
								<span>Mặt bằng tổng thể</span>
								<img src="{{ asset('image/1317_floor_plan_project_12.jpg') }}">
							</li>
							<li class="modal-project-li col-12 col-md-6">
								<span>Mặt bằng tổng thể</span>
								<img src="{{ asset('image/1317_floor_plan_project_13.jpg') }}">
							</li>
						</ul>
						<div>
							<div class="see-more-project-li">
								<span>Xem thêm</span>
							</div>
						</div>
					</div>
					<div class="modal-project-item-wrapper">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>