<!-- MODAL FILTER VIDEO TYPE -->
<div class="modal fade" id="dynamic-filter-video-type" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="dynamic-filter-modal-dialog modal-dialog" role="document">
		<div class="dynamic-filter-modal-content modal-content">
			<div class="dynamic-filter-modal-header">
				<h4 class="text-center dymanic-filter-modal-title">Tin có video</h4>
				<button type="button" class="close float-left" data-dismiss="modal" aria-label="Close">
					<span class="close-btn" aria-hidden="true">&times;</span>
				</button>
				<button type="button" class="cancel-filter-btn" data-dismiss="modal" aria-label="Close">
					<span class="cancel-filter" aria-hidden="true">Bỏ lọc</span>
				</button>
			</div>
			<div class="dynamic-filter-modal-body">
				<div class="section-filter p-0">
					<div class="static-check-field">
						<h2 class="h2-label"></h2>
						<div class="without-modal-group">
							<div class="layout-group">
								<div class="layout-group-wrapper">
									<div class="layout-group-item">
										<div class="label-item">
											Tất cả
										</div>
										<div class="radio-button">
											<!-- <label for="" class="label-button label-button-checked"></label> -->
											<input type="radio" name="1" value="" checked="">
										</div>
									</div>
								</div>
							</div>
							<div class="layout-group">
								<div class="layout-group-wrapper">
									<div class="layout-group-item">
										<div class="label-item">
											Có video
										</div>
										<div class="radio-button">
											<!-- <label for="" class="label-button"></label> -->
											<input type="radio" name="1" value="">
										</div>
									</div>
								</div>
							</div>
							<div class="layout-group">
								<div class="layout-group-wrapper">
									<div class="layout-group-item">
										<div class="label-item">
											Không có video
										</div>
										<div class="radio-button">
											<!-- <label for="" class="label-button"></label> -->
											<input type="radio" name="1" value="">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>