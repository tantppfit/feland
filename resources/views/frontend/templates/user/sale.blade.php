@extends('frontend.layouts.app')
@section('content')
	<div class="main-content">
		<div class="profile-container">
			<div class="profile-bread-crumb">
				<ul>
					<li><a href="#">Chợ Tốt</a></li>
					<li><a href="#">Trang cá nhân của Đặng Gia Huy</a></li>
					<li><a href="#" class="active">Đang bán</a></li>
				</ul>
			</div>
			<div class="user-index-page" style="padding-bottom: 15px;">
				<div class="paper-wrapper">
					<h4 class="title-heading">
						Đang bán 
						<span>- 13 tin</span>
					</h4>
					<div class="row ml-0 mr-0">
						<div style="padding:8px 0; width:100%">
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Biệt thự CC (6x16) 1trệt 4Lầu Hxh Phan Văn Trị P.7</h3>
														</div>
														<div class="user-ad-item-price">
															<span>13.200.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(images/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>hôm qua</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image-2.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Nhà góc 2 mặt tiền hẻm xe hơi 5m Phạm Văn Chiêu P9</h3>
														</div>
														<div class="user-ad-item-price">
															<span>4.460.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(images/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>2 ngày trước</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image-3.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Nhà phố hxh 4m (4*20) trệt 3lầu Quang Trung P.8 GV</h3>
														</div>
														<div class="user-ad-item-price">
															<span>7.200.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(images/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>2 ngày trước</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Biệt thự CC (6x16) 1trệt 4Lầu Hxh Phan Văn Trị P.7</h3>
														</div>
														<div class="user-ad-item-price">
															<span>13.200.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(images/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>hôm qua</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image-2.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Nhà góc 2 mặt tiền hẻm xe hơi 5m Phạm Văn Chiêu P9</h3>
														</div>
														<div class="user-ad-item-price">
															<span>4.460.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(images/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>2 ngày trước</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image-3.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Nhà phố hxh 4m (4*20) trệt 3lầu Quang Trung P.8 GV</h3>
														</div>
														<div class="user-ad-item-price">
															<span>7.200.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(images/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>2 ngày trước</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Biệt thự CC (6x16) 1trệt 4Lầu Hxh Phan Văn Trị P.7</h3>
														</div>
														<div class="user-ad-item-price">
															<span>13.200.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(images/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>hôm qua</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image-2.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Nhà góc 2 mặt tiền hẻm xe hơi 5m Phạm Văn Chiêu P9</h3>
														</div>
														<div class="user-ad-item-price">
															<span>4.460.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(images/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>2 ngày trước</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image-3.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Nhà phố hxh 4m (4*20) trệt 3lầu Quang Trung P.8 GV</h3>
														</div>
														<div class="user-ad-item-price">
															<span>7.200.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(images/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>2 ngày trước</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Biệt thự CC (6x16) 1trệt 4Lầu Hxh Phan Văn Trị P.7</h3>
														</div>
														<div class="user-ad-item-price">
															<span>13.200.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(images/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>hôm qua</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image-2.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Nhà góc 2 mặt tiền hẻm xe hơi 5m Phạm Văn Chiêu P9</h3>
														</div>
														<div class="user-ad-item-price">
															<span>4.460.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(images/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>2 ngày trước</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image-3.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Nhà phố hxh 4m (4*20) trệt 3lầu Quang Trung P.8 GV</h3>
														</div>
														<div class="user-ad-item-price">
															<span>7.200.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(images/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>2 ngày trước</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div>
								<div class="user-list-item">
									<ul>
										<li>
											<a href="#">
												<div class="user-ad-item-container">
													<div class="user-ad-image-wrapper">
														<div class="user-ad-image-container">
															<div class="position-relative w-100" style="height: 100px;">
																<img src="{{ asset('image/user-ad-item-image.jpg') }}">
															</div>
														</div>
													</div>
													<div class="user-ad-item-info">
														<div class="user-ad-item-title">
															<h3>Biệt thự CC (6x16) 1trệt 4Lầu Hxh Phan Văn Trị P.7</h3>
														</div>
														<div class="user-ad-item-price">
															<span>13.200.000.000 đ</span>
														</div>
													</div>
												</div>
											</a>
											<div class="user-ad-item-footer">
												<div class="position-relative w-100 h-100">
													<div class="user-role">
														<div class="d-flex">
															<div class="user-role-image" style="background-image: url(images/icon/pro.svg);"></div>
															<span>Môi giới</span>
														</div>
													</div>
													<div class="user-posted-time">
														<span>hôm qua</span>
													</div>
												</div>
											</div>
											<div class="user-ad-save">
												<button>
													<img width="20" src="{{ asset('image/icon/heart.png') }}" alt="like">
												</button>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
@endsection