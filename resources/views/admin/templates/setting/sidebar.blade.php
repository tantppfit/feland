@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- main ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3 class="font-weight-bold">メディア設定</h3>
			@include('admin.parts.alert')
		</div>
		@include('admin.templates.setting.parts.setting-tabs')

		<section id="nav-tab-1">
			<div class="nav-tab-1-title">
				<p>サイドバー設定</p>
			</div>

			<div class="media-edit">
				<form action="{{ route('admin.setting.update',['redirect' => route('admin.setting.sidebar')]) }}" method="POST" class="media-edit-form" style="margin: 1rem 0;">

					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>Feature category</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<select name="siderbar_feature_cat_id" class="form-control">
								<option value="">Select category</option>
								@if(isset($parentCategories) && count($parentCategories))
									@foreach($parentCategories as $parentCategory)
										<option value="{{ $parentCategory->id }}" {{ (array_key_exists('siderbar_feature_cat_id', $settings) && ($settings['siderbar_feature_cat_id'] == $parentCategory->id)) ? 'selected' : '' }}>{{ $parentCategory->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>Feature number</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<input type="number" class="form-control" name="siderbar_feature_number" min="1" value="{{ array_key_exists('siderbar_feature_number', $settings) ? $settings['siderbar_feature_number'] : '1' }}" />
						</div>
					</div>
					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>Series category</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<select name="siderbar_series_cat_id" class="form-control">
								<option value="">Select category</option>
								@if(isset($parentCategories) && count($parentCategories))
									@foreach($parentCategories as $parentCategory)
										<option value="{{ $parentCategory->id }}" {{ (array_key_exists('siderbar_series_cat_id', $settings) && ($settings['siderbar_series_cat_id'] == $parentCategory->id)) ? 'selected' : '' }}>{{ $parentCategory->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>Series number</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<input type="number" class="form-control" name="siderbar_series_number" min="1" value="{{ array_key_exists('siderbar_series_number', $settings) ? $settings['siderbar_series_number'] : '1' }}" />
						</div>
					</div>
					<button class="btn btn-no-radius" style="background: #111; color: #fff;">更新</button>
					{{ csrf_field() }}
				</form>
			</div>
		</section>
	</section>
	<!-----x----- main -----x----->
@endsection