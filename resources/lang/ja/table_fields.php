<?php
return [
	'categories' => [
		'name' => 'Category name'
	],
	'inquiries' => [
		'id' => 'ID',
		'ids' => 'IDs',
		'email' => 'Email',
		'content' => 'Content',
		'message' => 'Message',
		'work_status' => 'Work status'
	],
	'medias' => [
		'id' => 'ID',
		'file' => 'File',
		'files' => 'Files'
	],
	'menus' => [
		'name' => 'Name',
		'url' => 'Url',
		'location' => 'Location'
	],
	'tags' => [
		'name' => 'Name'
	],
	'pages' => [
		'title' => 'Title'
	],
	'posts' => [
		'title' => 'タイトル',
		'content' => '記事の内容',
		'slug' => 'Slug',
		'cover_image_style' => 'Cover image style'
	],
	'post_sliders' => [
		'slider_image_path' => 'Path',
		'content' => 'Content',
		'slug' => 'Slug',
	],
	'users' => [
		'email' => 'Email',
		'current_password' => 'Current password',
		// 'password' => 'Password',
		'password' => 'パスワード',
		'role_id' => 'Role',
		'file' => 'File'
	],
	'support_emails' => [
		'email' => 'Email'
	],
	'votes' => [
		'name' => 'Name',
		'content' => 'Content'
	],
];