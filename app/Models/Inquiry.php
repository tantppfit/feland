<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
	protected $fillable = [
		'email', 'content', 'work_status', 'agent', 'admin_note'
	];

	public function getInquiries($request){
		$query = $this->query();
		$query->orderBy('id', 'desc');
		if($request->has('email') && !empty($request->email)){
			$query->where('email', 'like', '%' . $request->email . '%');
		}
		if($request->has('id') && !empty($request->id)){
			$query->where('id', $request->id);
		}
		if($request->has('work_status') && is_array($request->work_status)){
			$query->whereIn('work_status', $request->work_status);
		}
		return $query->paginate();
	}

	public function updateAll(array $attributes){
		foreach ($attributes['ids'] as $id) {
			$entity = $this->findOrFail($id);
			if(array_key_exists('work_status', $attributes)){
				$entity->update(['work_status' => $attributes['work_status']]);
			}
		}
		return true;
	}
}