<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Page extends Model
{
    protected $fillable = [
        'title', 'slug', 'content', 'cover_image', 'user_id', 'status'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function featureImage()
    {
        return $this->belongsTo(Media::class, 'cover_image');
    }
    
    public function getPages($request, $number=15){
        $query = $this->query();
        $query->orderBy('id','desc');
        if($request->has('s')){
            $query->where('title','like', '%'.$request->s.'%');
        }
        return $query->paginate($number);
    }

    public function getPageBySlug($slug){
        $query = $this->query();
        $query->where('status', 1);
        $query->where('slug', $slug);
        return $query->first();
    }
}