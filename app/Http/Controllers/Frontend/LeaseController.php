<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

/**
 * 
 */
class LeaseController extends Controller
{
	
	function __construct()
	{
		
	}
	
	public function index() 
	{
		return view('frontend.templates.lease.index');
	}

	public function item()
	{
		return view('frontend.templates.lease.item');
	}
}